import tkinter
from http import HTTPStatus, server
from urllib import request, response, error, parse

root = tkinter.Tk()

root.title("My window")

label = tkinter.Label(root, text="Mon label")
label.pack()

entry = tkinter.Entry(root, width=35, borderwidth=5)
entry.pack()

def submit():
    current = entry.get()
    print(current)
    

my_button = tkinter.Button(root, text='Submit', padx=40, pady=40, command=submit)
my_button.pack()

tkinter.mainloop()
