from tkinter import *

root = Tk()
root.title("Copy Label text")
root.iconbitmap("codemy.ico")
root.geometry('500x350')

# Create Label
my_label = Label(root, text="Label 1", font=("Helvetica", 20))
my_label.pack(pady=20)

# -----------------
# Use string var
# StringVar for entry box
my_text = StringVar(root)
my_text.set("This is the label 2")

# Create Entry Box
my_entry = Entry(root,
                 font=("Helvetica", 20),
                 bd=0,
                 state="readonly",
                 textvariable=my_text)
my_entry.pack(pady=20)

# -----------------

# Use insert
# Create Entry Box
my_entry2 = Entry(root,
                  font=("Helvetica", 20),
                  bd=0,
                  width=16)
my_entry2.pack(pady=20)

# insert text
my_entry2.insert(0, "This is my label 3")
# set to read only
my_entry2.config(state="readonly")

root.mainloop()
