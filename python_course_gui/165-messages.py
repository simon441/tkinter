from tkinter import *

root = Tk()
root.title("Simple Button Amination")
root.iconbitmap("codemy.ico")
root.geometry('400x900')

def change_text():
    my_message.config(text="And now for something completely different: a man with nine legs!")

def change_aspect():
    my_message.config(aspect=200)

# First One
frame1 = LabelFrame(root, text="Right Justified")
frame1.pack(pady=20)

my_message = Message(frame1,
                     text="This is some\nlong text that I am typing so that we can look at it, isn't it cool?",
                     font=("Helvetica", 18),
                     aspect=150,# 1502 = default value
                     justify=RIGHT)
my_message.pack(pady=10, padx=10)

# Second One

frame2 = LabelFrame(root, text="Left Justified")
frame2.pack(pady=20)

my_message2 = Message(frame2,
                     text="This is some\nlong text that I am typing so that we can look at it, isn't it cool?",
                     font=("Helvetica", 18),
                     aspect=100,
                     justify=LEFT)
my_message2.pack(pady=10, padx=10)

# Third One

frame3 = LabelFrame(root, text="Centered")
frame3.pack(pady=20)
my_message3 = Message(frame3,
                     text="This is some\nlong text that I am typing so that we can look at it, isn't it cool?",
                     font=("Helvetica", 18),
                     aspect=300,
                     justify=CENTER)
my_message3.pack(pady=10, padx=10)


# Button
my_button =Button(root, text="Change text", command=change_text)
my_button.pack(pady=20)

my_button =Button(root, text="Change aspect", command=change_aspect)
my_button.pack(pady=20)

root.mainloop()
