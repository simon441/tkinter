from tkinter import *
from tkinter.font import Font
import tkinter as tki

root = Tk()
root.title("Custom Font")
root.iconbitmap("codemy.ico")
root.geometry('500x500')

# Define the font
big_font = Font(root,
                family="Times",
                size=42,
                weight="bold",
                slant="roman",  # italic/roman
                underline=0,  # 10 or 1
                overstrike=0,  # 0 or 1 (strikethrough)
                )

# Define the font
medium_font = Font(root,
                   family="HJelvetica",
                   size=24,
                   weight="normal",
                   slant="italic",  # italic/roman
                   underline=1,  # 10 or 1
                   overstrike=0,  # 0 or 1 (strikethrough)
                   )


# Define a button
my_button1 = Button(root, text="Big Text", font=big_font)
my_button1.pack(pady=20)
# Define a label
my_label = Label(root, text="more big text", font=big_font)
my_label.pack(pady=20)
my_label2 = Label(root, text="medium text", font=medium_font)
my_label2.pack(pady=20)

root.mainloop()
