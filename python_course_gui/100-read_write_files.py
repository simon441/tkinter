from tkinter import *
from tkinter import filedialog
import os

root = Tk()
root.title('Text box')
root.iconbitmap("codemy.ico")
root.geometry("500x550")

def open_file():
#    text_file = open("data/sample.txt", 'r')
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Open Text File", filetypes=(("Text Files", "*.txt"),))
    text_file = open(file_path, 'r')
    temp_text = text_file.read()

    my_textbox.insert(END, temp_text)
    text_file.close()

# Read only r  
# Read and Write r+  (beginning of file)
# Write Only w   (over-written)
# Write and Read w+  (over written)
# Append Only a  (end of file)
# Append and Read a+  (end of file)

def save_file():
    # text_file = open('data/sample.txt', 'w')
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Save Text File", filetypes=(("Text Files", "*.txt"),))
    text_file = open(file_path, 'w')
    text_file.write(my_textbox.get(1.0, END))
    # text_file.close()

my_textbox = Text(root, width=40, height=10, font=('Helvetica', 16))
my_textbox.pack(pady=20)

open_button = Button(root, text="Open Text File", command=open_file)
open_button.pack(pady=20)

save_button = Button(root, text="Save Text File", command=save_file)
save_button.pack(pady=20)

root.mainloop()