from tkinter import Tk, Label, Frame, Menu, BOTH, messagebox
from tkinter import * 
from PIL import ImageTk, Image
from random import randint
from os import listdir, path


root = Tk()
root.title('Flashcards!')
root.iconbitmap('codemy.ico')
root.geometry("500x500")

state_image = None
STATES_DIR = 'states'

#############
# FUNCTIONS
#############

def states():
    """ State Flashcard Function """
    # hide all frames
    hide_all_frames()
    state_frame.pack(fill=BOTH, expand=1)
    # my_label = Label(state_frame, text="States").pack()

    # Create a list of state names
    files_list = listdir(STATES_DIR)
    # print(files_list)
    our_states = [x.replace('.png', '') for x in files_list]
    # print(states)

    # Generate a random number
    random_state = randint(0, len(our_states) -1)

    state_path = STATES_DIR + '/' + our_states[random_state] + '.png'
    # print(state_path)
    if not path.isfile(state_path):
        messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
        return

    # Create ou state images
    global state_image
    state_image = ImageTk.PhotoImage(Image.open(state_path))
    show_state = Label(state_frame, image=state_image)
    show_state.pack(pady=15)

    # Create button to Randomize State Images
    random_state_button = Button(state_frame, text="Next State", command=states)
    random_state_button.pack(pady=10)


def state_capitals():
    """ State capitals Flashcard Function """
    # hide all frames
    hide_all_frames()
    state_capitals_frame.pack(fill=BOTH, expand=1)
    my_label = Label(state_capitals_frame, text="Capitals")
    my_label.pack()


def hide_all_frames():
    """ Hide all frames """

    # Loop through all the children in each frame and delete them
    for widget in state_frame.winfo_children():
        widget.destroy()

    for widget in state_capitals_frame.winfo_children():
        widget.destroy()

    # hide frames
    state_frame.pack_forget()
    state_capitals_frame.pack_forget()


############
# MENU
############

# Create our menu
my_menu = Menu(root)
root.config(menu=my_menu)

# Create Geography menu items
states_menu = Menu(my_menu)
my_menu.add_cascade(label="Geography", menu=states_menu)

states_menu.add_command(label="States", command=states)
states_menu.add_command(label="State Capitals", command=state_capitals)
states_menu.add_separator()
states_menu.add_command(label="Exit", command=root.quit)

###########
# Frames
###########

state_frame = Frame(root, width=500, height=500)
state_capitals_frame = Frame(root, width=500, height=500)

# Launch GUI
root.mainloop()