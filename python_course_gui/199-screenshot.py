
from pathlib import Path

from mss import mss
import tkinter

root = tkinter.Tk()
root.title('Screen Shot')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('500x200')


def shot():
    """Take a screenshot"""
    # save a screen shot of the 1st monitor
    with mss() as sct:
        # Designate the filename
        filename = sct.shot(mon=1, output='output.png')

        # Confirm message of screenshot taken
        my_label.config(text="Screen Shot Has Been Saved!")


my_button = tkinter.Button(
    root, text='Take a Screenshot!', font=('Helvetica', 24), command=shot)
my_button.pack(pady=40)

my_label = tkinter.Label(root, text='')
my_label.pack(pady=10)

root.mainloop()
