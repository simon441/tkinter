from tkinter import *

WIDTH = 500
HEIGHT = 300

root = Tk()
root.title('Change Titlebar Color')
root.iconbitmap('codemy.ico')
root.geometry(f"{WIDTH}x{HEIGHT}")


# Remove titlebar
root.overrideredirect(True)


def move_app(e):
    """ Used when dragging app titlebar """
    root.geometry(f'+{e.x_root}+{e.y_root}')


def quitter(e):
    """ Quit App """
    root.quit()
    root.destroy()


# Fake titlebar
title_bar = Frame(root, bg="darkgreen", relief="raised", bd=0)
title_bar.pack(expand=1, fill=X)

# Bind the titlebar
title_bar.bind('<B1-Motion>', move_app)

# Put Icon code here

# Title text
title_label = Label(title_bar, text="     My Awesome App!     ",
                    bg="darkgreen", fg="white")
title_label.pack(side=LEFT, pady=4)

# Close button on titlebar
close_label = Label(title_bar, text="  X  ", bg="darkgreen",
                    fg="white", relief=SUNKEN, bd=0, font=("Helvetica", 10))
close_label.pack(side=RIGHT, pady=4)
close_label.bind('<Button-1>', quitter)

# Buttons

my_button = Button(root, text="Close", font=(
    "Helvetica", 32), command=quitter)
my_button.pack(pady=100)

root.mainloop()
