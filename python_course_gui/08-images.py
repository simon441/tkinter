from tkinter import *
from PIL import ImageTk, Image

root = Tk()
root.title("Images and Icons")
root.iconbitmap('codemy.ico')

my_img = ImageTk.PhotoImage(Image.open('images/aspen.png'))

my_label = Label(root, image=my_img)
my_label.pack()


button_quit = Button(root, text="Exit program", command=root.quit)
button_quit.pack()


root.mainloop()