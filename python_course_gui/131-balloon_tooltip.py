from tkinter import *
from tkinter.tix import *

root = Tk()
root.title("Balloon tooltip * tkinter.tix deprecated *")
root.iconbitmap("codemy.ico")
root.geometry('500x350')

# Create Tooltip
tip = Balloon(root)

# [style the border of the messagebox]
tip.config(bd=10, bg="blue")

# Sub-categories:
# [style the arrow on the top left]
tip.label.config(bg="red", fg="white", bd=20)
# [tooltip text]
tip.message.config(bg="blue", fg="white")

# Button
my_button = Button(root, text="Click me!")
my_button.pack(pady=50)

# Label
my_label = Label(root, text="Some random text", font=("Helvetica", 20))
my_label.pack(pady=20)

# Bind tooltip to button
tip.bind_widget(my_button, balloonmsg="This is my awesome tooltip!")

# Bind tooltip to label
tip.bind_widget(my_label, balloonmsg="This is also my awesome tooltip!")

root.mainloop()