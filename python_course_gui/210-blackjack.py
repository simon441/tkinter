import random
from pathlib import Path
from tkinter import Button, Frame, Label, LabelFrame, Tk, messagebox
from typing import List

from PIL import Image, ImageTk

root = Tk()
root.title('Blackjack')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('1200x800')
root.configure(background='green')


class BlackJack():

    def __init__(self, root) -> None:
        self.root = root
        # Global variables
        # The Deck of cards
        self.Deck: List[str] = []
        # Dealer
        self.Dealer: List[str] = []
        # Player
        self.Player: List[str] = []
        # Dealer Starting Spot
        self.Dealer_spot = 0
        # Player Starting Spot
        self.Player_spot = 0
        # Maximum card number show per player
        self.MAX_CARD_SHOWN = 5
        # Scores
        self.dealer_score_list: List[int] = []
        self.player_score_list: List[int] = []

        # Blackjack status
        self.blackjack_status = {'dealer': 'no', 'player': 'no'}

        self.create_widgets()

        # Shuffle cards on app load
        self.shuffle()

    def create_widgets(self):
        # Main Frame
        main_frame = Frame(self.root, bg='green')
        main_frame.pack(pady=20)

        # Dealer Frame
        dealer_frame = LabelFrame(main_frame, text='Dealer', bd=0)
        dealer_frame.pack(padx=20, ipadx=20)
        # dealer_frame.grid(row=0, column=0, padx=20, ipadx=20)

        # Player Frame
        player_frame = LabelFrame(main_frame, text='Player', bd=0)
        # player_frame.grid(row=1, column=1, ipadx=20)
        player_frame.pack(ipadx=20, pady=10)

        # Put cards in Frames
        self.dealer_label_0 = Label(dealer_frame, text='')
        self.dealer_label_0.grid(row=0, column=0, pady=20, padx=20)

        self.dealer_label_1 = Label(dealer_frame, text='')
        self.dealer_label_1.grid(row=0, column=1, pady=20, padx=20)

        self.dealer_label_2 = Label(dealer_frame, text='')
        self.dealer_label_2.grid(row=0, column=2, pady=20, padx=20)

        self.dealer_label_3 = Label(dealer_frame, text='')
        self.dealer_label_3.grid(row=0, column=3, pady=20, padx=20)

        self.dealer_label_4 = Label(dealer_frame, text='')
        self.dealer_label_4.grid(row=0, column=4, pady=20, padx=20)

        self.player_label_0 = Label(player_frame, text='')
        self.player_label_0.grid(row=1, column=0, pady=20, padx=20)

        self.player_label_1 = Label(player_frame, text='')
        self.player_label_1.grid(row=1, column=1, pady=20, padx=20)

        self.player_label_2 = Label(player_frame, text='')
        self.player_label_2.grid(row=1, column=2, pady=20, padx=20)

        self.player_label_3 = Label(player_frame, text='')
        self.player_label_3.grid(row=1, column=3, pady=20, padx=20)

        self.player_label_4 = Label(player_frame, text='')
        self.player_label_4.grid(row=1, column=4, pady=20, padx=20)

        # Button Frame
        button_frame = Frame(self.root, bg='green')
        button_frame.pack(pady=20)

        # Button to Shuffle the Deck
        shuffle_button = Button(button_frame, text='Shuffle Deck',
                                font=('Helvetica', 14), command=self.shuffle)
        shuffle_button.grid(row=0, column=0)

        # Button to Deal the Cards to the Players
        self.hit_me_button = Button(button_frame, text='Hit Me!', font=(
            'Helvetica', 14), command=self.player_hit)
        self.hit_me_button.grid(row=0, column=1, padx=10)

        self.stand_button = Button(button_frame, text='Stand',
                                   font=('Helvetica', 14))
        self.stand_button.grid(row=0, column=2)

    def resize_card(self, card: str) -> ImageTk.PhotoImage:
        """Resize the Cards with the PIL library
        """
        # Get the full image path
        image_path = Path.joinpath(
            Path(__file__).parent.resolve(), 'images', 'card_deck', card)

        # Open the Image
        card_image = Image.open(image_path)

        # Resize the Image
        card_image_resized = card_image.resize((150, 218))

        # Output the card
        card_image_tk = ImageTk.PhotoImage(card_image_resized)

        # Return the resized card
        return card_image_tk

    def shuffle(self):
        """Shuffle the cards
        """
        # Keep track of winning
        self.blackjack_status = {'dealer': 'no', 'player': 'no'}

        # Enable buttons
        self.hit_me_button.config(state='normal')
        self.stand_button.config(state='normal')

        # Clear all the old cards from previous games
        self.dealer_label_0.config(image='')
        self.dealer_label_1.config(image='')
        self.dealer_label_2.config(image='')
        self.dealer_label_3.config(image='')
        self.dealer_label_4.config(image='')

        self.player_label_0.config(image='')
        self.player_label_1.config(image='')
        self.player_label_2.config(image='')
        self.player_label_3.config(image='')
        self.player_label_4.config(image='')

        # Define the Deck
        # The four suits of cards
        suits = ['diamonds', 'clubs', 'hearts', 'spades']
        # 2 is the first card in the Deck, Ace is the 14th (range does not include the end number e.g. 15)
        # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        values = range(2, 15)

        # Remove all cards from the deck
        self.Deck.clear()

        # fill the Deck
        for suit in suits:
            for value in values:
                self.Deck.append(f'{value}_of_{suit}')

        # Create the players
        self.Dealer.clear()
        self.Player.clear()

        self.Dealer_spot = 0
        self.Player_spot = 0

        self.dealer_score_list.clear()
        self.player_score_list.clear()

        # Grab a random Card for the Dealer
        self.dealer_hit()
        self.dealer_hit()

        # Shuffle two cards for the Player
        self.player_hit()
        self.player_hit()

        # Put number of remaining cards in the app's title bar
        root.title(f'{len(self.Deck)} cards left')

    def dealer_hit(self):
        # global Dealer_spot
        if self.Dealer_spot < self.MAX_CARD_SHOWN:
            try:
                # Get a Card for the Player
                dealer_card = random.choice(self.Deck)
                # Remove card from Deck
                self.Deck.remove(dealer_card)
                # Append card to Dealer's list
                self.Dealer.append(dealer_card)
                # Append to Dealers' score list and converts facecards to 10 or 11
                self.dealer_score_list.append(
                    self._get_card_score(dealer_card))

                if self.Dealer_spot == 0:
                    # Resize Card image
                    self.dealer_image1 = self.resize_card(f'{dealer_card}.png')
                    # Output card to screen
                    self.dealer_label_0.config(image=self.dealer_image1)
                    # Increment player spot counter
                    self.Dealer_spot += 1
                elif self.Dealer_spot == 1:
                    # Resize Card image
                    # global dealer_image2
                    self.dealer_image2 = self.resize_card(f'{dealer_card}.png')
                    # Output card to screen
                    self.dealer_label_1.config(image=self.dealer_image2)
                    # Increment player spot counter
                    self.Dealer_spot += 1
                elif self.Dealer_spot == 2:
                    # Resize Card image
                    # global dealer_image3
                    self.dealer_image3 = self.resize_card(f'{dealer_card}.png')
                    # Output card to screen
                    self.dealer_label_2.config(image=self.dealer_image3)
                    # Increment player spot counter
                    self.Dealer_spot += 1
                elif self.Dealer_spot == 3:
                    # Resize Card image
                    # global dealer_image4
                    self.dealer_image4 = self.resize_card(f'{dealer_card}.png')
                    # Output card to screen
                    self.dealer_label_3.config(image=self.dealer_image4)
                    # Increment player spot counter
                    self.Dealer_spot += 1
                elif self.Dealer_spot == 4:
                    # Resize Card image
                    # global dealer_image5
                    self.dealer_image5 = self.resize_card(f'{dealer_card}.png')
                    # Output card to screen
                    self.dealer_label_4.config(image=self.dealer_image5)
                    # Increment player spot counter
                    self.Dealer_spot += 1

                # Put number of remaining cards in the app's title bar
                root.title(f'{len(self.Deck)} cards left')
            except IndexError as e:
                root.title(f'No Cards remaining in Deck')

            # Check for blackjack
            self.blackjack_shuffle('dealer')

    def player_hit(self):
        if self.Player_spot < self.MAX_CARD_SHOWN:
            try:
                # Get a Card for the Player
                player_card = random.choice(self.Deck)
                # Remove card from Deck
                self.Deck.remove(player_card)
                # Append card to Player's list
                self.Player.append(player_card)
                # Append to Players' score list and converts facecards to 10 or 11
                self.player_score_list.append(
                    self._get_card_score(player_card))

                if self.Player_spot == 0:
                    # Resize Card image
                    self.player_image1 = self.resize_card(f'{player_card}.png')
                    # Output card to screen
                    self.player_label_0.config(image=self.player_image1)
                    # Increment player spot counter
                    self.Player_spot += 1
                elif self.Player_spot == 1:
                    # Resize Card image
                    self.player_image2 = self.resize_card(f'{player_card}.png')
                    # Output card to screen
                    self.player_label_1.config(image=self.player_image2)
                    # Increment player spot counter
                    self.Player_spot += 1
                elif self.Player_spot == 2:
                    # Resize Card image
                    self.player_image3 = self.resize_card(f'{player_card}.png')
                    # Output card to screen
                    self.player_label_2.config(image=self.player_image3)
                    # Increment player spot counter
                    self.Player_spot += 1
                elif self.Player_spot == 3:
                    # Resize Card image
                    self.player_image4 = self.resize_card(f'{player_card}.png')
                    # Output card to screen
                    self.player_label_3.config(image=self.player_image4)
                    # Increment player spot counter
                    self.Player_spot += 1
                elif self.Player_spot == 4:
                    # Resize Card image
                    self.player_image5 = self.resize_card(f'{player_card}.png')
                    # Output card to screen
                    self.player_label_4.config(image=self.player_image5)
                    # Increment player spot counter
                    self.Player_spot += 1

                # Put number of remaining cards in the app's title bar
                root.title(f'{len(self.Deck)} cards left')
            except IndexError as e:
                root.title(f'No Cards remaining in Deck')

            # Check for blackjack
            self.blackjack_shuffle('player')

    def deal_cards(self):
        """Deal Out the Cards
        """
        try:
            # Get a Card for the Dealer
            card = random.choice(self.Deck)
            # Remove card from Deck
            self.Deck.remove(card)
            # Append card to Dealer's list
            self.Dealer.append(card)
            # Output card to screen
            # global dealer_image0
            dealer_image0 = self.resize_card(f'{card}.png')

            self.dealer_label_1.config(image=dealer_image0)
            self.dealer_label_1.config(text=card)

            # Get a Card for the Player
            card = random.choice(self.Deck)
            # Remove card from Deck
            self.Deck.remove(card)
            # Append card to Player's list
            self.Player.append(card)

            # Output card to screen
            # global player_image1
            player_image1 = self.resize_card(f'{card}.png')

            self.player_label_1.config(image=player_image1)

            # Put number of remaining cards in the app's title bar
            root.title(f'{len(self.Deck)} cards left')
        except IndexError as e:
            root.title(f'No Cards remaining in Deck')

    def run(self):
        """Start app"""
        self.root.mainloop()

    def blackjack_shuffle(self, player: str):
        """Test for Blackjack on shuffle

        Args:
            player (str): player type (player or dealer)
        """
        if player == 'dealer':
            if len(self.dealer_score_list) == 2:
                if self.dealer_score_list[0] + self.dealer_score_list[1] == 21:
                    # Update status
                    self.blackjack_status['dealer'] = 'yes'

                    # messagebox.showinfo(
                    #     'Dealer Wins!', 'BlackJack!\nDealer Wins!')
                    # # Disable buttons
                    # self.hit_me_button.config(state='disabled')
                    # self.stand_button.config(state='disabled')

        if player == 'player':
            if len(self.player_score_list) == 2:
                if self.player_score_list[0] + self.player_score_list[1] == 21:
                    self.blackjack_status['player'] = 'yes'

                    # messagebox.showinfo(
                    #     'Player Wins!', 'BlackJack!\nPlayer Wins!')
                    # # Disable buttons
                    # self.hit_me_button.config(state='disabled')
                    # self.stand_button.config(state='disabled')

        # Both player have got their two cards
        if len(self.dealer_score_list) == 2 and len(self.player_score_list) == 2:
            # Check for Push/Tie
            if self.blackjack_status['dealer'] == 'yes' and self.blackjack_status['player'] == 'yes':
                # It's a push: tie
                messagebox.showinfo('Push!', "It's a tie!")
                self.hit_me_button.config(state='disabled')
                self.stand_button.config(state='disabled')
                
            # Check for dealer win
            elif self.blackjack_status['dealer'] == 'yes':
                # Dealer wins
                messagebox.showinfo('Dealer Wins!', 'BlackJack!\nDealer Wins!')
                self.hit_me_button.config(state='disabled')
                self.stand_button.config(state='disabled')
                
            # Check for player win
            elif self.blackjack_status['player'] == 'yes':
                # Player wins
                messagebox.showinfo('Player Wins!', 'BlackJack!\nPlayer Wins!')
                self.hit_me_button.config(state='disabled')
                self.stand_button.config(state='disabled')

    def _get_card_score(self, card: str) -> int:
        """convert facecards to card number, 10 or 11

        Args:
            card (str): facecard name

        Returns:
            int: [blackjack card value]
        """
        card_number = int(card.split('_', 1)[0])
        if card_number == 14:
            return 11
        elif card_number in (11, 12, 13):
            return 10
        else:
            return card_number


if __name__ == '__main__':
    BlackJack(root).run()
