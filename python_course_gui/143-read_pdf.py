import tkinter as Tk
import PyPDF2
from tkinter import filedialog
from pathlib import Path

root = Tk.Tk()
root.title("Read a PDF")
root.iconbitmap("codemy.ico")
root.geometry('500x550')


def open_pdf():
    """ Open PDF File """
    open_file = filedialog.askopenfilename(
        initialdir=Path.cwd(),
        title="Open PDF File",
        filetypes=(
            ("PDF Files", "*.pdf"),
            ("All Files", "*.*")
        )
    )

    # Check to see if there is a file
    if open_file:
        # Open the PDF file
        pdf_file = PyPDF2.PdfFileReader(open_file)
        # Set the page to read
        page = pdf_file.getPage(0)
        # Extract the text from the PDF file
        page_content = page.extractText()

        # Add text to the textbox
        my_text.insert(1.0, page_content)


def clear_text():
    """ Clear the text box """
    my_text.delete(1.0, Tk.END)


# Create a textbox
my_text = Tk.Text(root, height=30, width=60)
my_text.pack(pady=10)

# Create a Menu
my_menu = Tk.Menu(root)
root.config(menu=my_menu)

# Add some dropdown menus
file_menu = Tk.Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="Open", command=open_pdf)
file_menu.add_command(label="Clear", command=clear_text)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)


root.mainloop()
