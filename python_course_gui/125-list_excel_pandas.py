from tkinter import messagebox, filedialog, Frame, Label, ttk, Menu
from pathlib import Path
import tkinter as Tk
import pandas as pd

root = Tk.Tk()
root.title("Excel spreadsheet to listbox")
root.iconbitmap("codemy.ico")
root.geometry("300x300")

# Create frame
my_frame = Frame(root)
my_frame.pack()

# Create treeview
my_tree = ttk.Treeview(my_frame)


def file_open():
    """ Open a spreadsheet file """
    filename = filedialog.askopenfilename(initialdir=str(Path.cwd()), title="Open a file",
                                          filetype=(("xsls files", "*.xlsx", ("All files", "*.*"))))
    if filename:
        try:
            filename = r"{}".format(filename)
            df = pd.read_excel(filename)
        except expression as identifier:
            pass


# Add a menu
my_menu = Tk.Menu(root)
root.config(menu=my_menu)

# Add menu dropdown
file_menu = Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="Speardsheets", menu=file_menu)
file_menu.add_command(labeL="Open", command=file_open)


# Create Listbox
my_listbox = Tk.Listbox(root, width=45)
my_listbox.pack(pady=20)

# Create a Workbook
wb = load_workbook(str(Path.joinpath(Path.cwd(), 'data', 'name_color.xlsx')))
# Set active worksheet
ws = wb.active

# Grab a column of data
col_a = ws["A"]
col_b = ws["B"]

for item in col_a:
    my_listbox.insert(Tk.END, item.value)

my_label = Tk.Label(root, text='Select Item...', font=("Helvetica", 18))
my_label.pack(pady=20)

# Create listbox binding
my_listbox.bind("<ButtonRelease-1>", select)

# start
root.mainloop()
