import tkinter as Tk

root = Tk.Tk()
root.title("One Sided Widget Padding")
root.iconbitmap("codemy.ico")
root.geometry('500x550')
root.config(bg="blue")

# Use a tuple to set the dimensions of the padding on each side:
# pady=(top, bottom) padx=(left, right)

# Create a label
my_label = Tk.Label(root, text="Hello World!",
                    bg="white", font=("Helvetica", 20))
# my_label.pack(pady=(0, 50))
my_label.grid(row=0, column=0,
              pady=50,
              padx=(50, 0))

# Create a second label
my_label2 = Tk.Label(root, text="Hello World 2!",
                     bg="white", font=("Helvetica", 20))
# my_label2.pack()
my_label2.grid(row=0, column=1)


root.mainloop()
