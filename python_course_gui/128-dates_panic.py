import tkinter as Tk
from datetime import date

root = Tk.Tk()
root.title("Panic Date")
root.iconbitmap("codemy.ico")
root.geometry('500x350')

panic = Tk.Label(root, text="DON'T PANIC!", font=("Helvetica", 42), bg="black", fg="green")
panic.pack(pady=20, ipadx=10, ipady=10)

# Get current date
today = date.today()

# Format Date
f_today = today.strftime("%A - %B %d, %Y")

# Output date in a label
today_label = Tk.Label(root, text=f'Today is {f_today}')
today_label.pack(pady=20)

# Countdown
days_in_year = 365
todays_day_number = int(today.strftime("%j"))
# print(todays_day_number)

# Days left in year
days_left = days_in_year - todays_day_number

# Output days left
countdown_label = Tk.Label(root, text=f'There are only {days_left} days\nleft in the current year!!', font=("Helvetica", 20))
countdown_label.pack(pady=20)

root.mainloop()