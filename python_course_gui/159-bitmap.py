from tkinter import *
from tkinter import ttk

root = Tk()
root.title("Bitmaps")
root.iconbitmap("codemy.ico")
root.geometry('320x900')

# my_button = Button(root, bitmap="error", width=50,
#                    height=50, fg="#f00")  # fg="#ce41ff")
# my_button.pack(pady=50)

"""
error
gray75
gray50
gray12
hourglass
info
questhead
question
warning
.xbm files for bitmap=""
"""
list = ["error",
        "gray75",
        "gray50",
        "gray12",
        "hourglass",
        "info",
        "questhead",
        "question",
        "warning"]

for map in list:
    Button(root, bitmap=map, width=50, height=50, fg="darkblue").pack(pady=15)


root.mainloop()
