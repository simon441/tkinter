
import locale
from pathlib import Path

import tkinter


root = tkinter.Tk()
root.title('Mortgage Calculator')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('500x400')


class Mortgage():
    """Tkinter Mortgage Calculator class"""

    def __init__(self, master: tkinter.Tk):
        self.master = master

        self.create_widgets()

    def create_widgets(self):
        """Create the App widgets"""
        my_label_frame = tkinter.LabelFrame(
            self.master, text='Motgage Calculator')
        my_label_frame.pack(pady=30)

        my_frame = tkinter.Frame(my_label_frame)
        my_frame.pack(pady=20, padx=20)

        # Define Labels and Entry boxes
        amount_label = tkinter.Label(my_frame, text='Loan Amount')
        self.amount_entry = tkinter.Entry(my_frame, font=('Helvetica', 18))

        interest_label = tkinter.Label(my_frame, text='Interest Rate')
        self.interest_entry = tkinter.Entry(my_frame, font=('Helvetica', 18))

        term_label = tkinter.Label(my_frame, text='Term (years)')
        self.term_entry = tkinter.Entry(my_frame, font=('Helvetica', 18))

        # Put Labels and Entry boxeson the screen
        amount_label.grid(row=0, column=0)
        self.amount_entry.grid(row=0, column=1)

        interest_label.grid(row=1, column=0)
        self.interest_entry.grid(row=1, column=1, pady=20)

        term_label.grid(row=2, column=0)
        self.term_entry.grid(row=2, column=1)

        self.my_button = tkinter.Button(
            my_label_frame, text='Calculate Payment', command=self.payment)
        self.my_button.pack(pady=20)

        # Output Label
        self.payment_label = tkinter.Label(
            self.master, text='', font=('Helvetica', 18))
        self.payment_label.pack(pady=20)

    def payment(self):
        """Calculate Mortagage payment"""
        if self.amount_entry.get() and self.interest_entry.get() and self.term_entry.get():
            # Convert Entry boxes to numbers
            try:
                years = int(self.term_entry.get())
                months = years * 12
            except ValueError:
                self.payment_label.config(
                    text='Hey! The Term must be a number')
                return
            try:
                rate = float(self.interest_entry.get())
            except ValueError:
                self.payment_label.config(
                    text='Hey! The Interest Rate must be a number')
                return
            try:
                loan = int(self.amount_entry.get())
            except ValueError:
                self.payment_label.config(
                    text='Hey! The Load Amount must be a number')
                return
            # Calculate the loan
            # Monthly interest rate
            monthly_rate = rate / 100 / 12
            # Get the Payment
            # payment = (
            #     monthly_rate / (1 - (1 + monthly_rate)) ** (-months)) * loan
            payment = (monthly_rate /
                       (1 - (1 + monthly_rate)**(-months))) * loan

            # Set the locale for the locale monatery formatting (eg: the default locale from your python installation)
            locale.setlocale(locale.LC_MONETARY, '')

            # Output payment to the screen, formatting the payment as money
            self.payment_label.config(
                text=f'Monthly Payment: {locale.currency(payment, grouping=True)}')

        else:
            self.payment_label.config(
                text='Hey! You forgot to enter something...')

    def run(self):
        """Start the App"""
        self.master.mainloop()


app = Mortgage(root)
# Start app
app.run()
