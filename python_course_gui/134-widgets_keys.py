import tkinter as Tk

root = Tk.Tk()
root.title("Keys method")
root.iconbitmap("codemy.ico")
root.geometry('500x550')

my_label = Tk.Label(root, text="My Label", font=("Helvetica", 18))
my_label.pack(pady=20)

my_entry = Tk.Entry(root)
my_entry.pack()

def something():
    pass

my_button = Tk.Button(root, text="click Me!", command=something)
my_button.pack()

for key in my_button.keys():
    print(key)
# get the value of an key (an attribute of the widget)
# by using array notation: <widget>[<attribute-name>]
print(my_label['relief'])
print(my_entry['relief'])
print(my_button['command'])

root.mainloop()
