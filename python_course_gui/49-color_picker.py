from tkinter import Tk, colorchooser, Button, Label

root = Tk()
root.title('Color Picker')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

def color():
    my_color = colorchooser.askcolor()[1]

    my_label = Label(root, text=my_color).pack(pady=10)
    my_label2 = Label(root, text="You picked a Color!", font=("Helvetica", 32), bg=my_color).pack(pady=10)


my_button = Button(root, text="pick a color", command=color)
my_button.pack(pady=20)

# Launch GUI
root.mainloop()