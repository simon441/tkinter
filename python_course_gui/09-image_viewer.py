from tkinter import *
from PIL import ImageTk, Image
import os
from pathlib import Path

root = Tk()

# Gets the requested values of the height and widht.
windowWidth = root.winfo_reqwidth()
windowHeight = root.winfo_reqheight()
# print("Width",windowWidth,"Height",windowHeight)
 
# Gets both half the screen width/height and window width/height
positionRight = int(root.winfo_screenwidth()/3 - windowWidth/2)
positionDown = int(root.winfo_screenheight()/3 - windowHeight/2)

# Positions the window in the center of the page.
root.geometry("+{}+{}".format(positionRight, positionDown))

root.title("Simon Image Viewer")
root.iconbitmap('codemy.ico')


my_img1 = ImageTk.PhotoImage(Image.open("images/aspen.png"))
my_img2 = ImageTk.PhotoImage(Image.open("images/aspen2.png"))
my_img3 = ImageTk.PhotoImage(Image.open("images/me.png"))
my_img4 = ImageTk.PhotoImage(Image.open("images/me1.png"))
my_img5 = ImageTk.PhotoImage(Image.open("images/me2.png"))
my_img5 = ImageTk.PhotoImage(Image.open("images/me3.png"))

image_list = [my_img1, my_img2, my_img3, my_img4, my_img5]

dir_list = os.listdir('images')
# print(dir_list)

image_list.clear()
for image in dir_list:
    my_img = ImageTk.PhotoImage(Image.open("images/" + image))
    image_list.append(my_img)

my_label = Label(image=my_img1)
my_label = Label(image=image_list[0])
my_label.grid(row=0, column=0, columnspan=3)

# Status bar (at the bottom)
status = Label(root, text="Image 1 of " + str(len(image_list)), bd=1, relief=SUNKEN, anchor=E)

def forward(image_number):
    """ Move to next image """
    global my_label
    global button_forwards
    global button_back

    my_label.grid_forget()
    my_label = Label(image=image_list[image_number - 1])
    button_forwards = Button(root, text=">>", command=lambda: forward(image_number + 1))
    button_back = Button(root, text="<<", command=lambda: back(image_number - 1))

    if image_number == len(image_list):
        button_forwards = Button(root, text=">>", state=DISABLED)

    my_label.grid(row=0, column=0, columnspan=3)
    button_back.grid(row=1, column=0)
    button_exit.grid(row=1, column=1)
    button_forwards.grid(row=1, column=2)

    # Update Status bar (at the bottom)
    status = Label(root, text="Image " + str(image_number) + " of " + str(len(image_list)), bd=1, relief=SUNKEN, anchor=E)
    status.grid(row=2, column=0, columnspan=3, stick=W+E, padx=5)

def back(image_number):
    """ Move to previous image """
    global my_label
    global button_forwards
    global button_back

    my_label.grid_forget()
    my_label = Label(image=image_list[image_number - 1])
    my_label.grid(row=0, column=0, columnspan=3)

    button_forwards = Button(root, text=">>", command=lambda: forward(image_number + 1))
    button_back = Button(root, text="<<", command=lambda: back(image_number - 1))

    if image_number == 1:
        button_back = Button(root, text="<<", state=DISABLED)
    
    button_back.grid(row=1, column=0)
    button_exit.grid(row=1, column=1)
    button_forwards.grid(row=1, column=2)

    # Update Status bar (at the bottom)
    status = Label(root, text="Image " + str(image_number) + " of " + str(len(image_list)), bd=1, relief=SUNKEN, anchor=E)
    status.grid(row=2, column=0, columnspan=3, stick=W+E, padx=5)

button_forwards = Button(text=">>", command=lambda: forward(2))
button_back = Button(text="<<", command=back, state=DISABLED)
button_exit = Button(text="Exit Viewer", command=root.quit)

button_back.grid(row=1, column=0)
button_exit.grid(row=1, column=1)
button_forwards.grid(row=1, column=2, pady=10)

status.grid(row=2, column=0, columnspan=3, stick=W+E, padx=5)

root.mainloop()