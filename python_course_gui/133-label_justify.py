import tkinter as Tk

root = Tk.Tk()
root.title("Justify labels")
root.iconbitmap("codemy.ico")
root.geometry('500x550')

my_label1 = Tk.Label(root,
    text="Stuff\nMore stuff\nagain more stuff",
    font=("Helvetica", 18),
    bd=1,
    relief=Tk.SUNKEN
)
my_label1.pack(pady=20, ipady=10, ipadx=10)

my_label2 = Tk.Label(root,
    text="Stuff\nMore stuff\nagain more stuff",
    font=("Helvetica", 18),
    bd=1,
    relief=Tk.SUNKEN,
    justify="left"
)
my_label2.pack(pady=20, ipady=10, ipadx=10)


my_label3 = Tk.Label(root,
    text="Stuff\nMore stuff\nagain more stuff",
    font=("Helvetica", 18),
    bd=1,
    relief=Tk.SUNKEN,
    justify="right"
)
my_label3.pack(pady=20, ipady=10, ipadx=10)

my_label4 = Tk.Label(root,
    text="Stuff\nMore stuff\nagain more stuff",
    font=("Helvetica", 18),
    bd=1,
    relief=Tk.SUNKEN,
    justify="center"
)
my_label4.pack(pady=20, ipady=10, ipadx=10)

root.mainloop()
