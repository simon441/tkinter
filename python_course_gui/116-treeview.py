from tkinter import *
from tkinter import ttk
from os import path

root = Tk()
root.title("Treeview")
root.iconbitmap("codemy.ico")
root.geometry("500x500")

my_tree = ttk.Treeview(root)

# define the columns
my_tree['columns'] = ("Name", "ID", "Favorite Pizza")

# Format the columns
# width > 00 => show phantom column
my_tree.column("#0", width=120, minwidth=25)
my_tree.column("Name", anchor=W, width=120)
my_tree.column("ID", anchor=CENTER, width=80)
my_tree.column("Favorite Pizza", anchor=W, width=150)

# no phantom column
my_tree.column("#0", width=0, stretch=NO)

# Create Headings
my_tree.heading("#0", text="Label", anchor=W)

my_tree.heading("Name", text="Name", anchor=W)
my_tree.heading("ID", text="Id", anchor=CENTER)
my_tree.heading("Favorite Pizza", text="Favorite Pizza", anchor=W)

# remove phantom
my_tree.heading("#0", text="", anchor=W)

# Add Data
data = [
    ["John", 1, "Pepperoni"],
    ["Mary", 2, "Cheese"],
    ["Tina", 3, "Ham"],
    ["Bob", 4, "Supreme"],
    ["Erin", 5, "Cheese"],
    ["Wes", 6, "Onion"]
]

count = 0
for record in data:
    # my_tree.insert(parent="", index=END, iid=count, text="", values=record)
    my_tree.insert(parent="", index=END, iid=count, text="",
                   values=(record[0], record[1], record[2]))
    count += 1


# Add Data explicitely row by row
""" my_tree.insert(parent="", index='end', iid=0, text="",
               values=("John", 1, "Pepperoni",))
my_tree.insert(parent="", index='end', iid=1,
               text="", values=("Mary", 2, "Cheese"))
my_tree.insert(parent="", index='end', iid=2,
               3text="", values=("Tina", 3, "Ham"))
my_tree.insert(parent="", index='end', iid=3,
               text="", values=("Bob", 4, "Supreme"))
my_tree.insert(parent="", index='end', iid=4,
               text="", values=("Erin", 5, "Cheese",))
my_tree.insert(parent="", index='end', iid=5,
               text="", values=("Wes", 6, "Onion",)) """

# Add Data with phantom column
""" my_tree.insert(parent="", index='end', iid=0, text="Parent",
               values=("John", 1, "Pepperoni",))
my_tree.insert(parent="", index='end', iid=1,
               text="Parent", values=("Mary", 2, "Cheese"))
my_tree.insert(parent="", index='end', iid=2,
               text="Parent", values=("Tina", 3, "Ham"))
my_tree.insert(parent="", index='end', iid=3,
               text="Parent", values=("Bob", 4, "Supreme"))
my_tree.insert(parent="", index='end', iid=4,
               text="Parent", values=("Erin", 5, "Cheese",))
my_tree.insert(parent="", index='end', iid=5,
               text="Parent", values=("Wes", 6, "Onion",))
# Child
my_tree.insert(parent="", index='end', iid=6, text="Child",
               values=("Steve", "1.2", "Peppers"))
my_tree.move('6', '0', '0') """

# Pack to the screen
my_tree.pack(pady=20)


# start
root.mainloop()
