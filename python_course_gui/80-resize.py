from tkinter import Button, Tk, Entry, Label

root = Tk()
root.title("Resize A Window Dynamically")
root.iconbitmap('codemy.ico')
root.geometry("600x400")


def resize():
    w = width_entry.get()
    h = height_entry.get()
    # root.geometry(str(w) + 'x' + str(h))
    # root.geometry("{width}x{height}".format(width=w, height=h)
    # root.geometry(f"Mix%i" % (w, h))
    root.geometry(f"{w}x{h}")


title_label = Label(root, text="Resize The Window", font=("Helvetica", 14))
title_label.pack(pady=(20, 10))

width_label = Label(root, text="Width:")
width_label.pack(pady=20)
width_entry = Entry(root)
width_entry.pack()

height_label = Label(root, text="Height:")
height_label.pack(pady=20)
height_entry = Entry(root)
height_entry.pack()

my_button = Button(root, text="Resize", command=resize)
my_button.pack(pady=20)

root.mainloop()
