import tkinter as Tk
from namer import nameit

root = Tk.Tk()
root.title("Other Files")
root.iconbitmap("codemy.ico")
root.geometry('500x350')

greet = nameit("Simon")

print(greet)

def submit():
    greet = nameit(my_box.get())
    my_label.config(text=greet)

my_box = Tk.Entry(root)
my_box.pack(pady=20)

my_label = Tk.Label(root, text="", font=("Helvetica", 18))
my_label.pack(pady=20)

my_button = Tk.Button(root, text="Submit", command=submit)
my_button.pack(pady=20)

root.mainloop()