import tkinter as Tk
from tkinter.font import Font, families

root = Tk.Tk()
root.title("ToDo List!")
root.iconbitmap("codemy.ico")
root.geometry('600x500')

# Define the font
my_font = Font(
    family="Comic Sans MS",
    # family="Brush Script MT",
    size=28,
    weight="bold"
)


# Create frame
my_frame = Tk.Frame(root)
my_frame.pack(pady=10)

btn = Tk.Button(root)
systemface = btn['bg']
print(systemface)
# Create listbox
# my_list = Tk.Listbox(my_frame,
#  font=my_font,
#  width=25,
#  height=5,
#  bg="SystemButtonFace",
#  bd=0,
#  fg="#464646",

#  )
my_list = Tk.Listbox(my_frame,
                     font=my_font,
                     width=25,
                     height=5,
                     bg="SystemButtonFace",
                     bd=0,
                     fg="#464646",
                     highlightthickness=0,
                     selectbackground="#a6a6a6",
                     activestyle="none")
my_list.pack(side=Tk.LEFT, fill=Tk.BOTH)

# Create dummy list
stuff = ["Walk The Dog", "Buy Groceries", "Learn Tkinter", "Rule The World"]
# Add dummy list to list box
for item in stuff:
    my_list.insert(Tk.END, item)
# for name in sorted(families()):
#     print(name)

# Create scrollbar
scrollbar = Tk.Scrollbar(my_frame)
scrollbar.pack(side=Tk.RIGHT, fill=Tk.BOTH)

# Add scrollbar
my_list.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=my_list.yview)

# Create entry box to add items to the list
my_entry = Tk.Entry(root, font=("Helvetica", 24))
my_entry.pack(pady=20)

# Create a button frame
button_frame = Tk.Frame(root)
button_frame.pack(pady=20)

# Functions


def delete_item():
    """
    Delete an item from the list
    """
    my_list.delete(Tk.ANCHOR)


def add_item():
    """
    Add an item to the list
    """
    my_list.insert(Tk.END, my_entry.get())
    my_entry.delete(0, Tk.END)


def done_item():
    """
    Cross an item from the list when done
    """
    pass


def waiting_item():
    """
    Remove the cross from an item from the list
    """
    pass


# Add some buttons
delete_button = Tk.Button(
    button_frame, text="Delete Item", command=delete_item)
add_button = Tk.Button(
    button_frame, text="Add Item", command=add_item)
cross_off_button = Tk.Button(
    button_frame, text="Cross off Item", command=done_item)
uncross_button = Tk.Button(
    button_frame, text="Cross off Item", command=waiting_item)

delete_button.grid(row=0, column=0)
add_button.grid(row=0, column=1, padx=20)
cross_off_button.grid(row=0, column=2)
uncross_button.grid(row=0, column=3, padx=20)

root.mainloop()
