from tkinter import *
from PIL import ImageTk, Image
from tkinter import filedialog
import os

root = Tk()

root.title("Simon")
root.iconbitmap('codemy.ico')
root.geometry("400x400")

def show():
    my_label = Label(root, text=var.get()).pack()

var = IntVar()

c = Checkbutton(root, text="Check this box", variable=var)
c.pack()

my_button = Button(root, text="Show Selection", command=show).pack()

def show_size():
    Label(root, text=var2.get()).pack()

var2 = StringVar()

Label(root, text="Your order", font=("Arial, Helvetica", 17)).pack(pady=20)

# StringVar is selected by default
c2 = Checkbutton(root, text="Would you like to SuperSize your order?", variable=var2, onvalue="Supersize", offvalue="RegularSize")
c2.deselect()
c2.pack()


my_button2 = Button(root, text="Show Selection", command=show_size).pack()

root.mainloop()
