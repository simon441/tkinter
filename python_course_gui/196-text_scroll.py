from os import path

import tkinter as tk

root = tk.Tk()
root.title('Multiple Text Scrolls')
root.iconbitmap(path.join(path.abspath(__file__), '..', 'codemy.ico'))
root.geometry('600x500')

# This is an example of what can be done to connect
# multiple tkinter widgets together: Label, Text, Entry, ...


def multiple_yview(*args: tuple) -> None:
    """Yview function
        :*args (tuple) :scrollbar positions (moveto position:float)

    """
    my_text1.yview(*args)
    my_text2.yview(*args)
    print(*args, type(args))


# Frame
my_frame = tk.Frame(root)
my_frame.pack(pady=20)

# Scrollbar
text_scroll = tk.Scrollbar(my_frame)
text_scroll.pack(side=tk.RIGHT, fill=tk.Y)

# Create Two Text Boxes
my_text1 = tk.Text(my_frame, width=20, height=25, font=(
    'Helvetica', 16), yscrollcommand=text_scroll.set, wrap=tk.NONE)
my_text1.pack(side=tk.RIGHT, padx=5)

my_text2 = tk.Text(my_frame, width=20, height=25, font=(
    'Helvetica', 16), yscrollcommand=text_scroll.set, wrap=tk.NONE)
my_text2.pack(side=tk.LEFT)


# Configure scrollbar
text_scroll.config(command=multiple_yview)

root.mainloop()
