from tkinter import *
from tkhtmlview import HTMLLabel



root = Tk()
root.title("Using HTML")
root.iconbitmap("codemy.ico")
root.geometry('500x600')

my_label= HTMLLabel(root, html='<pre><h1>Hello World!</h1></pre>\
    <p><a href="https://localhost.com">Learn to Code!</a></p>\
        <p><ul>\
        <li>One</li>\
        <li>Two</li>\
        <li>Three</li>\
        </ul></p>\
        <br><hr><blockquote><code>my_variable = \'Hello world\'\
        print(my_variable)</code></blockquote><hr><br>\
        <p><img src="images/aspen.png" alt="My photo"/></p>')
my_label.pack(pady=20, padx=20, fill=BOTH, expand=True)


root.mainloop()