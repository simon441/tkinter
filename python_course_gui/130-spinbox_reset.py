import tkinter as Tk

root = Tk.Tk()
root.title("Reset spinbox")
root.iconbitmap("codemy.ico")
root.geometry('500x350')

def reset():
    """
    reset the spinner
    """
    my_var = Tk.IntVar(root)
    my_var.set(0)
    my_spin.config(textvariable=my_var)
    var2 = Tk.IntVar(root)
    var2.set("Mary")
    my_spin2.config(textvariable=var2)

# set IntVar
my_var = Tk.IntVar(root)
my_var.set(20)

# Set StringVar
var2 = Tk.StringVar(root)
var2.set("John")

# spin box
my_spin = Tk.Spinbox(root, from_=0, to=100, increment=2, font=("Helvetica", 20), textvariable=my_var)
my_spin.pack(pady=20)
my_spin2 = Tk.Spinbox(root, values=("John", "Simon", "Jane", "Samentha", "Mary", "Tina"), font=("Helvetica", 20), textvariable=var2)
my_spin2.pack(pady=20)

# button to reset the spinner
my_button = Tk.Button(root, text="Reset", font=("Helvetica", 12), command=reset)
my_button.pack(pady=20)

root.mainloop()