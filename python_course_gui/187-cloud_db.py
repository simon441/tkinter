from tkinter import *
import psycopg2
from dotenv import dotenv_values

config = dotenv_values('.env')

WIDTH = 500
HEIGHT = 550

root = Tk()
root.title('PostGres Cloud App')
root.iconbitmap('codemy.ico')
root.geometry(f"{WIDTH}x{HEIGHT}")


def heroku_conn():
    return psycopg2.connect(
        host="ec2-18-214-238-28.compute-1.amazonaws.com",
        database="d7ftscpdn8sjbk",
        user="ihddwdertvsmoh",
        password="2c2f5bf06563f8138aa45744cdaf8ca650d86ff4bc0344fe7298461ce451f456",
        port="5432",
    )



def local_conn():
    return psycopg2.connect(
        host="localhost",
        database="py_cloud_app",
        user=config.get('POSTGRES_LOCAL_USER'),
        password=config.get('POSTGRES_LOCAL_PASSWORD'),
        port="5432",
    )


class PostGresCloupApp():

    def __init__(self, master: Tk, *args, **kwargs):
        self.master = master
        self.create_widgets()
        self.query_initialize_app()

    def create_widgets(self):
        """ Create The GUI For The App """

        self.my_frame = LabelFrame(self.master, text="Postgres Example")
        self.my_frame.pack(pady=20)

        self.f_label = Label(self.my_frame, text="First Name:")
        self.f_label.grid(row=0, column=0, pady=10, padx=10)

        self.f_name = Entry(self.my_frame, font=("Helvetica, 18"))
        self.f_name.grid(row=0, column=1, pady=10, padx=10)

        self.l_label = Label(self.my_frame, text="Last Name:")
        self.l_label.grid(row=1, column=0, pady=10, padx=10)

        self.l_name = Entry(self.my_frame, font=("Helvetica, 18"))
        self.l_name.grid(row=1, column=1, pady=10, padx=10)

        self.submit_button = Button(
            self.my_frame, text="Submit", command=self.submit)
        self.submit_button.grid(row=2, column=0, pady=10, padx=10)

        self.update_button = Button(
            self.my_frame, text="Update", command=self.update)
        self.update_button.grid(row=2, column=1, pady=10, padx=10)

        self.output_label = Label(self.master, text="")
        self.output_label.pack(pady=50)

    def run(self):
        """ Run App"""
        self.master.mainloop()

    def connect(self):
        """ Conect to database """
        self.conn = local_conn()

        # Create a cursor
        self.cursor = self.conn.cursor()

    def query_initialize_app(self):
        """ Configure and connect to Postgres """
        self.connect()

        # Create a Table
        self.cursor.execute("""
        CREATE TABLE IF NOT EXISTS customers (
        first_name TEXT,
        last_name TEXT
    );
        """)
        self.conn.commit()
        self.conn.close()

    def submit(self):
        """ Submit """
        self.connect()

        # Insert data into table
        fname = self.f_name.get().strip()
        lname = self.l_name.get().strip()
        self.cursor.execute('''
            INSERT INTO customers(first_name, last_name)
            VALUES(%s, %s);
        ''', (fname, lname,))

        self.conn.commit()
        self.conn.close()

        self.update()
        self.clear()

    def update(self):
        """ Update """
        self.connect()

        # Grab all data from the Database
        self.cursor.execute('SELECT * FROM customers')
        records = self.cursor.fetchall()

        output = ''
        for record in records:
            self.output_label.config(text=f'{output}\n{record[0]} {record[1]}')
            output += self.output_label['text']

        self.conn.close()

    def clear(self):
        """ Clear entry boxes """
        self.f_name.delete(0, END)
        self.l_name.delete(0, END)


if __name__ == '__main__':
    app = PostGresCloupApp(root)
    app.run()
