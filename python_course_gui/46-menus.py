from tkinter import *
from tkinter import messagebox

root = Tk()
root.title('Menu App')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# Initialize menu
my_menu = Menu(root)
root.config(menu=my_menu)


def our_command():
    """ command for menu action """
    my_label = Label(root, text="You click a Dropdown menu!")
    my_label.pack()

def about_command():
    """ About App command """
    messagebox.showinfo('About App', 'App created by Simon Cateau\n\nTemplate to show menu creation in tKinter in python\n@copyright 2020')

##########
# MENU
##########

# Create a File menu item
file_menu = Menu(my_menu)
my_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New...", command=our_command)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)


# Create an Edit menu item
edit_menu = Menu(my_menu)
my_menu.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Undo", command=our_command)
edit_menu.add_command(label="Redo", command=our_command)
edit_menu.add_separator()
edit_menu.add_command(label="Cut", command=our_command)
edit_menu.add_command(label="Copy", command=our_command)
edit_menu.add_command(label="Paste", command=our_command)


# Create an Options menu item
options_menu = Menu(my_menu)
my_menu.add_cascade(label="Options", menu=options_menu)
options_menu.add_command(label="Find", command=our_command)
options_menu.add_command(label="Find Next", command=our_command)


# Create a Help menu item
help_menu = Menu(my_menu)
my_menu.add_cascade(label="?", menu=help_menu)
help_menu.add_command(label="Help", command=our_command)
help_menu.add_command(label="About", command=about_command)

# Launch GUI
root.mainloop()