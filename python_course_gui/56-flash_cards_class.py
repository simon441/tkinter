from tkinter import Tk, Label, Frame, Menu, BOTH, messagebox, Button, Entry
from tkinter import * 
from PIL import ImageTk, Image
from random import randint
from os import listdir, path


root = Tk()
root.title('Flashcards!')
root.iconbitmap('codemy.ico')
root.geometry("500x600")




class FlashCards():
    
    # STATES_DIR = 'states'
    STATES_DIR = 'all_states/rem'
    
    def __init__(self, master):

        
        self.STATES_LIST_GEOGRAPH = self._state_listing()

        ############
        # MENU
        ############

        # Create our menu
        my_menu = Menu(master)
        master.config(menu=my_menu)

        # Create Geography menu items
        states_menu = Menu(my_menu)
        my_menu.add_cascade(label="Geography", menu=states_menu)

        states_menu.add_command(label="States", command=self.states)
        states_menu.add_command(label="State Capitals", command=self.state_capitals)
        states_menu.add_separator()
        states_menu.add_command(label="Exit", command=master.quit)

        ###########
        # Frames
        ###########

        self.state_frame = Frame(root, width=500, height=500)
        self.state_capitals_frame = Frame(root, width=500, height=500)

        
    #############
    # FUNCTIONS
    #############

    def random_state(self):
        """ Generate a random state from the state list (from the state image dir) 
            show_state [tKinter.Label]: label to show if the answer is correct or not
        """

        # Generate a random number
        random_state = randint(0, len(self.STATES_LIST_GEOGRAPH) -1)

        self.selected_state = self.STATES_LIST_GEOGRAPH[random_state]
        print('random_state', self.selected_state)


        state_path = FlashCards.STATES_DIR + '/' + self.selected_state + '.png'
        print(state_path)
        if not path.isfile(state_path):
            messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
            return

        # Create ou state images
        self.state_image = ImageTk.PhotoImage(Image.open(state_path))
        self.show_state.config(image=self.state_image, bg="white")
        return self.selected_state


    def state_answer(self):
        """ Verify if answer is correct function 
            answer_label [String]: Label to display text if correct or not
            answer_input [String]: input by the user to be verified
            valid_answer [String]: Valid answer to compare to user input
        """
        # print(answer)
        answer = self.answer_input.get().replace(' ', '').replace('-', '').lower()
        valid_answer = self._get_name(self.selected_state, is_showed=False).lower()

        print(answer, valid_answer)

        # Determine if the answer is right or wrong
        if answer == valid_answer:
            response = "Correct! " + self._get_name(self.selected_state, is_showed=True).title()
        else:
            response = "Incorrect! " + self._get_name(self.selected_state, is_showed=True).title()
        
        self.answer_label.config(text=response)

        # Refreash to next card
        self.random_state()

        self.answer_input.delete(0, END)


    def states(self):
        """ State Flashcard Function """
        # hide all frames
        self.hide_all_frames()
        self.state_frame.pack(fill=BOTH, expand=1)
        # my_label = Label(state_frame, text="States").pack()


        # # Generate a random number
        # random_state = randint(0, len(STATES_LIST_GEOGRAPH) -1)

        # selected_state = STATES_LIST_GEOGRAPH[random_state]



        # state_path = FlashCards.STATES_DIR + '/' + selected_state + '.png'
        # # print(state_path)
        # if not path.isfile(state_path):
        #     messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
        #     return

        # # Create ou state images
        # global self.state_image
        # self.state_image = ImageTk.PhotoImage(Image.open(state_path))
        # global show_state
        self.show_state = Label(self.state_frame)
        self.show_state.pack(pady=15)

        self.selected_state = self.random_state()

        # Answer imput box
        # global answer_input
        self.answer_input = Entry(self.state_frame, font=('Helvetice', 18), bd=2)
        self.answer_input.pack(pady=15)
        
        # Button to Randomize State Images
        random_state_button = Button(self.state_frame, text="Pass", command=self.states)
        random_state_button.pack(pady=10)

        # Label to Tell us if we got the answer right or not
        self.answer_label = Label(self.state_frame, text="", font=('Helvetice', 18), bg="white")

        # Button to answer the Question
        answer_button = Button(self.state_frame, text="Answer", command=self.state_answer)
        answer_button.pack(pady=5)

        self.answer_label.pack(pady=15)


    def state_capitals(self):
        """ State capitals Flashcard Function """
        # hide all frames
        self.hide_all_frames()
        self.state_capitals_frame.pack(fill=BOTH, expand=1)
        my_label = Label(self.state_capitals_frame, text="Capitals")
        my_label.pack()


    def hide_all_frames(self):
        """ Hide all frames """

        # Loop through all the children in each frame and delete them
        for widget in self.state_frame.winfo_children():
            widget.destroy()

        for widget in self.state_capitals_frame.winfo_children():
            widget.destroy()

        # hide frames
        self.state_frame.pack_forget()
        self.state_capitals_frame.pack_forget()

    def _state_listing(self):
        # Create a list of state names
        files_list = listdir(FlashCards.STATES_DIR)
        # print(files_list)
        our_states = [x.replace('.png', '') for x in files_list]
        # print(states)
        return our_states
    
    def _get_name(self, name, is_showed = False):
        if is_showed:
            return name.replace('+', ' ')
        return name.replace('+', '')

app = FlashCards(root)

# Launch GUI
root.mainloop()