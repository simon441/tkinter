from tkinter import Tk, Label, Frame, Menu, BOTH, messagebox, Button, Entry
from tkinter import * 
from PIL import ImageTk, Image
from random import randint, shuffle
from os import listdir, path


root = Tk()
root.title('Flashcards!')
root.iconbitmap('codemy.ico')
root.geometry("500x600")




class FlashCards():
    
    # STATES_DIR = 'states'
    STATES_DIR = 'all_states/rem'


    states_list = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        # 'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        # 'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        # 'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        # 'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        # 'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        # 'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        # 'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
    }
    state_capitals_list = {"Washington":"Olympia","Oregon":"Salem",\
                        "California":"Sacramento","Ohio":"Columbus",\
                        "Nebraska":"Lincoln","Colorado":"Denver",\
                        "Michigan":"Lansing","Massachusetts":"Boston",\
                        "Florida":"Tallahassee","Texas":"Austin",\
                        "Oklahoma":"Oklahoma City","Hawaii":"Honolulu",\
                        "Alaska":"Juneau","Utah":"Salt Lake City",\
                        "New Mexico":"Santa Fe","North Dakota":"Bismarck",\
                        "South Dakota":"Pierre","West Virginia":"Charleston",\
                        "Virginia":"Richmond","New Jersey":"Trenton",\
                        "Minnesota":"Saint Paul","Illinois":"Springfield",\
                        "Indiana":"Indianapolis","Kentucky":"Frankfort",\
                        "Tennessee":"Nashville","Georgia":"Atlanta",\
                        "Alabama":"Montgomery","Mississippi":"Jackson",\
                        "North Carolina":"Raleigh","South Carolina":"District Of Columbia",\
                        "Maine":"Augusta","Vermont":"Montpelier",\
                        "New Hampshire":"Concord","Connecticut":"Hartford",\
                        "Rhode Island":"Providence","Wyoming":"Cheyenne",\
                        "Montana":"Helena","Kansas":"Topeka",\
                        "Iowa":"Des Moines","Pennsylvania":"Harrisburg",\
                        "Maryland":"Annapolis","Missouri":"Jefferson City",\
                        "Arizona":"Phoenix","Nevada":"Carson City",\
                        "New York":"Albany","Wisconsin":"Madison",\
                        "Delaware":"Dover","Idaho":"Boise",\
                        "Arkansas":"Little Rock","Louisiana":"Baton Rouge"}
    
    STATES_MAX_ANSWER = 3
    
    def __init__(self, master, base_path):

        self.base_path = base_path
        
        self.STATES_LIST_GEOGRAPH = self._state_listing()

        ############
        # MENU
        ############

        # Create our menu
        my_menu = Menu(master)
        master.config(menu=my_menu)

        # Create Geography menu items
        states_menu = Menu(my_menu)
        my_menu.add_cascade(label="Geography", menu=states_menu)

        states_menu.add_command(label="States", command=self.states)
        states_menu.add_command(label="State Capitals", command=self.state_capitals)
        states_menu.add_separator()
        states_menu.add_command(label="Exit", command=master.quit)

        # Math Flashcard Menu
        math_menu = Menu(my_menu)
        my_menu.add_cascade(label="Math", menu=math_menu)

        math_menu.add_cascade(label="Addition", command=self.add)

        ###########
        # Frames
        ###########

        # State Frame
        self.state_frame = Frame(master, width=500, height=500, bg="white")
        #State Capital Frame
        self.state_capitals_frame = Frame(master, width=500, height=500)
        # Addition Frame
        self.addition_frame = Frame(master, width=500, height=500)

        self.answer = ''

        
    #############
    # FUNCTIONS
    #############

    def random_state(self):
        """ Generate a random state from the state list (from the state image dir) 
            show_state [tKinter.Label]: label to show if the answer is correct or not
        """

        # Generate a random number
        random_state = randint(0, len(self.STATES_LIST_GEOGRAPH) -1)

        self.selected_state = self.STATES_LIST_GEOGRAPH[random_state]
        # print('random_state', self.selected_state)


        state_path = FlashCards.STATES_DIR + '/' + self.selected_state + '.png'
        # print(state_path)
        if not path.isfile(state_path):
            messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
            return

        # Create ou state images
        self.state_image = ImageTk.PhotoImage(Image.open(state_path))
        self.show_state.config(image=self.state_image, bg="white")
        return self.selected_state


    def state_answer(self):
        """ Verify if answer is correct function 
            answer_label [String]: Label to display text if correct or not
            answer_input [String]: input by the user to be verified
            valid_answer [String]: Valid answer to compare to user input
        """
        # print(answer)
        answer = self.answer_input.get().replace(' ', '').replace('-', '').lower()
        valid_answer = self._get_name(self.selected_state, is_showed=False).lower()

        # print(answer, valid_answer)

        # Determine if the answer is right or wrong
        if answer == valid_answer:
            response = "Correct! "
        else:
            response = "Incorrect! "
        
        self.answer_label.config(text=response + self._get_name(self.selected_state, is_showed=True).title())

        # Refreash to next card
        self.random_state()

        self.answer_input.delete(0, END)



    def state_capital_answer(self, input_answer, our_states_capitals):
        """ Validate the answer entered by the user """
        
        if input_answer == our_states_capitals[self.answer]:
            response = "Correct!"
        else:
            response = "Incorrect!"
        self.capital_answer_label.config(text=response + ' ' + our_states_capitals[self.answer].title() + ' is the state capital of ' + self.answer.title())


    def states(self):
        """ State Flashcard Function """
        # hide all frames
        self.hide_all_frames()
        self.state_frame.pack(fill=BOTH, expand=1)
        # my_label = Label(state_frame, text="States").pack()


        # # Generate a random number
        # random_state = randint(0, len(STATES_LIST_GEOGRAPH) -1)

        # selected_state = STATES_LIST_GEOGRAPH[random_state]



        # state_path = FlashCards.STATES_DIR + '/' + selected_state + '.png'
        # # print(state_path)
        # if not path.isfile(state_path):
        #     messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
        #     return

        # # Create ou state images
        # global self.state_image
        # self.state_image = ImageTk.PhotoImage(Image.open(state_path))
        # global show_state
        self.show_state = Label(self.state_frame)
        self.show_state.pack(pady=15)

        self.selected_state = self.random_state()

        # Answer imput box
        # global answer_input
        self.answer_input = Entry(self.state_frame, font=('Helvetice', 18), bd=2)
        self.answer_input.pack(pady=15)
        
        # Button to Randomize State Images
        random_state_button = Button(self.state_frame, text="Pass", command=self.states)
        random_state_button.pack(pady=10)

        # Label to Tell us if we got the answer right or not
        self.answer_label = Label(self.state_frame, text="", font=('Helvetice', 18), bg="white")

        # Button to answer the Question
        answer_button = Button(self.state_frame, text="Answer", command=self.state_answer)
        answer_button.pack(pady=5)

        self.answer_label.pack(pady=15)
    def _generate_capitals(self, our_states):

        answer_list = []
        self.answer = ''
        
        # Generate state capitals
        for i in range(self.STATES_MAX_ANSWER):
            # Randomly select our state
            rando = randint(0, len(our_states) - 1)

            # If first selection, make it our answer
            if i == 0:
                self.answer = our_states[rando]
                state_path = self.base_path + '/' + FlashCards.STATES_DIR + '/' + our_states[rando].replace(' ', '+')  + '.png'
                if not path.exists(state_path):
                    self._generate_capitals(our_states)
                self.state_image = ImageTk.PhotoImage(Image.open(state_path))
                # print('self.state_imzge', self.state_image)
                self.show_state.config(image=self.state_image)

            # add our random selection to our answer_list list 
            answer_list.append(our_states[rando])

            # remove from old list
            our_states.remove(our_states[rando])

            # Shuffle the original list
            shuffle(our_states)
        shuffle(answer_list)
        # print(answer_list, rando)
        return [our_states, answer_list]
    


    def state_capitals(self):
        """ State capitals Flashcard Function """
        # hide all frames
        self.hide_all_frames()
        self.state_capitals_frame.pack(fill=BOTH, expand=1)
        # my_label = Label(self.state_capitals_frame, text="Capitals")
        # my_label.pack()
        # create a show state
        self.show_state = Label(self.state_capitals_frame)
        self.show_state.pack(pady=15)

        answer_list = []
        
        our_states = list(self.states_list.values())#list(map(lambda x: x.lower(), list(states.values())))

        our_states_capitals = FlashCards.state_capitals_list

        count = 1
        # print(our_states)

        # Generate state capitals
        # for i in range(self.STATES_MAX_ANSWER):
        #     # Randomly select our state
        #     rando = randint(0, len(our_states) - 1)

        #     # If first selection, make it our answer
        #     if i == 0:
        #         answer = our_states[rando]
        #         state_path = self.base_path + '/' + FlashCards.STATES_DIR + '/' + our_states[rando].replace(' ', '+')  + '.png'
        #         self.state_image = ImageTk.PhotoImage(Image.open(state_path))
        #         print('self.state_imzge', self.state_image)
        #         self.show_state.config(image=self.state_image)

        #     # add our random selection to our answer_list list 
        #     answer_list.append(our_states[rando])

        #     # remove from old list
        #     our_states.remove(our_states[rando])

        #     # Shuffle the original list
        #     shuffle(our_states)
        # shuffle(our_states)

        [our_states, answer_list] = self._generate_capitals(our_states)

        capital_radio = StringVar()
        capital_radio.set(our_states_capitals[answer_list[0]])

        capital_radio_button = []
        # print("###############", our_states_capitals[self.answer])

        # print(answer_list[0], our_states_capitals)
        # for i in range(0, self.STATES_MAX_ANSWER):
        #     capital_radio_button.append(Radiobutton(self.state_capitals_frame, text=our_states_capitals[answer_list[i]], variable=capital_radio, value=i).pack())
        for i in range(0, len(answer_list)):
            capital_radio_button.append(Radiobutton(self.state_capitals_frame, text=our_states_capitals[answer_list[i]].title(), variable=capital_radio, value=our_states_capitals[answer_list[i]]).pack())
        
        # Add a Pass Button
        pass_button = Button(self.state_capitals_frame, text="Pass", command=self.state_capitals)
        pass_button.pack(pady=15)

        capital_answer_button = Button(self.state_capitals_frame, text="Answer", command=lambda: self.state_capital_answer(capital_radio.get(), our_states_capitals))
        capital_answer_button.pack(pady=15)

        # Answer label
        self.capital_answer_label = Label(self.state_capitals_frame, text='', font=("Helvetica", 18))
        self.capital_answer_label.pack(pady=15)

    def add(self):
        """ Addition Math Flashcard """
        self.hide_all_frames()

        self.addition_frame.pack(fill=BOTH, expand=1)

        add_label = Label(self.addition_frame, text="Addition Flashcard", font=("Helvetica", 18))
        add_label.pack(pady=20)

        picture_frame = Frame(self.addition_frame, width=400, height=300)
        picture_frame.pack()

        # Generate a random number
        num1 = randint(0, 10)
        num2 = randint(0, 10)

        # Create 2 labels inside the picture Frame
        add_1 = Label(picture_frame)
        add_2 = Label(picture_frame)
        math_sign = Label(picture_frame, text="+", font=("Helvetica", 28))

        # Grid all the Labels
        add_1.grid(row=0, column=0)
        add_2.grid(row=0, column=2)
        math_sign.grid(row=0, column=1)

        add_1.config(text=num1)
        add_2.config(text=num2)
        math_sign.config(text="+")

        # Images
        card1 = f'images/flashcards/{num1}.png'
        card2 = f'images/flashcards/{num2}.png'
        self.add_image1 = ImageTk.PhotoImage(Image.open(card1))
        self.add_image2 = ImageTk.PhotoImage(Image.open(card2))

        add_1.config(image=self.add_image1)
        add_2.config(image=self.add_image2)

        # Create answer box and button
        add_answer = Entry(self.addition_frame, font=("Helvetica", 18))
        add_answer.pack(pady=50)

        add_answer_button = Button(self.addition_frame, text="Answer")
        add_answer_button.pack(pady=5)


    def hide_all_frames(self):
        """ Hide all frames """

        # Loop through all the children in each frame and delete them
        for widget in self.state_frame.winfo_children():
            widget.destroy()

        for widget in self.state_capitals_frame.winfo_children():
            widget.destroy()

        for widget in self.addition_frame.winfo_children():
            widget.destroy()

        # hide frames
        self.state_frame.pack_forget()
        self.state_capitals_frame.pack_forget()
        self.addition_frame.pack_forget()

    def _state_listing(self):
        # Create a list of state names
        files_list = listdir(FlashCards.STATES_DIR)
        # print(files_list)
        our_states = [x.replace('.png', '') for x in files_list]
        # print(states)
        return our_states
    
    def _get_name(self, name, is_showed = False):
        if is_showed:
            return name.replace('+', ' ')
        return name.replace('+', '')


app = FlashCards(root, path.abspath(path.curdir))

# Launch GUI
root.mainloop()