import tkinter as Tk
import tkinter.ttk as ttk
root = Tk.Tk()
root.title("Right click menu")
root.iconbitmap("codemy.ico")
root.geometry('500x550')


def hello():
    """ Display hello on menu hello click """
    my_label.config(text="Hello World!")


def goodbye():
    """ Display hello on menu goodbye click """
    my_label.config(text="Goodbye World!")


def my_popup(e):
    my_menu.tk_popup(e.x_root, e.y_root)


my_label = Tk.Label(root, text='', font=("Helvetica", 20))
my_label.pack(pady=20)

# Create a menu
my_menu = Tk.Menu(root, tearoff=False)
my_menu.add_command(label="Say Hello", command=hello)
my_menu.add_command(label="Say Goodbye", command=goodbye)
my_menu.add_separator()
my_menu.add_command(label="Exit", command=root.quit)

# <Button-3> for Mouse Right Click, <Button-1> for Mouse Left Click
# Mouse Button Left Click
# root.bind("<Button-1>", my_popup)
# Mouse Button Right Click
root.bind("<Button-3>", my_popup)

root.mainloop()
