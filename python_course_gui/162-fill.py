from tkinter import *
root = Tk()
root.title("Auto Select / Search")
root.iconbitmap("codemy.ico")
root.geometry('500x400')


def update(data: list):
    """
    Update the listbox
    """

    # Clear the listbox
    my_listbox.delete(0, END)

    # add toppings to listbox
    for item in data:
        my_listbox.insert(END, item)


def fillout(event: Event):
    """
    Update entry box with listbox item clicked
    """
    # Delete whatever is in the entry box
    my_entry.delete(0, END)

    # Add clicked list item to entry box
    my_entry.insert(0, my_listbox.get(ACTIVE))


def check(event: Event):
    """
    Check entry box content vs listbox contents
    """
    # grab what was typed
    typed = my_entry.get()

    if typed == "":
        data = toppings
    else:
        data = []
        for item in toppings:
            if typed.lower() in item.lower():
                data.append(item)

    #  update listbox with selected items
    update(data)


# Create a Label
my_label = Label(root, text="Start Typing...",
                 font=("Helvetica", 14), fg="grey")
my_label.pack(pady=20)


# Create an Entry box
my_entry = Entry(root, font=("Helvetica", 20))
my_entry.pack()

# Create a listbox
my_listbox = Listbox(root, width=50)
my_listbox.pack(pady=40)

# Create a list of pizza toppings
toppings = ["Pepperoni", "Pepper", "Cheese",
            "Mushroom", "Veggie", "Onions", "Ham", "Tacos"]
toppings.sort()

# Add the toppings to the listbox
update(toppings)

my_entry.focus()

# Create a binding on the listbox on click
my_listbox.bind("<<ListboxSelect>>", fillout)
# Create a binding on th eentry box
my_entry.bind("<KeyRelease>", check)

root.mainloop()
