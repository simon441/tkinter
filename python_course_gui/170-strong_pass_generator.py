from tkinter import *
from tkinter import messagebox
from random import randint

root = Tk()
root.title("Strong Password Generator")
root.iconbitmap("codemy.ico")
root.geometry('500x300')

DEFAULT_BG_COLOR = root['bg']


def new_rand():
    # Clear password entry box
    password_entry.delete(0, END)

   # Get password length and convert to integer
    try:
        pwd_length = int(my_entry.get())
        if pwd_length <= 0:
            raise ValueError
    except:
        messagebox.showwarning('Strong Password Generator',
                               'Password length must be a positive whole number')
        return

    # create a variavle to hold the password
    my_password = ''

    # Loop through password length
    for x in range(pwd_length):
        my_password += chr(randint(33, 126))

    # Output password to the screen
    password_entry.insert(0, my_password)


def copy_to_clipboard():
    # Clear Clipboard
    root.clipboard_clear()
    root.clipboard_append(password_entry.get())
    messagebox.showinfo("Strong Password Generator", "Password copied to clipboard!")


# Label Frame
lf = LabelFrame(root, text="How Many Characters?")
lf.pack(pady=20)

# Create Entry Box To Designate Number of Charaters
my_entry = Entry(lf, font=("Helvetica", 24))
my_entry.pack(pady=20, padx=20)

# Create Entry Box For The Returned Password
password_entry = Entry(root, text="", font=(
    "Helvetica", 24), bd=0, bg=DEFAULT_BG_COLOR)
password_entry.pack(pady=20)


# Frame fo the buttons
my_frame = Frame(root)
my_frame.pack(pady=20)

# Buttons
my_button = Button(my_frame, text="Generate Strong Password", command=new_rand)
my_button.grid(row=0, column=0, padx=10)

clip_button = Button(my_frame, text="Copy to Clipboard",
                     command=copy_to_clipboard)
clip_button.grid(row=0, column=1)


root.mainloop()
