import tkinter as Tk

root = Tk.Tk()
root.title("Delete menus")
root.iconbitmap("codemy.ico")
root.geometry('500x500')

# functions

def new():
    pass

def open():
    pass

def disable_new():
    """ Disable the New Menu Item """
    file_menu.entryconfig("New", state=Tk.DISABLED)

def enable_new():
    """ Enable the New Menu Item """
    file_menu.entryconfig("New", state="normal")


def disable_file_menu():
    """ Disable File menu """
    my_menu.entryconfig("File", state=Tk.DISABLED)

def enable_file_menu():
    """ Enable File menu """
    my_menu.entryconfig("File", state="normal")

def delete_new():
    """ Delete New Menu Item  """
    file_menu.delete("New")

def delete_file():
    """ Delete File Menu Item  """
    my_menu.delete("File")

# Create main menu
my_menu = Tk.Menu(root)
root.config(menu=my_menu)

# Add menu items to main menu
file_menu = Tk.Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="File", menu=file_menu)
# Add dropdown items
file_menu.add_command(label="New", command=new)
file_menu.add_command(label="Open", command=open)

# buttons
disable_button = Tk.Button(root, text="Disable New", command=disable_new)
disable_button.pack(pady=50)
enable_button = Tk.Button(root, text="Enable New", command=enable_new)
enable_button.pack(pady=10)
disable_all_button = Tk.Button(root, text="Disable File Menu", command=disable_file_menu)
disable_all_button.pack(pady=20)
enable_all_button = Tk.Button(root, text="Enable File Menu", command=enable_file_menu)
enable_all_button.pack(pady=10)
delete_new_button = Tk.Button(root, text="Delete New", command=delete_new)
delete_new_button.pack(pady=20)
delete_new_button = Tk.Button(root, text="Delete File menu", command=delete_file)
delete_new_button.pack(pady=10)

root.mainloop()
