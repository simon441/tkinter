from tkinter import *

root = Tk()
root.title("Canvas, omve shapers with keyboard keys")
root.iconbitmap('codemy.ico')
root.geometry("800x600")

w = 600
h = 400


# Creat ethe Canvas
my_canvas = Canvas(root, width=w, height=h, bg="#fcfcfc")
my_canvas.pack(pady=20)

# Add Image to Canvas

img = PhotoImage(file='images/me.png')

# Create and Image
my_image = my_canvas.create_image(260, 125, anchor=NW, image=img)


def move(e):
    # Coordinates: (e.x, e.y) x starts at top left to top right, y starts at top left to bottom left
    global img
    img = PhotoImage(file='images/me.png')
    # Create and Image
    my_image = my_canvas.create_image(e.x, e.y, image=img)
    my_label.config(text=f"Coordinates: x:{e.x}, y:{e.y}")

# Bind the keys to the events
# define specific keys (the arrow keys)
# root.bind("<Left>", left)
# root.bind("<Right>", right)
# root.bind("<Up>", up)
# root.bind("<Down>", down)

my_label = Label(root, text="")
my_label.pack(pady=20)

my_canvas.bind('<B1-Motion>', move)

root.mainloop()