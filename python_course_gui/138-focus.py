import tkinter as Tk

root = Tk.Tk()
root.title("Tab Order and Focus")
root.iconbitmap("codemy.ico")
root.geometry('500x550')

# 2 solutions to change the tab order:
# - change the order of the widgets when defined
# - put widgets in a list and use lift() when looping through the list

# To put focus on a widget use focus()

# Entry boxes
red = Tk.Entry(root, bg="red", font=("Helvetica", 20))
white = Tk.Entry(root, bg="white", font=("Helvetica", 20))
blue = Tk.Entry(root, bg="blue", font=("Helvetica", 20))

# pack the entry boxes
red.pack(pady=20)
white.pack(pady=20)
blue.pack(pady=20)

# Pick focus
white.focus()


def change_tab_order():
    """
    Change the tab order programatically
    """
    # Set the focus to blue
    blue.focus()

    # Setting the tab oirder in the list
    widgets = [blue, white, red]
    for w in widgets:
        # lift the order of the tabs in the order of the input list
        w.lift()


my_button = Tk.Button(root, text="Change Tab Order", command=change_tab_order)
my_button.pack(pady=20)


root.mainloop()
