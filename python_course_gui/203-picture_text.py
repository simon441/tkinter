from pathlib import Path
import tkinter as tk
from tkinter import Button, Entry, Label, PhotoImage, Tk
from tkinter.constants import END

from PIL import Image, ImageDraw, ImageFont

root = Tk()
root.title('System Info')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('600x650')

images_path = Path.joinpath(
    Path(__file__).parent.resolve(), 'images')
image_path = Path.joinpath(images_path, 'aspen.png')
image_xy = (150, 300)


def add_it():
    """Add Text To Font"""
    # Open the image
    my_image = Image.open(fp=image_path)
    # Define the Font
    text_font = ImageFont.truetype('arial.ttf', 46)
    # Get text to add to image
    text_to_add = my_entry.get().strip()

    # Edit the Image
    edit_image = ImageDraw.Draw(im=my_image)
    edit_image.text(xy=image_xy, text=text_to_add,
                    fill=('red'), font=text_font)

    # Save the edited Image
    my_image.save(Path.joinpath(images_path, 'aspen_picture_text.png'))

    # Clear the Entry Box
    my_entry.delete(0, END)
    my_entry.insert(0, 'Saving File...')

    # Wait a few seconds and the show image
    my_label.after(2000, show_pic)


def show_pic():
    """Show new Image"""
    global aspen2
    aspen2 = PhotoImage(file=Path.joinpath(
        images_path, 'aspen_picture_text.png'))
    my_label.config(image=aspen2)

    # Clear the Entry Box
    my_entry.delete(0, END)


# Define Image
aspen = PhotoImage(file=image_path)

# Create a Label
my_label = Label(root, image=aspen)
my_label.pack(pady=20)

# Entry Box
my_entry = Entry(root, font=('Helvetica', 24))
my_entry.pack(pady=20)

# Button
my_button = Button(root, text='Add Text To Image',
                   command=add_it, font=('Helvetica', 24))
my_button.pack(pady=20)

root.mainloop()
