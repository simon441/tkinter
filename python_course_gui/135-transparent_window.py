import tkinter as Tk
import tkinter.ttk as ttk
root = Tk.Tk()
root.title("Alpha method")
root.iconbitmap("codemy.ico")
root.geometry('500x550')

BASE_TRANSPARENCY = 0.7

# transparency levels go from 0.1 to 1.0
root.attributes('-alpha', BASE_TRANSPARENCY)

my_label = Tk.Label(root, text="Hello World!", font=('Helvetica', 20))
my_label.pack(pady=20)


def slide(e):
    """
    Using the slider
    """
    root.attributes('-alpha', my_slider.get())
    my_label.config(text=str(round(my_slider.get(), 3)))


def make_solid(event):
    """
    Make second window solid when clicked
    """
    new.attributes('-alpha', 1.0)


def new_window():
    """
    Open a transparent new window
    """
    global new
    new = Tk.Toplevel()
    new.attributes('-alpha', 0.5)
    new.bind("<Button-1>", make_solid)


# Create a slider to change the transparency of the window
my_slider = ttk.Scale(root, from_=0.1, to=1.0,
                      value=BASE_TRANSPARENCY, orient=Tk.HORIZONTAL, command=slide)
my_slider.pack(pady=20)

# Label to display the curent value of my_slider
my_label = Tk.Label(root, text='')
my_label.pack(pady=10)

# new window button
new_window = Tk.Button(root, text='Open a new window', command=new_window)
new_window.pack(pady=20)

root.mainloop()
