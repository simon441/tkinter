from tkinter import *
from PIL import ImageTk, Image

root = Tk()

root.title("Simon")
root.iconbitmap('codemy.ico')
# my_image = ImageTk.PhotoImage(Image.open('images/aspen.png'))

def open():    
    # must global the image because python doesn't display the image otherwise
    global my_image

    # second window
    top = Toplevel()
    top.title("My second window")
    top.iconbitmap('codemy.ico')

    my_image = ImageTk.PhotoImage(Image.open('images/aspen.png'))

    label = Label(top, image=my_image).pack()
    btn2 = Button(top, text="close window", command=top.destroy).pack()

def open_2nd():
    def quit():
        top.destroy()
        root.update()
        root.deiconify()
    
    root.withdraw()
    top = Toplevel()
    root.title('Third window')
    img1 = ImageTk.PhotoImage(Image.open('images/aspen.png'))
    lbl2 = Label(top, image=img1)
    lbl2.pack()
    lbl2.image = img1
    Button(top, text="close window", command=quit).pack()

btn = Button(root, text="Open second window", command=open).pack()
btn2 = Button(root, text="Open third window", command=open_2nd).pack()

root.mainloop()