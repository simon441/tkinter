from tkinter import *
import time
from random import randint
import threading

root = Tk()
root.title('Text box')
root.iconbitmap("codemy.ico")
root.geometry("500x400")

# clear function to clear the textbox
def clear():
    my_textbox.delete(1.0, END)

# Grab the text from the text box
def get_text():
    my_label.config(text=my_textbox.get(1.0, END))

my_textbox = Text(root, width=40, height=10, font=('Helvetica', 16))
my_textbox.pack(pady=20)

button_frame = Frame(root)
button_frame.pack()

clear_button = Button(button_frame, text="Clear Screen", command=clear)
clear_button.grid(row=0, column=0)

get_text = Button(button_frame, text="Get Text", command=get_text)
get_text.grid(row=0, column=1, padx=20)


my_label = Label(root, text='')
my_label.pack(pady=20)

root.mainloop()