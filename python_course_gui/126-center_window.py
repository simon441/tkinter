import tkinter as Tk

root = Tk.Tk()
root.title("Center the window on load")
root.iconbitmap("codemy.ico")

# Designate Height and Width of the app
app_width = 500
app_height = 500

# screen dimensions
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

# Corrodinates of the top left of the app
x = (screen_width / 2) - (app_width / 2)
y = (screen_height / 2) - (app_height / 2)


root.geometry(f"{app_height}x{app_width}+{int(x)}+{int(y)}")

my_label = Tk.Label(
    root, text=f"Width: {screen_width}, Height: {screen_height}")
my_label.pack(pady=20)

root.mainloop()
