from tkinter import *
from tkinter import ttk, messagebox
from PIL import ImageTk, Image
import mysql.connector
from mysql.connector import Error
import csv
import json
from decimal import Decimal

root = Tk()
root.title("Simon Cateau's Customer Database Manager")
root.iconbitmap('codemy.ico')
root.geometry("400x600")

db_name = "crm_python"
table_name = "customers"
IS_DEBUG = True
file_name_default_for_export = table_name

def print_cursor(cursor, query):
    if not IS_DEBUG:
        return
    cursor.execute(query)
    for x in cursor:
        print(x)

def print_cursor_result(cursor):
    # if not IS_DEBUG:
    #     return
    for x in cursor:
        print(x)


# Connect to MySQL
mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="root",
    database=db_name 
)

# Check to see if connection to MySQL was created
# print(mydb)

# Create a cursor and initialize it
my_cursor = mydb.cursor()

# print(my_cursor)

#################################### 
# Database and table initialization
###################################

# Create Database
my_cursor.execute(f"CREATE DATABASE IF NOT EXISTS {db_name};")

# print_cursor(my_cursor, "SHOW DATABASES")

# Create a table
my_cursor.execute("""CREATE TABLE IF NOT EXISTS customers (
	user_id INT(11) PRIMARY KEY AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	zipcode INT(10),
	price_paid DECIMAL(10,2) NOT NULL,
	email VARCHAR(255),
	address_1 VARCHAR(255),
	address_2 VARCHAR(255),
	city VARCHAR(50),
	state VARCHAR(50),
	country VARCHAR(255),
	phone VARCHAR(255),
	payment_method VARCHAR(50),
	discount_code VARCHAR(255)
    );
""")

# show database tables
# print_cursor(my_cursor, "SHOW TABLES")

# show content of table 
# my_cursor.execute("SELECT * FROM " + table_name)
# print_cursor_result(my_cursor.description)

"""
FIELDS
	first_name,
	last_name,
	zipcode,
	price_paid,
	email,
	address_1,
	address_2,
	city,
	state,
	country,
	phone,
	payment_method,
	discount_code
"""

################
# Functions
################

# Tkinter helpers

def on_quit_app():
    if messagebox.askyesno('App Info', "Exit Application Customer Manager?"):
        print("Closing MySQL connections")
        # messagebox.info('App Info', "Closing Database Connections")
        my_cursor.close()
        mydb.close()
        root.destroy()

# Utility functions

def write_to_file(data, filename):
    pass

def export_to_csv(data, filename=None):
    """ Export a file to csv
        data: data to write to file
        filename: optional filename, defaults to previously defined name
     """

    if filename is None:
        filename = file_name_default_for_export + '.csv'
    elif not filename.endswith('.csv'):
        filename += '.csv' 
    
    with open(file=filename, mode="w", encoding="utf-8", newline='') as f:
        w = csv.writer(f, dialect="excel")
        w.writerows(data)
    f.close()


            
def export_to_json(data, field_names, filename=None):
    """ Export a file to JSON
        data: data to parse (values)
        field_names: result field names (key)
        filename: optional filename, defaults to previously defined name
     """

    if filename is None:
        filename = file_name_default_for_export + '.json'
    elif not filename.endswith('.json'):
        filename += '.json'

    #  Make a dictionary from the data
    data_dict = []
    for row in data:
        my_row = {}
        for index, column in enumerate(row):
            # print(column, index, field_names[index], type(column))
            if type(column) is Decimal:
                column = float(column)
            my_row[field_names[index]] = column
        data_dict.append(my_row)

    # print(data_dict)
    # print(json.dumps(data_dict))

    with open(file=filename, mode="w", encoding="utf-8") as f:
        f.write(json.dumps(data_dict, indent=4))
    f.close()


# GUI and Db Functions

def clear_fields():
    """ Clear the text from all Entry fields """
    first_name_box.delete(0, END)
    last_name_box.delete(0, END)
    address1_box.delete(0, END)
    address2_box.delete(0, END)
    city_box.delete(0, END)
    state_box.delete(0, END)
    zipcode_box.delete(0, END)
    country_box.delete(0, END)
    phone_box.delete(0, END)
    email_box.delete(0, END)
    payment_method_box.delete(0, END)
    discount_code_box.delete(0, END)
    price_paid_box.delete(0, END)


def add_customer():
    """ Submit Customer to Database """
    """ Insert a customer into the table """

    sql_command = """INSERT INTO customers 
    (first_name, last_name, address_1, address_2, city, state, zipcode, country, phone, email, payment_method, discount_code, price_paid)
     VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
     """
    values = (
        first_name_box.get(),
        last_name_box.get(),
        address1_box.get(),
        address2_box.get(),
        city_box.get(),
        state_box.get(),
        zipcode_box.get(),
        country_box.get(),
        phone_box.get(),
        email_box.get(),
        payment_method_box.get(),
        discount_code_box.get(),
        price_paid_box.get()
    )
    # print(values)

    try:
        my_cursor.execute(sql_command, values)

        # Commit the changes to the database
        mydb.commit()
    except Error as e:
        print(f"Error: {e}")
        messagebox.showerror("Could not add the customer", "An error occurred when adding the customer\nCould not add the customer")
        mydb.rollback()
    
    messagebox.showinfo("Adding Customer", f"Added Customer \"{first_name_box.get()} {last_name_box.get()}\" successfully!")

    # Clear the fields after insert
    clear_fields()

  
def list_customers():
    """ List customers """
    list_customers_query = Tk()
    list_customers_query.title("List All Costumers")
    list_customers_query.iconbitmap('codemy.ico')
    list_customers_query.geometry("1000x600")

    my_cursor.execute("SELECT * FROM customers")
    results = my_cursor.fetchall()
    for index, result in enumerate(results):
        num = 0
        # lookup_label = Label(list_customers_query, text="{0} {1} {2}".format(x[1], x[2], x[0]))
        for field in result:
            lookup_label = Label(list_customers_query, text=field)
            lookup_label.grid(row=index, column=num)
            num += 1
        # print(x)
    
    headers = list(my_cursor.column_names)
    data = []
    data.append(headers)
    data.extend(results)

    # Button to Export to CSV format
    csv_button = Button(list_customers_query, text="Save to CSV", command=lambda: export_to_csv(data))
    csv_button.grid(row=index + 1, column=0, padx=10, pady=10)


    # Button to Export to JSON format
    json_button = Button(list_customers_query, text="Save to JSON", command=lambda: export_to_json(results, headers))
    json_button.grid(row=index + 1, column=1, padx=10, pady=10)


def search_customers():
    """ Search All customers """

    # Dropdown search options
    search_options = [
        'Search by...',
        'First Name',
        'Last Name',
        'Email Address',
        'Customer ID',
        'City',
    ]
    search_options_dict = {
        '_NOVALUE_': 'Search by...',
        'first_name': 'First Name',
        'last_name': 'Last Name',
        'email': 'Email Address',
        'id': 'Customer ID',
        'city': 'City',
    }

    # functions
 
    def edit_now(id, index):
        """ Edit a customer
            id: customer id
            index: row number
         """

        my_cursor.execute("SELECT * FROM customers WHERE user_id = %s", (id, ))
        result = my_cursor.fetchone()

        # def update(first_name, last_name, address_1, address_2, city, state, zipcode, country, phone, email, payment_method, discount_code, price_paid, id):
        def update():
            sql = "UPDATE customers SET first_name = %s, last_name = %s, address_1 = %s, address_2 = %s, city = %s, state = %s, zipcode = %s, country = %s, phone = %s, email = %s, payment_method = %s, discount_code = %s, price_paid = %s WHERE user_id = %s"
            values = (
                first_name_box2.get(),
                last_name_box2.get(),
                address1_box2.get(),
                address2_box2.get(),
                city_box2.get(),
                state_box2.get(),
                zipcode_box2.get(),
                country_box2.get(),
                phone_box2.get(),
                email_box2.get(),
                payment_method_box2.get(),
                discount_code_box2.get(),
                price_paid_box2.get(),
                id_box2.get(),
                )
            # print(values)
            try:
                my_cursor.execute(sql, values)
                mydb.commit()
            except Exception as e:
                messagebox.showerror("Update Customer", "An error occurred trying to update the customer.\nThe customer was not updated\nTry again")
                print(e)
                mydb.rollback()
            
            # print(mydb.get_warnings())
            messagebox.showinfo("Customer Update", f"Updated Customer \"{first_name_box2.get()} {last_name_box2.get()}\" successfully!")

            # Close Search Customers window
            search_customers.destroy()
        
        # Create Labels
        title_label = Label(search_customers, text="Cateau Customer Database Manager", font=("Helvetica", 16))
        title_label.grid(row=0, column=0, columnspan=2, pady="10")

        # Main Form to Enter Customer
        first_name_label = Label(search_customers, text="First Name").grid(row=index+1, column=0, sticky=W, padx=10)
        last_name_label = Label(search_customers, text="Last Name").grid(row=index+2, column=0, sticky=W, padx=10)
        address1_label = Label(search_customers, text="Address 1").grid(row=index+3, column=0, sticky=W, padx=10)
        address2_label = Label(search_customers, text="Address 2").grid(row=index+4, column=0, sticky=W, padx=10)
        city_label = Label(search_customers, text="City").grid(row=index+5, column=0, sticky=W, padx=10)
        state_label = Label(search_customers, text="State").grid(row=index+6, column=0, sticky=W, padx=10)
        zipcode_label = Label(search_customers, text="Zipcode").grid(row=index+7, column=0, sticky=W, padx=10)
        country_label = Label(search_customers, text="Country").grid(row=index+8, column=0, sticky=W, padx=10)
        phone_label = Label(search_customers, text="Phone Number").grid(row=index+9, column=0, sticky=W, padx=10)
        email_label = Label(search_customers, text="Email Address").grid(row=index+10, column=0, sticky=W, padx=10)
        payment_method_label = Label(search_customers, text="Payment Method").grid(row=index+11, column=0, sticky=W, padx=10)
        discount_code_label = Label(search_customers, text="Discount Code").grid(row=index+12, column=0, sticky=W, padx=10)
        price_paid_label = Label(search_customers, text="Price Paid").grid(row=index+13, column=0, sticky=W, padx=10)
        id_label = Label(search_customers, text="User ID").grid(row=index+14, column=0, sticky=W, padx=10)

        print(result)

        # Entry Box
        # global first_name_box
        first_name_box2 = Entry(search_customers)
        first_name_box2.grid(row=index+1, column=1)
        first_name_box2.insert(0, result[1])

        # global last_name_box
        last_name_box2 = Entry(search_customers)
        last_name_box2.grid(row=index+2, column=1, pady=5)
        last_name_box2.insert(0, result[2])

        # global address1_box
        address1_box2 = Entry(search_customers)
        address1_box2.grid(row=index+3, column=1, pady=5)
        address1_box2.insert(0, result[6])

        # global address2_box
        address2_box2 = Entry(search_customers)
        address2_box2.grid(row=index+4, column=1, pady=5)
        address2_box2.insert(0, result[7])

        # global city_box
        city_box2 = Entry(search_customers)
        city_box2.grid(row=index+5, column=1, pady=5)
        city_box2.insert(0, result[8])

        # global state_box
        state_box2 = Entry(search_customers)
        state_box2.grid(row=index+6, column=1, pady=5)
        state_box2.insert(0, result[9])

        # global zipcode_box
        zipcode_box2 = Entry(search_customers)
        zipcode_box2.grid(row=index+7, column=1, pady=5)
        zipcode_box2.insert(0, result[3])

        # global country_box
        country_box2 = Entry(search_customers)
        country_box2.grid(row=index+8, column=1, pady=5)
        country_box2.insert(0, result[10])

        # global phone_box
        phone_box2 = Entry(search_customers)
        phone_box2.grid(row=index+9, column=1, pady=5)
        phone_box2.insert(0, result[11])

        # global email_box
        email_box2 = Entry(search_customers)
        email_box2.grid(row=index+10, column=1, pady=5)
        email_box2.insert(0, result[5])

        # global payment_method_box
        payment_method_box2 = Entry(search_customers)
        payment_method_box2.grid(row=index+11, column=1, pady=5)
        payment_method_box2.insert(0, result[12])

        # global discount_code_box
        discount_code_box2 = Entry(search_customers)
        discount_code_box2.grid(row=index+12, column=1, pady=5)
        discount_code_box2.insert(0, result[13])

        # global price_paid_box
        price_paid_box2 = Entry(search_customers)
        price_paid_box2.grid(row=index+13, column=1, pady=5)
        price_paid_box2.insert(0, result[4])

        # global id_box
        id_box2 = Entry(search_customers, state=DISABLED)
        id_box2.grid(row=index+14, column=1, pady=5)
        id_box2.insert(0, result[0])
        # global first_name_box2
        # 	first_name_box2 = Entry(search_customers)
        # 	first_name_box2.grid(row=index+1, column=1, pady=10)
        # 	first_name_box2.insert(0, result2[0][0])

        # 	global last_name_box2
        # 	last_name_box2 = Entry(search_customers)
        # 	last_name_box2.grid(row=index+2, column=1, pady=5)
        # 	last_name_box2.insert(0, result2[0][1])

        # 	global address1_box2
        # 	address1_box2 = Entry(search_customers)
        # 	address1_box2.grid(row=index+3, column=1, pady=5)
        # 	address1_box2.insert(0, result2[0][6])

        # 	global address2_box2
        # 	address2_box2 = Entry(search_customers)
        # 	address2_box2.grid(row=index+4, column=1, pady=5)
        # 	address2_box2.insert(0, result2[0][7])

        # 	global city_box2 
        # 	city_box2 = Entry(search_customers)
        # 	city_box2.grid(row=index+5, column=1, pady=5)
        # 	city_box2.insert(0, result2[0][8])

        # 	global state_box2
        # 	state_box2 = Entry(search_customers)
        # 	state_box2.grid(row=index+6, column=1, pady=5)
        # 	state_box2.insert(0, result2[0][9])

        # 	global zipcode_box2
        # 	zipcode_box2 = Entry(search_customers)
        # 	zipcode_box2.grid(row=index+7, column=1, pady=5)
        # 	zipcode_box2.insert(0, result2[0][2])

        # 	global country_box2
        # 	country_box2 = Entry(search_customers)
        # 	country_box2.grid(row=index+8, column=1, pady=5)
        # 	country_box2.insert(0, result2[0][10])

        # 	global phone_box2
        # 	phone_box2 = Entry(search_customers)
        # 	phone_box2.grid(row=index+9, column=1, pady=5)
        # 	phone_box2.insert(0, result2[0][11])

        # 	global email_box2
        # 	email_box2 = Entry(search_customers)
        # 	email_box2.grid(row=index+10, column=1, pady=5)
        # 	email_box2.insert(0, result2[0][5])

        # 	global payment_method_box2
        # 	payment_method_box2 = Entry(search_customers)
        # 	payment_method_box2.grid(row=index+11, column=1, pady=5)
        # 	payment_method_box2.insert(0, result2[0][12])

        # 	global discount_code_box2
        # 	discount_code_box2 = Entry(search_customers)
        # 	discount_code_box2.grid(row=index+12, column=1, pady=5)
        # 	discount_code_box2.insert(0, result2[0][13])

        # 	global price_paid_box2
        # 	price_paid_box2 = Entry(search_customers)
        # 	price_paid_box2.grid(row=index+13, column=1, pady=5)
        # 	price_paid_box2.insert(0, result2[0][3])

        # 	global id_box2
        # 	id_box2 = Entry(search_customers)
        # 	id_box2.grid(row=index+14, column=1, pady=5)
        # 	id_box2.insert(0, result2[0][4])
            # Buttons
        
        save_record_button = Button(search_customers, text="Update Record", command=update )

        # save_record_button = Button(search_customers, text="Update Record", command=lambda: update(
        #     first_name_box2.get(), last_name_box2.get(), address1_box2.get(), address2_box2.get(), city_box2.get(), state_box2.get(), zipcode_box2.get(), country_box2.get(), phone_box2.get(), email_box2.get(), payment_method_box2.get(), discount_code_box2.get(), price_paid_box2.get(),id_box2.get()
        # ) )
        save_record_button.grid(row=index+15, column=0, padx=10, pady=10, sticky=W)

 
    def search_now():
        """ Search Function """
        searched = search_box.get().strip()

        selected = search_options_box.get()
        # if selected == search_options[1]:
        #     search_query = "first_name LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "last_name LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "email LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "user_id LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "city LIKE %s"
        value_search = "%{}%"
        if selected == search_options_dict['_NOVALUE_']:
            text = Label(search_customers, text="Hey! You forget to select a search option")
            text.grid(row=3, column=0, columnspan=3)
            return
        elif selected == search_options_dict['first_name']:
            search_query = "first_name LIKE %s"
        elif selected == search_options_dict["last_name"]:
            search_query = "last_name LIKE %s"
        elif selected == search_options_dict["email"]:
            search_query = "email LIKE %s"
        elif selected == search_options_dict["id"]:
            search_query = "user_id = %s"
            value_search = "{}"
        elif selected == search_options_dict["city"]:
            search_query = "city LIKE %s"
        else:
            Label(search_customers, text="An error occurred in the selection").grid(row=3, column=0, columnspan=3)
            return

        sql = f"SELECT * FROM customers WHERE {search_query}"
        # print(sql)
        name = (value_search.format(searched), )
        my_cursor.execute(sql, name)

        # print(my_cursor.rowcount)
        # for x in my_cursor:
        #     print(x)
        results = my_cursor.fetchall()
        if my_cursor.rowcount == 0:
            results = "No record found..."
            searched_label = Label(search_customers, text=results)
            searched_label.grid(row=2, column=0, columnspan=3)
        else:            
            # print(my_cursor.column_names)
            for index, result in enumerate(results):
                num = 0
                id_reference = str(result[0])
                print("id_reference=" + id_reference)
                index += 2

                # searched_label = Label(search_customers, text="{0} {1} {2}".format(x[1], x[2], x[0]))
                edit_button = Button(search_customers, text=f"Edit {id_reference}", command=lambda id_reference=id_reference: edit_now(id_reference, index))
                edit_button.grid(row=index, column=num)
                for field in result:
                    searched_label = Label(search_customers, text=field)
                    searched_label.grid(row=index, column=num + 1)
                    num += 1
                # print(x)
            
            headers = list(my_cursor.column_names)
            data = []
            data.append(headers)
            data.extend(results)
            """
            #  Treeview not complete (missing scrollbars, and resize)
            tv = ttk.Treeview(search_customers, columns=my_cursor.column_names, show='headings', selectmode='browse')
                        
            vsb = ttk.Scrollbar(search_customers, orient="vertical", command=tv.yview)
            vsb.place(x=30+200+2, y=95, height=200+20)
            tv.configure(yscrollcommand=vsb.set)

            for col in my_cursor.column_names:
                print(col, type(col))
                tv.heading(col, text=col, anchor=W)
                if col in ('user_id', 'price_paid'):
                    width = 100
                else:
                    width = 200
                tv.column(col, width=width)
            
            for index, result in enumerate(results):
                # searched_label = Label(search_customers, text="{0} {1} {2}".format(x[1], x[2], x[0]))
                tv.insert("", index, values=result)
                # print(x)
            # for index, result in enumerate(results, 3):
            #     # num = 0
            #     # # lookup_label = Label(search_customers, text="{0} {1} {2}".format(x[1], x[2], x[0]))
            #     # for field in result:
            #     #     lookup_label = Label(search_customers, text=field)
            #     #     lookup_label.grid(row=index, column=num)
            #     #     num += 1
            #     tv.insert(search_customers, index, result)
            #     # print(x)
            # #     # tv.heading(str(col), text=str(col))
            # # # tv.column("#0", anchor="w")    
            # # # tv.heading("#0", text='RollNo', anchor='w')
            # # # tv.column("#0", anchor="w")
            # # # tv.heading('Name', text='Name')
            # # # tv.column('Name', anchor='center', width=100)
            # # # tv.heading('Mobile', text='Mobile')
            # # # tv.column('Mobile', anchor='center', width=100)
            # # # tv.heading('course', text='course')
            # # # tv.column('course', anchor='center', width=100)
            tv.grid(row=4, column=0, columnspan=3, sticky = (N,S,W,E))
            # # search_customers.treeview = tv
            # # search_customers.grid(3, weight = 1)
            # # search_customers.grid_columnconfigure(0, weight = 1)
            """
            
            save_search_file = file_name_default_for_export + '_search_results'
            # Button to Export to CSV format
            csv_button = Button(search_customers, text="Save to CSV", command=lambda: export_to_csv(data, save_search_file))
            csv_button.grid(row=index + 1, column=0, padx=10, pady=10)


            # Button to Export to JSON format
            json_button = Button(search_customers, text="Save to JSON", command=lambda: export_to_json(results, headers, save_search_file))
            json_button.grid(row=index + 1, column=1, padx=10, pady=10)
    
  
        index += 1

      


    search_customers = Tk()
    search_customers.title("Search All Costumers")
    search_customers.iconbitmap('codemy.ico')
    search_customers.geometry("1000x800")

    # Entry box to search for a customer
    search_box = Entry(search_customers)
    search_box.grid(row=0, column=1, padx=10, pady=10)

    # Entry box label
    search_box_label = Label(search_customers, text="Search Customer by Last Name")
    search_box_label.grid(row=0, column=0, padx=10, pady=10)

    # Entry box search button
    search_box_button = Button(search_customers, text="Search Customers", command=search_now)
    search_box_button.grid(row=1, column=0, padx=10)

    # Dropdown box to select fields
    search_options_box = ttk.Combobox(search_customers, values=list(search_options_dict.values()), state="readonly")
    search_options_box.current(0)
    search_options_box.grid(row=0, column=2, padx=10)



   

#################
# BEGIN GUI
################# 

# Create Labels
title_label = Label(root, text="Cateau Customer Database Manager", font=("Helvetica", 16))
title_label.grid(row=0, column=0, columnspan=2, pady="10")

# Main Form to Enter Customer
first_name_label = Label(root, text="First Name").grid(row=1, column=0, sticky=W, padx=10)
last_name_label = Label(root, text="Last Name").grid(row=2, column=0, sticky=W, padx=10)
address1_label = Label(root, text="Address 1").grid(row=3, column=0, sticky=W, padx=10)
address2_label = Label(root, text="Address 2").grid(row=4, column=0, sticky=W, padx=10)
city_label = Label(root, text="City").grid(row=5, column=0, sticky=W, padx=10)
state_label = Label(root, text="State").grid(row=6, column=0, sticky=W, padx=10)
zipcode_label = Label(root, text="Zipcode").grid(row=7, column=0, sticky=W, padx=10)
country_label = Label(root, text="Country").grid(row=8, column=0, sticky=W, padx=10)
phone_label = Label(root, text="Phone Number").grid(row=9, column=0, sticky=W, padx=10)
email_label = Label(root, text="Email Address").grid(row=10, column=0, sticky=W, padx=10)
payment_method_label = Label(root, text="Payment Method").grid(row=11, column=0, sticky=W, padx=10)
discount_code_label = Label(root, text="Discount Code").grid(row=12, column=0, sticky=W, padx=10)
price_paid_label = Label(root, text="Price Paid").grid(row=13, column=0, sticky=W, padx=10)

# Entry Boxes
first_name_box = Entry(root)
first_name_box.grid(row=1, column=1)

last_name_box = Entry(root)
last_name_box.grid(row=2, column=1, pady=5)

address1_box = Entry(root)
address1_box.grid(row=3, column=1, pady=5)

address2_box = Entry(root)
address2_box.grid(row=4, column=1, pady=5)

city_box = Entry(root)
city_box.grid(row=5, column=1, pady=5)

state_box = Entry(root)
state_box.grid(row=6, column=1, pady=5)

zipcode_box = Entry(root)
zipcode_box.grid(row=7, column=1, pady=5)

country_box = Entry(root)
country_box.grid(row=8, column=1, pady=5)

phone_box = Entry(root)
phone_box.grid(row=9, column=1, pady=5)

email_box = Entry(root)
email_box.grid(row=10, column=1, pady=5)

payment_method_box = Entry(root)
payment_method_box.grid(row=11, column=1, pady=5)

discount_code_box = Entry(root)
discount_code_box.grid(row=12, column=1, pady=5)

price_paid_box = Entry(root)
price_paid_box.grid(row=13, column=1, pady=5)

# Buttons
add_customer_button = Button(root, text="Add Customer to Database", command=add_customer)
add_customer_button.grid(row=14, column=0, padx=10, pady=10, sticky=W)
clear_fields_button = Button(root, text="Clear Fields", command=clear_fields)
clear_fields_button.grid(row=14, column=1)

# list customers button
list_customers_button = Button(root, text="List Customers", command=list_customers)
list_customers_button.grid(row=15, column=0, sticky=W, padx=10)

# Search customers button
search_customers_button = Button(root, text="Search/Edit Customers", command=search_customers)
search_customers_button.grid(row=15, column=1, sticky=W, padx=10)


#################################
# Start GUI and Launch Event Loop
#################################

root.protocol("WM_DELETE_WINDOW", on_quit_app)

root.mainloop()