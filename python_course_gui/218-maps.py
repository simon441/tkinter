from pathlib import Path
from tkinter import HORIZONTAL, Button, Entry, Tk
from tkinter.ttk import LabelFrame, Scale
import tkintermapview

root = Tk()
root.title('Tkinter MapView')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('900x800')

def lookup():
    """Lookup for a place on the map by search term"""
    map_widget.set_address(my_entry.get().strip())
    my_slider.config(value=9)

def slide(event):
    """On slider change"""
    map_widget.set_zoom(int(my_slider.get()))

my_label = LabelFrame(root)
my_label.pack(pady=(5,0))

# Map
map_widget = tkintermapview.TkinterMapView(my_label, width=800, height=int(600*(.88)), corner_radius=0)

# Set Coordinates
# map_widget.set_position(44.1015985, 3.0615855)

# Set Address
map_widget.set_address('notre dame, paris, france')

# Set Zoom Level
map_widget.set_zoom(20) # the higher the number the bigger the zoom

map_widget.pack()


my_frame = LabelFrame(root)
my_frame.pack(pady=5)

my_entry = Entry(my_frame, font=("Helvetica", 28))
my_entry.grid(row=0, column=0, pady=(10, 20), padx=10)

my_button = Button(my_frame, text="Lookup", font=("Helvetica", 18), command=lookup)
my_button.grid(row=0, column=1, padx=10)

my_slider = Scale(my_frame, from_=4, to=20, orient=HORIZONTAL, command=slide, value=20, length=220)
my_slider.grid(row=0, column=2, padx=10)
root.mainloop()