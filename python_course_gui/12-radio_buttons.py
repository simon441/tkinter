from tkinter import *

root = Tk()

root.title("Simon")
root.iconbitmap('codemy.ico')

# variable to get the value of the buttons
r = IntVar()
# set the selected option on load
r.set(1)

# base functionality
"""
def clicked(value):
    my_label = Label(root, text=value)
    my_label.pack()

# radio buttons
Radiobutton(root, text="Option 1", variable=r, value=1, command=lambda: clicked(r.get())).pack()
Radiobutton(root, text="Option 2", variable=r, value=2,command=lambda: clicked(r.get())).pack()

my_button = Button(root, text="Click me!", command=lambda: clicked(r.get())).pack()
"""

# advanced:  with options in list
title_label = Label(root, text="Choose your pizza topping", font=("Helvetica", 14)).pack(pady=(10, 20), padx=5)

def clicked(value):
    my_label = Label(root, text=value)
    my_label.pack(pady=(0, 5))


# TOPPINGS = [(text, mode),]
TOPPINGS = [
    ("Pepperoni", "Pepperoni"),
    ("Cheese", "Cheese"),
    ("Mushroom", "Mushroom"),
    ("Onion", "Onion"),
]

pizza = StringVar()
pizza.set("Pepperoni")

for text, topping in TOPPINGS:
    # update the display on radio button click (with a lambda)
    # Radiobutton(root, text=text, variable=pizza, value=topping, command=lambda: clicked(pizza.get())).pack(anchor=W)
    # do not update on radio button click (only with button)
    Radiobutton(root, text=text, variable=pizza, value=topping).pack(anchor=W)

my_button = Button(root, text="Submit your choice", command=lambda: clicked(pizza.get())).pack(pady=(0, 10))

my_label = Label(root, text=pizza.get()).pack(pady=(0, 5))

root.mainloop()
