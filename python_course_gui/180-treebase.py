from tkinter import *
from tkinter import ttk, messagebox

import sqlite3

root = Tk()
root.title("TreeBase")
root.iconbitmap("codemy.ico")
root.geometry('1000x500')

# Database stuff
DATABASE_NAME = 'tree_crm.db'


count = 0


def query_database():
    # Create a Database or connect to one that exists
    conn = sqlite3.connect(DATABASE_NAME)

    # Create a cursor instance
    c = conn.cursor()

    c.execute('SELECT rowid, * FROM customers')
    records = c.fetchall()

    # Add the data to the screen
    global count
    count = 0

    # for record in records:
    #     print(record)

    for record in records:

        row_type = 'evenrow' if count % 2 == 0 else 'oddrow'

        my_tree.insert(parent='', index=END, iid=count, text='', values=(
            record[1], record[2], record[0], record[4], record[5], record[6], record[7], ), tags=(row_type, ))
        # increment counter
        count += 1

    # Commit changes
    conn.commit()

    # Close the connection
    conn.close()


# Add Some Style
style = ttk.Style()

# Pick A Theme
style.theme_use('default')

# Configure the Treeview Colors
style.configure('Treeview',
                background='#D3D3D3',
                foreground='black',
                rowheight=25,
                fieldbackground='#D3D3D3'
                )

# Change Selected Color
style.map('Treeview',
          background=[('selected', '#347083')])

# Create a Treeview Frame
tree_frame = Frame(root)
tree_frame.pack(pady=10)

# Create a Treeview Scrollbar
tree_scroll = Scrollbar(tree_frame,)
tree_scroll.pack(side=RIGHT, fill=Y)

# Create the Treeview
my_tree = ttk.Treeview(
    tree_frame, yscrollcommand=tree_scroll.set, selectmode='extended')
my_tree.pack()

# Configure the Scrollbar
tree_scroll.config(command=my_tree.yview)

# Define the Columns
my_tree['columns'] = ('First Name', 'Last Name', 'ID',
                      'Address', 'City', 'State', 'Zipcode')

# Format the Columns
my_tree.column('#0', width=0, stretch=NO)
my_tree.column('First Name', anchor=W, width=140)
my_tree.column('Last Name', anchor=W, width=140)
my_tree.column('ID', anchor=CENTER, width=100)
my_tree.column('Address', anchor=CENTER, width=140)
my_tree.column('City', anchor=CENTER, width=140)
my_tree.column('State', anchor=CENTER, width=140)
my_tree.column('Zipcode', anchor=CENTER, width=140)


# Create Headings
my_tree.heading('#0', text='', anchor=W)
my_tree.heading('First Name', text='First Name', anchor=W)
my_tree.heading('Last Name', text='Last Name', anchor=W)
my_tree.heading('ID', text='ID', anchor=CENTER)
my_tree.heading('Address', text='Address', anchor=CENTER)
my_tree.heading('City', text='City', anchor=CENTER)
my_tree.heading('State', text='State', anchor=CENTER)
my_tree.heading('Zipcode', text='Zipcode', anchor=CENTER)


# Create Striped Row Tags
my_tree.tag_configure('oddrow', background='white')
my_tree.tag_configure('evenrow', background='lightblue')

# gggggggggggggggg

# Add Record Entry Boxes
data_frame = LabelFrame(root, text='Record')
data_frame.pack(fill=X, expand=YES, padx=20)

fn_label = Label(data_frame, text='First Name')
fn_label.grid(row=0, column=0, padx=10, pady=10)
fn_entry = Entry(data_frame)
fn_entry.grid(row=0, column=1, padx=10, pady=10)


ln_label = Label(data_frame, text='Last Name')
ln_label.grid(row=0, column=2, padx=10, pady=10)
ln_entry = Entry(data_frame)
ln_entry.grid(row=0, column=3, padx=10, pady=10)

id_label = Label(data_frame, text='ID')
id_label.grid(row=0, column=4, padx=10, pady=10)
id_entry = Entry(data_frame)
id_entry.grid(row=0, column=5, padx=10, pady=10)

address_label = Label(data_frame, text='Address')
address_label.grid(row=1, column=0, padx=10, pady=10)
address_entry = Entry(data_frame)
address_entry.grid(row=1, column=1, padx=10, pady=10)


city_label = Label(data_frame, text='City')
city_label.grid(row=1, column=2, padx=10, pady=10)
city_entry = Entry(data_frame)
city_entry.grid(row=1, column=3, padx=10, pady=10)

state_label = Label(data_frame, text='State')
state_label.grid(row=1, column=4, padx=10, pady=10)
state_entry = Entry(data_frame)
state_entry.grid(row=1, column=5, padx=10, pady=10)

zipcode_label = Label(data_frame, text='Zipcode')
zipcode_label.grid(row=1, column=6, padx=10, pady=10)
zipcode_entry = Entry(data_frame)
zipcode_entry.grid(row=1, column=7, padx=10, pady=10)


def clear_all():
    """ Clear all entry boxes """

    id_entry.config(state=NORMAL)
    # Clear entry boxes
    fn_entry.delete(0, END)
    id_entry.delete(0, END)
    ln_entry.delete(0, END)
    city_entry.delete(0, END)
    state_entry.delete(0, END)
    address_entry.delete(0, END)
    zipcode_entry.delete(0, END)


def move_up():
    """ Move row up """
    rows = my_tree.selection()
    for row in rows:
        my_tree.move(row, my_tree.parent(row), my_tree.index(row) - 1)


def move_down():
    """ Move row down """

    rows = my_tree.selection()
    for row in reversed(rows):
        my_tree.move(row, my_tree.parent(row), my_tree.index(row) + 1)


def remove_one_record():
    """ Remove a record """
    try:
        item = my_tree.selection()[0]
        my_tree.delete(item)
    except IndexError:
        return

    # Connect to database
    conn = sqlite3.connect(DATABASE_NAME)
    # Create cursor
    c = conn.cursor()

    try:
        #  Delete from database
        c.execute('DELETE FROM customers WHERE oid=:id',
                  {'id': id_entry.get()})

        # Commit then close
        conn.commit()

        # Clear entry boxes
        clear_all()

        # Add a confirmation message box
        messagebox.showinfo('Deleted!', 'Your record has been deleted!')
    except:
        messagebox.showerror('Error', 'Could not delete the selected record')
    conn.close()


def remove_many_records():
    """ Remove many records """

    # confirmation for deleting
    response = messagebox.askyesno(
        'WOAH!!!', 'This Will Delete all selected From the Table\nAre you sure?')

    # Check user answer
    if response == True:
        # Designate selection
        records = my_tree.selection()

        # List of IDs to delete
        ids_to_delete = []

        # Get the id from the treeview selection and add it to the ids_to_delete
        for record in records:
            ids_to_delete.append(my_tree.item(record, 'values')[2])

        # Connect to database
        conn = sqlite3.connect(DATABASE_NAME)
        # Create cursor
        c = conn.cursor()

        try:
            #  Delete the database table
            c.executemany('DELETE FROM customers WHERE oid=?',
                          [(a, ) for a in ids_to_delete])

            # Commit then close
            conn.commit()

            # Clear entry boxes
            clear_all()

            # Add a confirmation message box
            messagebox.showinfo(
                'Deleted!', 'The selected records have been deleted!')

        except:
            messagebox.showerror('Error', 'Could not delete the records')
        conn.close()


def remove_all_records():
    """ Remove many records """

    # confirmation for deleting
    response = messagebox.askyesno(
        'WOAH!!!', 'This Will Delete EVERYTHING From the Table\nAre you sure?')

    # Check user answer
    if response == True:
        for record in my_tree.get_children():
            my_tree.delete(record)

        # Connect to database
        conn = sqlite3.connect(DATABASE_NAME)
        # Create cursor
        c = conn.cursor()

        try:
            #  Delete the database table
            c.execute('DROP TABLE customers')
            # c.execute('DELETE FROM customers')

            # Commit then close
            conn.commit()

            # Clear entry boxes
            clear_all()

            # Add a confirmation message box
            messagebox.showinfo('Deleted!', 'All records have been deleted!')

        except:
            messagebox.showerror('Error', 'Could not delete the records')
        conn.close()
        create_table()


def update_record():
    """ Update a record """

    # Grab the record number
    selected = my_tree.focus()

    # Update record
    my_tree.item(selected, text='', values=(fn_entry.get(), ln_entry.get(
    ), id_entry.get(), address_entry.get(), city_entry.get(), state_entry.get(), zipcode_entry.get(),))

    # Update the database
    # Create a Database or connect to one that exists
    conn = sqlite3.connect(DATABASE_NAME)

    # Create a cursor instance
    c = conn.cursor()

    c.execute("""UPDATE customers SET
            first_name = :first_name,
            last_name = :last_name,
            address = :address, 
            city = :city,
            state=:state,
            zipcode=:zipcode
            WHERE oid= :oid
                """, {
        'first_name': fn_entry.get(),
        'last_name': ln_entry.get(),
        'address': address_entry.get(),
        'city': city_entry.get(),
        'state': state_entry.get(),
        'zipcode': zipcode_entry.get(),
        'oid': id_entry.get(),
    })

    conn.commit()
    conn.close()

    # Clear all entry boxes
    clear_all()


def add_record():
    """ Add new record to database """

    # Connect to database
    conn = sqlite3.connect(DATABASE_NAME)

    # Create a cursor instance
    c = conn.cursor()

    c.execute('INSERT INTO customers (first_name, last_name, id, address, city, state, zipcode) VALUES (:first_name, :last_name, :id, :address, :city, :state, :zipcode);',
              {
                  'first_name': fn_entry.get(),
                  'last_name': ln_entry.get(),
                  'id': id_entry.get(),
                  'address': address_entry.get(),
                  'city': city_entry.get(),
                  'state': state_entry.get(),
                  'zipcode': zipcode_entry.get(),
              })

    # Commit changes and close database
    conn.commit()
    conn.close()

    # Clear the entry boxes
    clear_all()

    # Create the Treeview Table
    my_tree.delete(*my_tree.get_children())

    # Get the data from the database to repopulat ethe Treeview
    query_database()


def select_record(e):
    """ Select a record """

    # Clear entry boxes
    clear_all()

    # Grab record number
    selected = my_tree.focus()
    # Grab the record value
    values = my_tree.item(selected, 'values')

    # output to entry boxes
    fn_entry.insert(0, values[0])
    ln_entry.insert(0, values[1])
    id_entry.insert(0, values[2])
    city_entry.insert(0, values[4])
    state_entry.insert(0, values[5])
    address_entry.insert(0, values[3])
    zipcode_entry.insert(0, values[6])
    id_entry.config(state=DISABLED)


def create_table():
    """ Create the table if not exists """
    # Create a Database or connect to one that exists
    conn = sqlite3.connect(DATABASE_NAME)

    # Create a cursor instance
    c = conn.cursor()

    # Create Table
    c.execute("""CREATE TABLE IF NOT EXISTS customers (
        first_name TEXT,
        last_name TEXT,
        id INTERGER,
        address TEXT,
        city TEXT,
        state TEXT,
        zipcode TEXT
    );
    """)

    # Add dummy data to table
    # data = [
    #     ["John", "Elder", 1, "123 Elder St.", "Las Vegas", "NV", "89137"],
    #     ["Mary", "Smith", 2, "435 West Lookout", "Chicago", "IL", "60610"],
    #     ["Tim", "Tanaka", 3, "246 Main St.", "New York", "NY", "12345"],
    #     ["Erin", "Erinton", 4, "333 Top Way.", "Los Angeles", "CA", "90210"],
    #     ["Bob", "Bobberly", 5, "876 Left St.", "Memphis", "TN", "34321"],
    #     ["Steve", "Smith", 6, "1234 Main St.", "Miami", "FL", "12321"],
    #     ["Tina", "Browne", 7, "654 Street Ave.", "Chicago", "IL", "60611"],
    #     ["Mark", "Lane", 8, "12 East St.", "Nashville", "TN", "54345"],
    #     ["John", "Smith", 9, "678 North Ave.", "St. Louis", "MO", "67821"],
    #     ["Mary", "Todd", 10, "9 Elder Way.", "Dallas", "TX", "88948"],
    #     ["John", "Lincoln", 11, "123 Elder St.", "Las Vegas", "NV", "89137"],
    #     ["Mary", "Bush", 12, "435 West Lookout", "Chicago", "IL", "60610"],
    #     ["Tim", "Reagan", 13, "246 Main St.", "New York", "NY", "12345"],
    #     ["Erin", "Smith", 14, "333 Top Way.", "Los Angeles", "CA", "90210"],
    #     ["Bob", "Field", 15, "876 Left St.", "Memphis", "TN", "34321"],
    #     ["Steve", "Target", 16, "1234 Main St.", "Miami", "FL", "12321"],
    #     ["Tina", "Walton", 17, "654 Street Ave.", "Chicago", "IL", "60611"],
    #     ["Mark", "Erendale", 18, "12 East St.", "Nashville", "TN", "54345"],
    #     ["John", "Nowerton", 19, "678 North Ave.", "St. Louis", "MO", "67821"],
    #     ["Mary", "Hornblower", 20, "9 Elder Way.", "Dallas", "TX", "88948"]

    # ]

    # for record in data:
    #     c.execute('INSERT INTO customers (first_name, last_name, id, address, city, state, zipcode) VALUES (:first_name, :last_name, :id, :address, :city, :state, :zipcode);',
    #               {
    #                   'first_name': record[0],
    #                   'last_name': record[1],
    #                   'id': record[2], 'address': record[3],
    #                   'city': record[4],
    #                   'state': record[5],
    #                   'zipcode': record[6]
    #               })

    # Commit changes
    conn.commit()

    # Close the connection
    conn.close()


# Add Buttons
button_frame = LabelFrame(root, text='Commands')
button_frame.pack(fill=X, expand=YES, padx=20)

update_button = Button(
    button_frame, text='Update Record', command=update_record)
update_button.grid(row=0, column=0, padx=10, pady=10)

add_button = Button(button_frame, text='Add Record', command=add_record)
add_button.grid(row=0, column=1, padx=10, pady=10)

remove_all_button = Button(
    button_frame, text='Remove All Records', command=remove_all_records)
remove_all_button.grid(row=0, column=2, padx=10, pady=10)

remove_one_button = Button(
    button_frame, text='Remove One Selected', command=remove_one_record)
remove_one_button.grid(row=0, column=3, padx=10, pady=10)

remove_many_button = Button(
    button_frame, text='Remove Many Selected', command=remove_many_records)
remove_many_button.grid(row=0, column=4, padx=10, pady=10)

move_up_button = Button(button_frame, text='Move Up', command=move_up)
move_up_button.grid(row=0, column=5, padx=10, pady=10)

move_down_button = Button(button_frame, text='Move Down', command=move_down)
move_down_button.grid(row=0, column=6, padx=10, pady=10)

select_record_button = Button(
    button_frame, text='Clear Entry Boxes', command=clear_all)
select_record_button.grid(row=0, column=7, padx=10, pady=10)

# Bind treeview
my_tree.bind('<ButtonRelease-1>', select_record)

# Run to pull data from database on start
create_table()
query_database()

# Launch event loop
root.mainloop()
