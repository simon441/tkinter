from tkinter import *
import pygame

root = Tk()
root.title('Sounds and Music')
root.iconbitmap("codemy.ico")
root.geometry("500x400")

pygame.mixer.init()

def play():
    pygame.mixer.music.load("audio/02 Harvest.mp3")
    pygame.mixer.music.play(loops=0)

def stop():
    pygame.mixer.music.stop()

my_button_play = Button(root, text="Play Song", font=("Helvetica", 32), command=play)
my_button_play.pack(pady=20)

my_button_stop = Button(root, text="Stop Song", font=("Helvetica", 32), command=stop)
my_button_stop.pack(pady=20)

root.mainloop()