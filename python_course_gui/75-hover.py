from tkinter import *

root = Tk()
root.title("Hover images")
root.iconbitmap('codemy.ico')
root.geometry("600x600")

def change(e):
    my_pic = PhotoImage(file='images/aspen2.png')    
    my_label.config(image=my_pic)
    my_label.image = my_pic

def change_back(e):
    my_pic = PhotoImage(file='images/aspen.png')    
    my_label.config(image=my_pic)
    my_label.image = my_pic

def do_something():
    my_label2 = Label(root, text="You clicked the button!")
    my_label2.pack()

my_pic = PhotoImage(file='images/aspen.png')
my_label = Button(root, image=my_pic, command=do_something)
my_label.pack(pady=20)

my_label.bind("<Enter>", change)
my_label.bind("<Leave>", change_back)

root.mainloop()