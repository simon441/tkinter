import tkinter as Tk
import pickle

root = Tk.Tk()
root.title("Save Textbox to Dat File")
root.iconbitmap("codemy.ico")
root.geometry('500x500')


def save_file():
    """
    Save text box data to file
    """
    # Grab the text from the text box
    stuff = my_text.get(1.0, Tk.END)

    # Define a filename
    filename = "data/dat_stuff"

    # Open the file
    with open(file=filename, mode="wb") as output_file:
        # Actually add the data to the file
        pickle.dump(stuff, output_file)


def open_file():

    # Define a filename
    filename = "data/dat_stuff"

    # Open the file
    with open(file=filename, mode="rb") as input_file:
        # Load the data from the file into a variable
        stuff = pickle.load(input_file)

        # Output to textbox
        my_text.insert(1.0, stuff)

        # print(stuff)


my_text = Tk.Text(root, width=20, height=20)
my_text.pack(pady=20)

my_button1 = Tk.Button(root, text="Save File", command=save_file)
my_button2 = Tk.Button(root, text="Open File", command=open_file)
my_button1.pack(pady=(10, 5))
my_button2.pack(pady=5)

root.mainloop()
