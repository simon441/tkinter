
from pathlib import Path

import tkinter
from tkinter import Button, Label, Text, ttk, messagebox
from tkinter.constants import END
from typing import Union
import googletrans
import textblob

root = tkinter.Tk()
root.title('Translator')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('880x300')


def get_language_key(search: str) -> Union[str, None]:
    """
    Search for a language in the languages dictionary
    returns the key if found, None if no match
    """
    for key, value in languages.items():
        if value == search:
            return key
    return None


def translate_it():
    """Translate text"""
    # Delete any previous translations
    translated_text.delete(1.0, END)
    try:
        # Get the LanguagesFrom the dictornary keys
        # Get the 'From' language Key
        from_language_key = get_language_key(original_combo.get())
        # Get the 'To' language Key
        to_language_key = get_language_key(translated_combo.get())

        # Turn Original text into TextBlob

        words = textblob.TextBlob(original_text.get(1.0, END))

        # Translate Text
        words = words.translate(
            from_lang=from_language_key, to=to_language_key)

        # Output translated text to screen
        translated_text.insert(1.0, words)

    except Exception as e:
        messagebox.showerror("Translator", str(e))


def clear():
    """Clear the text boxes"""
    original_text.delete(1.0, END)
    translated_text.delete(1.0, END)


# Grab Language list from GoogleTrans
languages = googletrans.LANGUAGES
# Convert to list
language_list = list(languages.values())

# Text Boxes
original_text = tkinter.Text(root, height=10, width=40)
original_text.grid(row=0, column=0, pady=20, padx=10)

translate_button = Button(root, text='Translate', font=(
    'Helvetica', 24), command=translate_it)
translate_button.grid(row=0, column=1, padx=10)

translated_text = tkinter.Text(root, height=10, width=40)
translated_text.grid(row=0, column=2, pady=20, padx=10)

# Combo Boxes
original_combo = ttk.Combobox(root, width=50, values=language_list)
original_combo.current(21)
original_combo.grid(row=1, column=0)

translated_combo = ttk.Combobox(root, width=50, values=language_list)
translated_combo.current(26)
translated_combo.grid(row=1, column=2)

# Clear button
clear_button = Button(root, text="Clear", command=clear)
clear_button.grid(row=2, column=1)

root.mainloop()
