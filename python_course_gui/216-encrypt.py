
import base64
from pathlib import Path
from tkinter import END, Button, Entry, Frame, Label, Text, Tk, messagebox
import types
import pybase64

root = Tk()
root.title('Encrypt/Decrypt in Base64')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('500x400')


class EncryptDecrypt():

    DEFAULT_ENCODING = 'ascii'
    DEFAULT_PASSWORD = 'elder'

    def __init__(self, master: Tk, encoding=DEFAULT_ENCODING, password = None) -> None:
        self.master = master
        self.build_widgets()

        self.encoding = encoding
        
        self.password = self.DEFAULT_PASSWORD
        if password is not None:
            self.password = password
            

    def build_widgets(self):
        """Build the Widgets"""

        # Frame for the buttons
        my_frame = Frame(self.master)
        my_frame.pack(pady=20)

        # Buttons
        self.encode_button = Button(my_frame, text='Encrypt', font=(
            'Helvatica', 18), command=self.encrypt)
        self.encode_button.grid(row=0, column=0)

        self.decrypt_button = Button(my_frame, text='Decrypt', font=(
            'Helvatica', 18), command=self.decrypt)
        self.decrypt_button.grid(row=0, column=1, padx=20)

        self.clear_button = Button(my_frame, text='Clear', font=(
            'Helvatica', 18), command=self.clear)
        self.clear_button.grid(row=0, column=2)

        #   Encode
        self.encode_label = Label(
            self.master, text='Encrypt/Decrypt Text...', font=('Helevetica', 14))
        self.encode_label.pack()

        #  Text to Encrypt/Decrypt
        self.my_text = Text(self.master, width=57, height=10)
        self.my_text.pack(pady=10)

        # Generated password
        self.password_label = Label(
            self.master, text='Enter your password...', font=('Helevetica', 14))
        self.password_label.pack()

        # Entry box
        self.my_entry = Entry(self.master, font=(
            'Helvetica', 18), width=35, show='*')
        self.my_entry.pack(pady=10)

    def encrypt(self):
        """encrypt Crypt text
        """
        # Get text from text box
        secret = self.my_text.get(1.0, END)
        # Clear the text box
        self.my_text.delete(1.0, END)

        # Logic for password
        if self.my_entry.get() == self.password:
            try:
                # Convert to bytes
                secret_bytes:bytes = secret.encode(encoding=self.encoding)

                # Convert to base64
                secret_encoded = pybase64.b64encode(secret_bytes)

                # Convert back to text
                secret:str = secret_encoded.decode(self.encoding)
                
                # Print to screen
                self.my_text.insert(END, secret)
    
            except Exception:      
                 # Flash message if error
                messagebox.showerror('Error!', 'Cannot encrypt text')
        
        else:
            # Flash message if wrong password
            messagebox.showwarning(
                'Incorrect!', 'Incorrect password, try again')

    def decrypt(self):
        """decrypt Decode text
        """
        # Get text from text box
        secret = self.my_text.get(1.0, END).strip()
 
        # Clear the screen
        self.my_text.delete(1.0, END)

        # Logic for password
        if self.my_entry.get().strip() == self.password:
            try:
                # Convert to bytes
                secret_bytes:bytes = secret.encode(encoding=self.encoding)

                # Convert to base64
                secret_encoded = pybase64.b64decode(secret_bytes)

                # Convert back to text
                secret:str = secret_encoded.decode(self.encoding)
                
                # Print to screen
                self.my_text.insert(END, secret)
            except Exception:      
                 # Flash message if error
                messagebox.showerror('Error!', 'Cannot decrypt text')
        else:
            # Flash message if wrong password
            messagebox.showwarning(
                'Incorrect!', 'Incorrect password, try again')

    def clear(self):
        """clear Clears text
        """
        # Clear boxes
        self.my_text.delete(1.0, END)
        self.my_entry.delete(0, END)

    def run(self):
        root.mainloop()


if __name__ == '__main__':
    app = EncryptDecrypt(root, password='password')
    app.run()
