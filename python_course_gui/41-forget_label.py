from tkinter import *

root = Tk()
root.title('Remove a Label')
root.iconbitmap('codemy.ico')
root.geometry("400x400")


def myClick():
    hello = "Hello " + e.get()

    global myLabel
    myLabel = Label(root, text=hello)
    e.delete(0, 'end')
    myLabel.pack(pady=10)
    myButton['state'] = DISABLED

def myDelete():
    if myLabel.winfo_exists() == 1:
        # do something
        do_something = 1
    else:
        do_something = 0
        #do something else
    # pack_forget
    # myLabel.pack_forget()

    # pack_destroy
    myLabel.pack_destroy()
    myButton['state'] = NORMAL
    print(myButton.winfo_exists())

e = Entry(root, width=50, font=('Helvetica', 30))
e.pack(padx=10, pady=10)

myButton = Button(root, text="Enter Your Name", command=myClick)
myButton.pack(pady=10)

deleteButton = Button(root, text='Delete text', command=myDelete)
deleteButton.pack(pady=10)

root.mainloop()