from tkinter import *

root = Tk()
root.title('Remove a Label')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# to be able to delete (destroy) a label in a scope (function),
# define it in the global scope at the top of the script
myLabel = Label(root)

def myClick():
    hello = "Hello " + e.get()

    global myLabel
    myLabel.grid_forget()
    myLabel = Label(root, text=hello)
    e.delete(0, 'end')
    myLabel.grid(row=3, column=0, pady=10)
    # myButton['state'] = DISABLED
'''
def myDelete():
    if myLabel.winfo_exists() == 1:
        # do something
        do_something = 1
    else:
        do_something = 0
        #do something else
    # grid_forget
    myLabel.grid_forget()

    # destroy
    myLabel.destroy()
    myButton['state'] = NORMAL
    print(myButton.winfo_exists())
'''

e = Entry(root, width=17, font=('Helvetica', 30))
e.grid(row=0, column=0, padx=10, pady=10)

myButton = Button(root, text="Enter Your Name", command=myClick)
myButton.grid(row=1, column=0, pady=10)

# deleteButton = Button(root, text='Delete text', command=myDelete)
# deleteButton.grid(row=2, column=0,pady=10)


root.mainloop()