from tkinter import *
from PIL import ImageTk, Image

root = Tk()
root.title('Flashcards!')
root.iconbitmap('codemy.ico')
root.geometry("500x500")

#############
# FUNCTIONS
#############

def states():
    """ State Flashcard Function """
    # hide all frames
    hide_all_frames()
    state_frame.pack(fill=BOTH, expand=1)
    my_label = Label(state_frame, text="States")
    my_label.pack()


def state_capitals():
    """ State capitals Flashcard Function """
    # hide all frames
    hide_all_frames()
    state_capitals_frame.pack(fill=BOTH, expand=1)
    my_label = Label(state_capitals_frame, text="Capitals")
    my_label.pack()


def hide_all_frames():
    """ Hide all frames """

    # Loop through all the children in each frame and delete them
    for widget in state_frame.winfo_children():
        widget.destroy()

    for widget in state_capitals_frame.winfo_children():
        widget.destroy()

    # hide frames
    state_frame.pack_forget()
    state_capitals_frame.pack_forget()


############
# MENU
############

# Create our menu
my_menu = Menu(root)
root.config(menu=my_menu)

# Create Geography menu items
states_menu = Menu(my_menu)
my_menu.add_cascade(label="Geography", menu=states_menu)

states_menu.add_command(label="States", command=states)
states_menu.add_command(label="State Capitals", command=state_capitals)
states_menu.add_separator()
states_menu.add_command(label="Exit", command=root.quit)

###########
# Frames
###########

state_frame = Frame(root, width=500, height=500)
state_capitals_frame = Frame(root, width=500, height=500)

# Launch GUI
root.mainloop()