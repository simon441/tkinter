from pathlib import Path
from tkinter import END, Button, Entry, Text, Tk
from tkinter.ttk import LabelFrame
from typing import Dict, List
from PyDictionary import PyDictionary

root = Tk()
root.title('English Dictionary')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('570x500')

def lookup():
    """lookup Search for entered text
    """
    # Clear the text box
    my_textbox.delete(1.0, END)

    # Lookup a word
    dictionary = PyDictionary()
    definition:Dict[str, List[str]] = dictionary.meaning(my_entry.get().strip())

    # Add definition to text box
    # my_textbox.insert(END, str(definition))

    # Find keys and values in definition
    for key, values in definition.items():
        # Put the key header in the text box
        my_textbox.insert(END, key + "\n\n")

        for value in values:
            my_textbox.insert(END, f"- {value}\n\n")



    
my_labelframe = LabelFrame(root, text="Enter A Word")
my_labelframe.pack(pady=20)

my_entry = Entry(my_labelframe, font=("Helvetica", 28))
my_entry.grid(row=0, column=0, padx=10, pady=10)

my_button = Button(my_labelframe, text="Lookup", command=lookup, font=("Helvetica",12))
my_button.grid(row=0, column=1, padx=10)

my_textbox = Text(root, height=20, width=65, wrap='word')
my_textbox.pack(pady=10)




if __name__ == '__main__':
    root.mainloop()
