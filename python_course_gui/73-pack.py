from tkinter import *

# Don't .pack() or .grid() on the same line if you want to change the widget afterwaqrs (with .config) 
# else you'll get a NonType error (you assign to your variable a packed Widget 
# and not a Widget so it cannot be used with widget methods)
# If you will not change your widget, you can do method chaining ending with .pack() or .grid()

root = Tk()
root.title("Don't pack on the same line")
root.iconbitmap('codemy.ico')
root.geometry("600x400")

def grab_same_line():
    my_label.config(text=my_box1.get())

def grab_not_same_line():
    my_label.config(text=my_box2.get())

# Do not do the following
my_box1 = Entry(root, font=("Helvetica", 28)).pack(pady=20)
#  but do this
my_box2 = Entry(root, font=("Helvetica", 28))
my_box2.pack(pady=20)

my_button1 = Button(root, text="Grab Text From Box Packed on same line", command=grab_same_line)
my_button1.pack(pady=20)

my_button1 = Button(root, text="Grab Text From Box not packed on same line", command=grab_not_same_line)
my_button1.pack(pady=20)

my_label = Label(root, text="")
my_label.pack(pady=20)

root.mainloop()