from tkinter import *
from tkinter import ttk
from tkinter import messagebox

root = Tk()
root.title('Limit the number of opening windows')
root.iconbitmap('codemy.ico')
root.geometry("500x500")

global window_count
window_count = 1

MAX_WINDOW_NUMBER = 2


def open():
    """ Open a new window """
    global window_count

    # Create counter logic
    if window_count < MAX_WINDOW_NUMBER:
        window_count += 1

        top = Toplevel()
        top.title('New Window')
        top.iconbitmap('codemy.ico')
        top.geometry("500x500")

        my_label = Label(top, text='New Window!', font=('Helvetica', 24))
        my_label.pack(padx=50, pady=50)
    else:
        messagebox.showinfo(
            'Error', "Hey! You've already opened a new window!")


my_button = Button(root, text='Open Window', command=open)
my_button.pack(pady=50, padx=50)

root.mainloop()
