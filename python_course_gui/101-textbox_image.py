from tkinter import *
from tkinter import filedialog
import os

root = Tk()
root.title('Text box')
root.iconbitmap("codemy.ico")
root.geometry("500x600")

def open_file():
#    text_file = open("data/sample.txt", 'r')
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Open Text File", filetypes=(("Text Files", "*.txt"),))
    text_file = open(file_path, 'r')
    temp_text = text_file.read()

    my_textbox.insert(END, temp_text)
    text_file.close()

# Read only r  
# Read and Write r+  (beginning of file)
# Write Only w   (over-written)
# Write and Read w+  (over written)
# Append Only a  (end of file)
# Append and Read a+  (end of file)

def save_file():
    # text_file = open('data/sample.txt', 'w')
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Save Text File", filetypes=(("Text Files", "*.txt"),))
    text_file = open(file_path, 'w')
    text_file.write(my_textbox.get(1.0, END))
    # text_file.close()

def add_image():
    # Add Image
    global my_image
    my_image = PhotoImage(file='images/login.png')
    position = my_textbox.index(INSERT)
    my_textbox.image_create(position, image=my_image)

    my_label.config(text=position)

my_frame = Frame(root)
my_frame.pack(pady=10)


# scrollbar
my_scollbar = Scrollbar(my_frame)
my_scollbar.pack(side=RIGHT, fill=Y)


my_textbox = Text(my_frame, width=40, height=10, font=('Helvetica', 16), selectbackground="yellow", selectforeground="black", yscrollcommand=my_scollbar.set)
my_textbox.pack(pady=20)

# Configure the scrollbar
my_scollbar.config(command=my_textbox.yview)

# buttons
open_button = Button(root, text="Open Text File", command=open_file)
open_button.pack(pady=20)

save_button = Button(root, text="Save Text File", command=save_file)
save_button.pack(pady=20)

image_button = Button(root, text="Add Image", command=add_image)
image_button.pack(pady=5)

my_label = Label(root, text="")
my_label.pack()

root.mainloop()