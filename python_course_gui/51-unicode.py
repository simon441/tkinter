from tkinter import Tk, Label, Button

root = Tk()
root.title('Unicode')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# u00b0 = ° (degrees symbol)
# unicode tables: https://unicode-table.com/fr/

my_label = Label(root, text='41' + u'\u00b0', font=('Helvetica', 32)).pack(pady=10)

my_button = Button(root, text=u'\u00BB', font=('Helvetica', 32)).pack(pady=10)

# Launch GUI
root.mainloop()