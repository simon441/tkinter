from tkinter import *
import sqlite3

root = Tk()

root.title("Simon")
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# Create a Database
conn = sqlite3.connect("address_book.db")

# Create cursor
c = conn.cursor()

# Create table
c.execute(""" CREATE TABLE addresses (
        first_name TEXT,
        last_name TEXT,
        address TEXT,
        city TEXT,
        state TEXT,
        zipcode INTEGER
    ) """)

# Commit Changes
conn.commit()

# Close Connection
conn.close()


root.mainloop()
