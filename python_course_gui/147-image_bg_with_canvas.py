import tkinter as Tk

root = Tk.Tk()
root.title("Set Image as Background")
root.iconbitmap("codemy.ico")
root.geometry('800x500')

# Define image
bg = Tk.PhotoImage(file="images/space.png")


# Create a label
my_label = Tk.Label(root, image=bg)
my_label.place(x=0, y=0, relwidth=1, relheight=1)

# Create a canvas
my_canvas = Tk.Canvas(root, width=800, height=500)
my_canvas.pack(fill=Tk.BOTH, expand=True)

# Set an image in canvas
my_canvas.create_image(0, 0, image=bg, anchor=Tk.NW)

# Add a label
my_canvas.create_text(400, 250, text="Welcome!",
                      font=("Helvetica", 50), fill="white")

# Add some buttons
my_button1 = Tk.Button(root, text="Start")
my_button2 = Tk.Button(root, text="Reset Score")
my_button3 = Tk.Button(root, text="Exit")

my_button1_window = my_canvas.create_window(
    10, 10, anchor=Tk.NW, window=my_button1)
my_button2_window = my_canvas.create_window(
    100, 10, anchor=Tk.NW, window=my_button2)
my_button3_window = my_canvas.create_window(
    230, 10, anchor=Tk.NW, window=my_button3)

root.mainloop()
