from tkinter import Tk, OptionMenu, StringVar, Button, Label, ttk

root = Tk()
root.title('Dropdown Boxes and Combobox event binding')
root.iconbitmap('codemy.ico')
root.geometry("400x400")
    
def selected(event):
    # myLabel = Label(root, text=clicked.get()).pack()

    if clicked.get() == 'Friday':
        myLabel = Label(root, text="It's " + clicked.get() + ": Hold tight! It's almost the weekend").pack()
    elif clicked.get() == 'Monday':
        myLabel = Label(root, text="It's " + clicked.get() + ": Oh... It's the begining of the week").pack()
    elif clicked.get() in ('Saturday', 'Sunday'):
        myLabel = Label(root, text="It's " + clicked.get() + ": It's the weekend! Try to relax").pack()
    else:
        myLabel = Label(root, text="It's " + clicked.get() + ": Work day").pack()

def combo_click(event):
    myLabel = Label(root, text=myCombo.get()).pack()

options = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursdday",
        "Friday",
        "Saturday",
        "Sunday"
]

clicked = StringVar()
clicked.set(options[0]) # select Monday

# Dropdown
drop = OptionMenu(root, clicked, *options, command=selected)
drop.pack(pady=20)

# Combobox
myCombo = ttk.Combobox(root, value=options) 
myCombo.current(0) # set defalut value to "Monday" (first value in list)
myCombo.bind('<<ComboboxSelected>>', combo_click)
myCombo.pack()

# myButton = Button(root, text="select a day from the list", command=selected)
# myButton.pack()


root.mainloop()