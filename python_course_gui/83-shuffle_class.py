# from tkinter import Tk, Label, Button, Entry
import tkinter
from random import choice, shuffle

class WordJumble(tkinter.Tk):

    
    def __init__(self, parent, words):
        tkinter.Tk.__init__(self)
        self.words = words
        self.initialize()

    def initialize(self):
        pass
        # Game
        title_label = tkinter.Label(self, text="  Word Jumble  ", font=("Helvetica", 24), fg="blue", borderwidth=2, relief="ridge")
        title_label.pack(pady=10)

        self.my_label = tkinter.Label(self, text="", font=("Helvetica", 48))
        self.my_label.pack()
        
        self.entry_answer = tkinter.Entry(self, font=("Helvetica", 20))
        self.entry_answer.pack(pady=20)

        button_frame = tkinter.Frame(self)
        button_frame.pack(pady=20)

        my_button = tkinter.Button(button_frame, text="Pick another word", command=self.shuffle_words)
        my_button.grid(row=0, column=0, padx=10)

        answer_button = tkinter.Button(button_frame, text="Answer", command=self.answer)
        answer_button.grid(row=0, column=1, padx=10)

        self.answer_label = tkinter.Label(self, text="", font=("Helvetica", 18))
        self.answer_label.pack(pady=20)
        


    def answer(self):
        """ Verify answer """
        if self.word.lower() == self.entry_answer.get().lower():
            self.answer_label.config(text="Correct")
        else:
            self.answer_label.config(text="Incorrect")


    def shuffle_words(self):
        """ Shuffle the words and outputs a shuffled word """
        # List of words to search
   
        # Pick random word from list
        self.word = choice(self.words)
        self.my_label.config(text=self.word)

        # Break apart our chosen self.word
        break_apart_word = list(self.word.replace(' ', ''))
        shuffle(break_apart_word)
        print(self.word, break_apart_word)
        break_apart_word = list(map(lambda x: x.lower(), break_apart_word))

        # turn shuffled list into a word
        shuffled_word = "".join(break_apart_word)

        self.my_label.config(text=shuffled_word)

if __name__ == "__main__":
    words = ['Washington', 'Oregon', 'California', 'Ohio', 'Nebraska', 'Colorado', 'Michigan', 'Massachusetts', 'Florida', 'Texas', 'Oklahoma', 'Hawaii', 'Alaska', 'Utah', 'New Mexico', 'North Dakota', 'South Dakota', 'West Virginia', 'Virginia', 'New Jersey', 'Minnesota', 'Illinois', 'Indiana', 'Kentucky', 'Tennessee', 'Georgia', 'Alabama', 'Mississippi', 'North Carolina', 'South Carolina', 'Maine', 'Vermont', 'New Hampshire', 'Connecticut', 'Rhode Island', 'Wyoming', 'Montana', 'Kansas', 'Iowa', 'Pennsylvania', 'Maryland', 'Missouri', 'Arizona', 'Nevada', 'New York', 'Wisconsin', 'Delaware', 'Idaho', 'Arkansas', 'Louisiana']

    app = WordJumble(None, words)
    app.title("Word Jumble Game")
    app.iconbitmap("codemy.ico")
    app.geometry("600x400")

    app.mainloop()
