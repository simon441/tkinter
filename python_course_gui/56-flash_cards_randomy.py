from random import randint
import random
# from os import listdir, path

# STATES_DIR = 'states'

# def _state_listing():
#     # Create a list of state names
#     files_list = listdir(STATES_DIR)
#     # print(files_list)
#     our_states = [x.replace('.png', '') for x in files_list]
#     # print(states)
#     return our_states

# STATES_LIST_GEOGRAPH = _state_listing()

answer_list = []

states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}
state_capitals = {"Washington":"Olympia","Oregon":"Salem",\
                    "California":"Sacramento","Ohio":"Columbus",\
                    "Nebraska":"Lincoln","Colorado":"Denver",\
                    "Michigan":"Lansing","Massachusetts":"Boston",\
                    "Florida":"Tallahassee","Texas":"Austin",\
                    "Oklahoma":"Oklahoma City","Hawaii":"Honolulu",\
                    "Alaska":"Juneau","Utah":"Salt Lake City",\
                    "New Mexico":"Santa Fe","North Dakota":"Bismarck",\
                    "South Dakota":"Pierre","West Virginia":"Charleston",\
                    "Virginia":"Richmond","New Jersey":"Trenton",\
                    "Minnesota":"Saint Paul","Illinois":"Springfield",\
                    "Indiana":"Indianapolis","Kentucky":"Frankfort",\
                    "Tennessee":"Nashville","Georgia":"Atlanta",\
                    "Alabama":"Montgomery","Mississippi":"Jackson",\
                    "North Carolina":"Raleigh","South Carolina":"Columbia",\
                    "Maine":"Augusta","Vermont":"Montpelier",\
                    "New Hampshire":"Concord","Connecticut":"Hartford",\
                    "Rhode Island":"Providence","Wyoming":"Cheyenne",\
                    "Montana":"Helena","Kansas":"Topeka",\
                    "Iowa":"Des Moines","Pennsylvania":"Harrisburg",\
                    "Maryland":"Annapolis","Missouri":"Jefferson City",\
                    "Arizona":"Phoenix","Nevada":"Carson City",\
                    "New York":"Albany","Wisconsin":"Madison",\
                    "Delaware":"Dover","Idaho":"Boise",\
                    "Arkansas":"Little Rock","Louisiana":"Baton Rouge"}

our_states = list(states.values())#list(map(lambda x: x.lower(), list(states.values())))

our_states_capitals = state_capitals

# rando = randint(0, len(our_states) - 1)

# print(our_states_capitals[our_states[rando]])

# answer = our_states[rando]

'''
# add our first random selection to our answer_list list
answer_list.append(our_states[rando])

# print(our_states[rando], our_states)

# remove answer from list
our_states.remove(our_states[rando])

# Shuffle the original list
random.shuffle(our_states)

# Randomly select our next state
rando = randint(0, len(our_states) - 1)


# add our second random selection to our answer_list list
answer_list.append(our_states[rando])

# print(answer, our_states)

# remove 2nd answer from list
our_states.remove(our_states[rando])

# Shuffle the original list
random.shuffle(our_states)

# Randomly select our next state
rando = randint(0, len(our_states) - 1)


# add our third random selection to our answer_list list
answer_list.append(our_states[rando])

# print(answer, our_states)

# remove 3rd answer from list
our_states.remove(our_states[rando])

print(answer_list, answer)

'''

count = 1
max_answers = 3

for i in range(max_answers):
    # Randomly select our state
    rando = randint(0, len(our_states) - 1)

    # If first selection, make it our answer
    if i == 0:
        answer = our_states[rando]

    # add our random selection to our answer_list list
    answer_list.append(our_states[rando])

    # remove from old list
    our_states.remove(our_states[rando])

    # Shuffle the original list
    random.shuffle(our_states)

print(answer, answer_list)