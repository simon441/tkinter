from tkinter import messagebox, Button, Toplevel, PhotoImage, Label, Frame
from pathlib import Path
import tkinter as Tk
from openpyxl.workbook import Workbook
from openpyxl import load_workbook

root = Tk.Tk()
root.title("Excel spreadsheet to listbox")
root.iconbitmap("codemy.ico")
root.geometry("300x300")


def select(event):
    """ Select an item and display its contents in the label """
    my_label.config(text=my_listbox.get(Tk.ANCHOR))


# Create Listbox
my_listbox = Tk.Listbox(root, width=45)
my_listbox.pack(pady=20)

# Create a Workbook
wb = load_workbook(str(Path.joinpath(Path.cwd(), 'data', 'name_color.xlsx')))
# Set active worksheet
ws = wb.active

# Grab a column of data
col_a = ws["A"]
col_b = ws["B"]

for item in col_a:
    my_listbox.insert(Tk.END, item.value)

my_label = Tk.Label(root, text='Select Item...', font=("Helvetica", 18))
my_label.pack(pady=20)

# Create listbox binding
my_listbox.bind("<ButtonRelease-1>", select)

# start
root.mainloop()
