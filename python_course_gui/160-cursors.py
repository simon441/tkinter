from tkinter import *
root = Tk()
root.title("Cursors")
root.iconbitmap("codemy.ico")
root.geometry('500x550')

# my_button = Button(root, text="Something", cursor="star")
# my_button.pack(pady=20)

list = [
    "arrow",
    "circle",
    "clock",
    "cross",
    "dotbox",
    "exchange",
    "fleur",
    "heart",
    "man",
    "mouse",
    "pirate",
    "plus",
    "shuttle",
    "sizing",
    "spider",
    "spraycan",
    "star",
    "target",
    "tcross",
    "trek",
]

# col = 0
# row = 0
# for cursor in list:
#     Button(root, text=cursor, cursor=cursor, width=10, height=5,
#            fg="darkblue").grid(row=row, column=col, pady=10, padx=10)
#     row += 1
#     if row % 5 == 0:
#         row = 0
#         col += 1
# Total row- 5
count = 0
for i in range(5):  # Total row- 5
    for j in range(4):  # Total column - 4
        Button(root, text=list[count], cursor=list[count], width=10, height=5, fg="darkblue").grid(
            row=i, column=j, padx=10, pady=10)
        count += 1
root.mainloop()
