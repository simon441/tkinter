from tkinter import Tk, Button, Label

root = Tk()
root.title('Height and Width Of Tkinter App')
root.iconbitmap("codemy.ico")
root.geometry("800x800")

def info():
    dimension_label = Label(root, text=root.winfo_geometry())
    dimension_label.pack(pady=20)

    height_label = Label(root, text=f"Height:{root.winfo_height()}")
    height_label.pack(pady=20)
    width_label = Label(root, text=f"Width: {root.winfo_width()}")
    width_label.pack(pady=20)

    x_label = Label(root, text=f"X position: {root.winfo_x()}")
    x_label.pack(pady=20)
    y_label = Label(root, text=f"Y position: {root.winfo_y()}")
    y_label.pack(pady=20)

my_button = Button(root, text="Get Window Dimensions", command=info)
my_button.pack(pady=20)



root.mainloop()