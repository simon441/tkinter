from tkinter import *
root = Tk()
root.title("Flip the Switch!")
root.iconbitmap("codemy.ico")
root.geometry('500x550')


# Keep track of the button state on/off
global is_on
is_on = True


def switch():
    """
    Make the button image on/off
    """
    global is_on

    # Determine is is on or off
    if is_on:
        on_button.config(image=off)
        my_label.config(text="The Switch is Off", fg="grey")
    else:
        on_button.config(image=on)
        my_label.config(text="The Switch is On", fg="green")
    is_on = not is_on


# Create Label
my_label = Label(root, text="The Switch Is On",
                 fg="green", font=("Helvetica", 32))
my_label.pack(pady=20)

# Define the images
on = PhotoImage(file="images/on.png")
off = PhotoImage(file="images/off.png")

# Create a Button
on_button = Button(root, image=on, bd=0, command=switch)
on_button.pack(pady=50)

root.mainloop()
