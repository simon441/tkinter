from tkinter import messagebox, Button, Tk, Menu, Label, DISABLED

class TicTacToe(Tk):
    def __init__(self, master):
        ############
        # Variables
        self.master = master

        # button font
        self.SYSTEM_FACE = "SystemButtonFace"

        # background of winning tiles
        self.BG_WON = "red"

        # number of button
        self.BUTTONS_MAX_RANGE = 9


        # list of all buttons
        self.button_list = []

        # whose turn (X or O)
        self.winner = False

        # Xs starts as True as game starts with X
        self.clicked = True

        # count how many clicked occured
        self.count = 0

        self.create_widgets()


    def create_widgets(self):
         # Menu
        my_menu = Menu(self.master)
        self.master.config(menu=my_menu)

        # Options Menu
        options_menu = Menu(my_menu, tearoff=False)
        my_menu.add_cascade(label="Options", menu=options_menu)
        options_menu.add_command(label="Reset Game", command=self.reset)
        options_menu.add_command(label="Quit Game", command=self.quit_app)

    def quit_app(self):
        msg = messagebox.askokcancel('Tic Tac Toe', 'Do your want to quit the Game ?')
        if msg:
            self.master.destroy()


    def disable_all_buttons(self):
        """ Disable buttons """

        for _ in range(self.BUTTONS_MAX_RANGE):
            self.button_list[_].config(state=DISABLED)
        # b1.config(state=DISABLED)
        # b2.config(state=DISABLED)
        # b3.config(state=DISABLED)
        # b4.config(state=DISABLED)
        # b5.config(state=DISABLED)
        # b6.config(state=DISABLED)
        # b7.config(state=DISABLED)
        # b8.config(state=DISABLED)
        # b9.config(state=DISABLED)


    def check_win_conditions(self, player):
        win_message = "Congratulations!\n" + player + " Won!"

        patterns = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
                    (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 5, 6)]
        # print(self.button_list)
        for p in patterns:
            # print(p)
            # print(player, self.button_list[p[0]]["text"], self.button_list[p[1]]["text"], self.button_list[p[2]]["text"])
            if self.button_list[p[0]]["text"] == player and self.button_list[p[1]]["text"] == player and self.button_list[p[2]]["text"] == player:
                self.button_list[p[0]].config(bg=self.BG_WON)
                self.button_list[p[1]].config(bg=self.BG_WON)
                self.button_list[p[2]].config(bg=self.BG_WON)
                self.winner = True
                messagebox.showinfo("Tic Tac Toe", win_message)
                self.disable_all_buttons()
                return True
        # print("iswinner", winner)
        return False


    def check_if_won(self, whose_turn):
        """ Check to see if someone won """

        # X turn?
        if whose_turn == True:
            self.check_win_conditions('X')
        # O turn?
        else:
            self.check_win_conditions('O')

        # Check if tie
        if self.count == 9 and self.winner == False:
            messagebox.showinfo("Tic Tac Toe", "It's a Tie!\nNo one wins!")
            self.disable_all_buttons()

    def button_click_list(self, btn_id):
        """ Actions on button click """

        btn = self.button_list[btn_id]
        # print('id', btn_id, btn, btn['text'])
        # button has not been already selected
        if btn['text'] == " ":
            # for the X player
            if self.clicked == True:
                btn["text"] = "X"
            # for the O player
            else:
                btn["text"] = "O"
            self.check_if_won(self.clicked)

            #  change player's turn
            self.clicked = not self.clicked
            # another round
            self.count += 1
        else:
            messagebox.showerror(
                "Tic Tac Toe", "Hey! That box has already been selected\nPick another box...")

    def start(self):
        """ Start the game """
        self.reset()

    def reset(self):
        """ Start the game over """
        self.clicked = True
        self.count = 0

        # Build the Buttons
        x = 0
        y = 0
        self.button_list.clear()
        for _ in range(0, 9):
            b = Button(self.master, text=" ", font=("Helvetica", 20), height=3,
                    width=6, bg=self.SYSTEM_FACE, command=lambda id = _: self.button_click_list(id))
            b.grid(row=x, column=y)
            if y == 2:
                y = 0
                x += 1
            else:
                y += 1
            self.button_list.append(b)


#################################
# Start GUI and Launch Event Loop
#################################

root = Tk()
root.title("Tic-Tac-Toe")
root.iconbitmap("codemy.ico")

app = TicTacToe(root)
app.start()

root.mainloop()
