from tkinter import *
from random import choice, shuffle

root = Tk()
root.title("Word Jumble Game")
root.iconbitmap("codemy.ico")
root.geometry("600x400")

# Game
title_label = Label(root, text="  Word Jumble  ", font=("Helvetica", 24), fg="blue", borderwidth=2, relief="ridge")
title_label.pack(pady=(20, 40))

my_label = Label(root, text="", font=("Helvetica", 48))
my_label.pack()

def answer():
    """ Verify answer """
    if word.lower() == entry_answer.get().lower():
        answer_label.config(text="Correct")
    else:
        answer_label.config(text="Incorrect")

def shuffle_words():

    # List of words to search
    words = ['Washington', 'Oregon', 'California', 'Ohio', 'Nebraska', 'Colorado', 'Michigan', 'Massachusetts', 'Florida', 'Texas', 'Oklahoma', 'Hawaii', 'Alaska', 'Utah', 'New Mexico', 'North Dakota', 'South Dakota', 'West Virginia', 'Virginia', 'New Jersey', 'Minnesota', 'Illinois', 'Indiana', 'Kentucky', 'Tennessee', 'Georgia', 'Alabama', 'Mississippi', 'North Carolina', 'South Carolina', 'Maine', 'Vermont', 'New Hampshire', 'Connecticut', 'Rhode Island', 'Wyoming', 'Montana', 'Kansas', 'Iowa', 'Pennsylvania', 'Maryland', 'Missouri', 'Arizona', 'Nevada', 'New York', 'Wisconsin', 'Delaware', 'Idaho', 'Arkansas', 'Louisiana']

    # Pick random word from list
    global word
    word = choice(words)
    my_label.config(text=word)

    # Break apart our chosen word
    break_apart_word = list(word.replace(' ', ''))
    shuffle(break_apart_word)
    print(word, break_apart_word)
    break_apart_word = list(map(lambda x: x.lower(), break_apart_word))

    # turn shuffled list into a word
    shuffled_word = "".join(break_apart_word)

    my_label.config(text=shuffled_word)


entry_answer = Entry(root, font=("Helvetica", 20))
entry_answer.pack(pady=20)

button_frame = Frame(root)
button_frame.pack(pady=20)

my_button = Button(button_frame, text="Pick another word", command=shuffle_words)
my_button.grid(row=0, column=0, padx=10)

answer_button = Button(button_frame, text="Answer", command=answer)
answer_button.grid(row=0, column=1, padx=10)

answer_label = Label(root, text="", font=("Helvetica", 18))
answer_label.pack(pady=20)

root.mainloop()