from tkinter import *
from PIL import ImageTk, Image
from tkinter import filedialog
import os

root = Tk()

root.title("Simon")
root.iconbitmap('codemy.ico')

# def open():
#     global my_image

#     root.filename = filedialog.askopenfilename(initialdir='images/', title="Select A File", filetypes=(("png files", "*.png"), ("jpeg files", "*.jpg;*.jpeg"),("All files", "*.*"),  ))

#     my_label = Label(root, text=root.filename).pack()

#     my_image = ImageTk.PhotoImage(Image.open(root.filename))
#     my_image_label = Label(root, image=my_image).pack()


# # Label(root, text="Image viewer", font=("Arial", 15)).pack(padx=5, pady=10)

# # my_button = Button(root, text="Open an image file", command=open).pack(pady=10)

def open():
    global my_image

    root.filename = filedialog.askopenfilename(initialdir='images/', title="Select A File", filetypes=(("png files", "*.png"), ("jpeg files", "*.jpg;*.jpeg"),("All files", "*.*"),  ))

    my_label = Label(root, text=root.filename).grid(row=2, column=0)

    my_image = ImageTk.PhotoImage(Image.open(root.filename))
    my_image_label = Label(root, image=my_image).grid(row=3, column=0)



Label(root, text="Image viewer", font=("Arial", 15)).grid(row=0, column=0,padx=5, pady=10)

my_button = Button(root, text="Open an image file", command=open).grid(row=1, column=0,pady=10)

root.mainloop()
