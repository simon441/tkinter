from tkinter import *
from tkinter import messagebox

root = Tk()

root.title("Simon")
root.iconbitmap('codemy.ico')

def popup():
    """ show err/info/warning messages """
    messagebox.showinfo("This is my popup", "Hello World!")
    messagebox.showerror("This is my popup", "Hello World!")
    messagebox.showwarning("This is my popup", "Hello World!")

def popup_ask():
    """ ask a question """
    response = messagebox.askquestion("This is my popup", "Hello World!")
    # messagebox.askyesno("This is my popup", "Hello World!")
    Label(root, text="Question: " + response).pack()

def popup_askyesno():
    response = messagebox.askyesno("This is my popup", "Hello World!")
    if response == 1:
        Label(root, text="You clicked yes").pack()
    else:
        Label(root, text="You clicked no").pack()
        

def popup_askyesnocancel():
    response = messagebox.askyesnocancel("This is my popup", "Hello World!")
    print(response)
        

Button(root, text="Popup", command=popup).pack()
Button(root, text="Popup ask", command=popup_ask).pack()
Button(root, text="Popup ask (yes/no)", command=popup_askyesno).pack()
Button(root, text="Popup ask (yes/no/cancel)", command=popup_askyesnocancel).pack()

root.mainloop()