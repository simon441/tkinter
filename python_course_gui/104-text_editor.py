from tkinter import *
from tkinter import filedialog, font
import os
from pathlib import Path

root = Tk()
root.title("Simon Cateau's TextPad!")
root.iconbitmap("codemy.ico")
root.geometry("1200x660")

# Main Frame
main_frame = Frame(root)
main_frame.pack(pady=5)

# Scrollbar for the Text box
text_scroll = Scrollbar(main_frame)
text_scroll.pack(side=RIGHT, fill=Y)

# Text box
my_textbox = Text(main_frame, width=97, height=25, font=("Helvetica", 16), selectbackground="yellow", selectforeground="black", undo=True, yscrollcommand=text_scroll.set)
my_textbox.pack()

# Configure the scrollbar
text_scroll.config(command=my_textbox.yview)

########### 
# Menu bar
###########
main_menu = Menu(root)
root.config(menu=main_menu)

# Add File Menu
file_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New")
file_menu.add_command(label="Open")
file_menu.add_command(label="Save")
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)

# Add Edit Menu
edit_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Undo")
edit_menu.add_command(label="Redo")
edit_menu.add_separator()
edit_menu.add_command(label="Cut")
edit_menu.add_command(label="Copy")
edit_menu.add_command(label="Paste")


##########
# Status bar to bottom o fapp
status_bar = Label(root, text='Ready    ', anchor=E)
status_bar.pack(fill=X, side=BOTTOM, ipady=5)

root.mainloop()