from tkinter import *
from tkcalendar import *
from datetime import datetime

root = Tk()
root.title("Date Picker")
root.iconbitmap('codemy.ico')
root.geometry("600x400")

cal = Calendar(root, seleectmode="day", year=2020, month=5, day=datetime.now().day)
cal.pack(pady=20, fill="both", expand=True)

def grab_date():

    my_label.config(text="The selected date is: " + cal.get_date())

my_button = Button(root, text="Get Date", command=grab_date)
my_button.pack(pady=20)


my_button_today = Button(root, text="Back to TODAY", command=lambda: cal.selection_set(datetime.now()))
my_button_today.pack(pady=20)

my_label = Label(root, text="")
my_label.pack(pady=10)

root.mainloop()