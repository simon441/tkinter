from tkinter import *

root = Tk()

# DISABLED BUTTON
# myButton = Button(root, text="Click Me!", state=DISABLED)

# sized button with padx and pady
# myButton = Button(root, text="Click Me!", padx=50, pady=50)

# button with an action
def myClick():
    myLabel = Label(root, text="Look you clicked a Button!!")
    myLabel.pack()

# myButton = Button(root, text="Click Me!", command=myClick)

#  button colors
myButton = Button(root, text="Click Me!", fg="blue", bg="#c3c3c3")

myButton.pack()

root.mainloop()