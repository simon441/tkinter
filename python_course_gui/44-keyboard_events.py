from tkinter import *

##############################
# EVENT BINDING:
# [widget].bind(eventType, action)
# widget.bind(event, handler)
# If the defined event occurs in the widget, the "handler" function is called with an event object.
#    describing the event. 
# 
# eventType: "<modifier-type-detail>"
#               type: <Button> , <Motion> , <ButtonRelease>, <Double-Button> , 
#                       <Enter> , <Leave> , <FocusIn> , <FocusOut> ,
#                       <Return> , <Key> , a(keyboard key name), <Shift-Up> ,
#                       <Configure>
#           The type field is the essential part of an event specifier, whereas the "modifier"
#            and "detail" fields are not obligatory and are left out in many cases. 
#               They are used to provide additional information for the chosen "type". 
#           The event "type" describes the kind of event to be bound, e.g. actions like mouse clicks,
#            key presses or the widget got the input focus. 
# action: action to perform on fired event
#############################

root = Tk()
root.title('Keyboard events')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

def clicker(event):
    myLabel = Label(root, text="You clicked this button " + event.char)
    myLabel.pack()

myButton = Button(root, text="Click Me!") 
myButton.bind("<Key>", clicker)
myButton.pack(pady=20)

root.mainloop()