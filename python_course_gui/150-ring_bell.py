import tkinter as Tk

root = Tk.Tk()
root.title("Ring The Bell!!!")
root.iconbitmap("codemy.ico")
root.geometry('500x500')

# Define image
bell = Tk.PhotoImage(file="images/bell.png")

# Add image to label
bell_label = Tk.Label(root, image=bell)
bell_label.pack(pady=20)


def ring():
    """
    Makes the system bell ring
    """
    root.bell()


# Create the button
my_button = Tk.Button(root,
                      text="Ring The Bell",
                      command=ring,
                      font=("Helvetica", 24),
                      fg="#4d4d4d"
                      )
my_button.pack(pady=10)

root.mainloop()
