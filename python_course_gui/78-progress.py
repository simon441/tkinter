from tkinter import *
from tkinter import ttk
import time

root = Tk()
root.title("Progress bar")
root.iconbitmap('codemy.ico')
root.geometry("600x400")

def step():
    my_progress['value'] += 20
    # my_progress.start(20)
    my_label.config(text=my_progress['value'])

def step_auto():
    #  5 is 100 (progress bar length)/20
    for i in range(5):
        my_progress['value'] += 20
        my_label.config(text=my_progress['value'])
        root.update_idletasks()
        time.sleep(1)

def stop():
    # my_progress['value'] += 20
    my_progress.stop()



# Initialize progress bar
# 
# orient => progress bar orientaion: HORIZONTAL, VERTICAL
# 
# length: size of the progress bar on the scrrren
# 
# mode => 
#       - determinate: full bar that goes from left to right,
#       - indeterminate: dot to show progress and goes left to right then right to left
my_progress = ttk.Progressbar(root, orient=HORIZONTAL, length=300, mode='determinate')
my_progress.pack(pady=20)

my_button = Button(root, text="Manual Progress Bar", command=step)
my_button.pack(pady=20)

my_button = Button(root, text="Automatic Progress Bar", command=step_auto)
my_button.pack(pady=20)

my_button2 = Button(root, text="Stop / Reset", command=stop)
my_button2.pack(pady=20)

my_label = Label(root, text="")
my_label.pack(pady=20)

root.mainloop()