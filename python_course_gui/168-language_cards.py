from tkinter import *
from random import randint

root = Tk()
root.title("Spanish Language Flashcards")
root.iconbitmap("codemy.ico")
root.geometry('550x410')

words = [
    (('Me llamo'), ('My name is ')),
    (('Mi Nombre es'), ('My name is ')),
    (('Hola, soy Markus'), ('Hi, I’m Markus')),
    (('¿Cómo te llamas?'), ('What is your name?')),
    (('(Yo) tengo … años'), ('I am … years old.')),
    (('(Yo) soy de…'), 	('I come from…')),
    (('Buenos días'), 	('Good morning')),
    (('Buenas tardes'), ('Good afternoon')),
    (('Buenas noches'), ('Good evening / Good night')),
    (('¿Cómo está usted?'),	('How are you? (formal)')),
    (('¿Cómo estás?'), ('How are you? (informal)')),
    (('¿Qué tal?'), ('How are you? (informal) / What’s up?')),
    (('¿Cómo te va?'), ('How’s it going?')),
    (('¿Qué haces?'), ('What are you doing?')),
    (('¿Qué pasa?'), 	('What’s happening?')),
    (('Bien, gracias'), ('Good, thank you')),
    (('Muy bien'), 	('Very well')),
    (('Así, así'), ('So, so')),
    (('Como siempre'), ('As always')),
    (('¿Y tú?'), 	('And you?')),
    (('¡Gracias!'), ('Thank you!')),
    (('¡Muchas gracias!'), ('Thank you very much!')),
    (('¡De nada!'), ('You’re welcome! / No problem!')),
    (('Por favor'), 	('Please')),
    (('¡Perdon!'), ('Excuse me!')),
    (('¡Disculpe!'),	('Excuse me!')),
    (('¡Lo siento!'), ('Sorry!')),
    (('¿Qué…?'), ('What?')),
    (('¿Quién…?'), ('Who?')),
    (('¿Cuándo…?'), ('When?')),
    (('¿Dónde…?'), 	('Where?')),
    (('¿Por qué…?'), 	('Why?')),
    (('¿Cuál?'), ('Which?')),
    (('¿Cómo…?'), ('How?')),
    (('¿Qué hora tienes?'), ('What time is it?')),
    (('¿De dónde viene?') 	, ('Where are you from?')),
    (('¿Dónde vives?'), ('Where do you live?')),
    (('¿Puede ayudarme?'), ('Can you help me?')),
    (('¿Podría ayudarle?'), ('Can I help you?')),
    (('¿Cuánto cuesta eso?'), ('How much does it cost?')),
    (('¿Entiende?'), ('Do you understand?')),
    (('¡Puede repetirlo!'), 	('Can you say that again?')),
    (('¿Qué significa [word]?'), 	('What does [word] mean?')),
    (('¿Puedes hablar más despacio?'), 	('Can you speak slowly?')),
    (('¿Dónde puedo encontrar un taxi?'), 	('Where can I find a taxi?')),
    (('¿Dónde está [hotel’s name] hotel?'),
     ('Where is [hotel’s name] hotel?')),
    (('Sí'), ('Yes')),
    (('No') 	, ('No')),
    (('Tal vez'), ('Maybe')),
    (('Claro'), 	('Of course')),
]

# # get a count of the word list
# count = len(words)

# random_word = None  # Random word position in the words list
# hinter = ''  # hint text
# hint_count = 0  # how many letters already shown for the hint


# def next():
#     """ Generate next word """
#     global hinter, hint_count

#     # Clear the screen
#     answer_label.config(text='')
#     my_entry.delete(0, END)

#     # Reset hint stuff
#     hint_label.config(text='')
#     hinter = ''
#     hint_count = 0

#     # Create random selection
#     global random_word
#     random_word = randint(0, count - 1)

#     # update label with Spanish Word
#     spanish_word.config(text=words[random_word][0])


# def answer():
#     """ Check the user answer """
#     if my_entry.get().capitalize().strip() == words[random_word][1]:
#         answer_label.config(
#             text=f'Correct! {words[random_word][0]} is {words[random_word][1]}')
#     else:
#         answer_label.config(
#             text=f'Incorrect! {words[random_word][0]} is not {my_entry.get()}')


# def hint():
#     """ Show hint """
#     global hint_count, hinter

#     # no more hint than the length of the selected word
#     if hint_count < len(words[random_word][1]):
#         hinter = hinter + words[random_word][1][hint_count]
#         hint_label.config(text=hinter)
#         hint_count += 1


# spanish_word = Label(root, text='', font=('Helvetica', 36))
# spanish_word.pack(pady=50)

# answer_label = Label(root, text='')
# answer_label.pack(pady=20)

# my_entry = Entry(root, font=('Helvetica', 18))
# my_entry.pack(pady=20)

# # Create buttons
# buttons_frame = Frame(root)
# buttons_frame.pack(pady=20)

# answer_button = Button(buttons_frame, text='Answer', command=answer)
# answer_button.grid(row=0, column=0, padx=20)

# next_button = Button(buttons_frame, text='Next', command=next)
# next_button.grid(row=0, column=1,)

# hint_button = Button(buttons_frame, text='Hint', command=hint)
# hint_button.grid(row=0, column=2, padx=20)

# # Hint label
# hint_label = Label(root, text='')
# hint_label.pack(pady=20)

# # Run next function when program starts
# next()

class LanguageCardGame():

    def __init__(self, master, words, *args, **kwargs):
        self.master = master
        self.words = words
        # get a count of the word list
        self.number_of_words = len(words)

        self.random_word = None  # Random word position in the words list
        self.hinter = ''  # hint text
        self.hint_count = 0  # how many letters already shown for the hint
        self.create_widgets()

    def create_widgets(self):

        self.spanish_word = Label(self.master, text='', font=('Helvetica', 36))
        self.spanish_word.pack(pady=50)

        self.answer_label = Label(self.master, text='')
        self.answer_label.pack(pady=20)

        self.my_entry = Entry(self.master, font=('Helvetica', 18))
        self.my_entry.pack(pady=20)

        # Create buttons
        buttons_frame = Frame(self.master)
        buttons_frame.pack(pady=20)

        answer_button = Button(
            buttons_frame, text='Answer', command=self.answer)
        answer_button.grid(row=0, column=0, padx=20)

        next_button = Button(buttons_frame, text='Next', command=self.next)
        next_button.grid(row=0, column=1,)

        hint_button = Button(buttons_frame, text='Hint', command=self.hint)
        hint_button.grid(row=0, column=2, padx=20)

        # Hint label
        self.hint_label = Label(self.master, text='')
        self.hint_label.pack(pady=20)

    def next(self):
        """ Generate next word """
        # Clear the screen
        self.answer_label.config(text='')
        self.my_entry.delete(0, END)

        # Reset hint stuff
        self.hint_label.config(text='')
        self.hinter = ''
        self.hint_count = 0

        # Create random selection
        self.random_word = randint(0, self.number_of_words - 1)

        # update label with Spanish Word
        self.spanish_word.config(text=self.words[self.random_word][0])

    def answer(self):
        """ Check the user answer """
        if self.my_entry.get().capitalize().strip() == self.words[self.random_word][1].strip():
            self.answer_label.config(
                text=f'Correct! {self.words[self.random_word][0]} is {self.words[self.random_word][1]}')
        else:
            self.answer_label.config(
                text=f'Incorrect! {self.words[self.random_word][0]} is not {self.my_entry.get()}')

    def hint(self):
        """ Show hint """

        # no more hint than the length of the selected word
        if self.hint_count < len(self.words[self.random_word][1]):
            self.hinter = self.hinter + \
                self.words[self.random_word][1][self.hint_count]
            self.hint_label.config(text=self.hinter)
            self.hint_count += 1


app = LanguageCardGame(root, words=words)

# Run next function when program starts
app.next()

root.mainloop()
