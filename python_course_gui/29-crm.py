from tkinter import *
from PIL import ImageTk, Image
import mysql.connector

root = Tk()
root.title("Matplots")
root.iconbitmap('codemy.ico')
root.geometry("400x200")

db_name = "crm_python"
table_name = "customers"
IS_DEBUG = False

def print_cursor(cursor, query):
    if not IS_DEBUG:
        return
    cursor.execute(query)
    for x in cursor:
        print(x)

def print_cursor_result(cursor):
    # if not IS_DEBUG:
    #     return
    for x in cursor:
        print(x)


# Connect to MySQL
mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="root",
    database=db_name 
)

# Check to see if connection to MySQL was created
# print(mydb)

# Create a cursor and initialize it
my_cursor = mydb.cursor()

# print(my_cursor)

# Create Database
my_cursor.execute(f"CREATE DATABASE IF NOT EXISTS {db_name};")

print_cursor(my_cursor, "SHOW DATABASES")

# Create a table
my_cursor.execute("""CREATE TABLE IF NOT EXISTS customers (
	user_id INT(11) PRIMARY KEY AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	zipcode INT(10),
	price_paid DECIMAL(10,2) NOT NULL
);
""")

# show database tables
print_cursor(my_cursor, "SHOW TABLES")

# show content of table 
my_cursor.execute("SELECT * FROM " + table_name)
print_cursor_result(my_cursor.description)



root.mainloop()