from tkinter import *
from random import randint

root = Tk()
root.title("Random Winner")
root.iconbitmap('codemy.ico')
root.geometry("400x400")

def pick():
    entries = ['Sanbid Roy Chowdhury', 'The refuge Malik', 'Google India', 'DotDotCom', 'Jon Layman', 'Itz Omen', 'Roberto Picco', 'Adigun olamide', 'Saurabh Parate', 'Exceptional Faruk', 'Avinash Upadhyaya', 'Fabio F. de Aquino', 'Ganesh Hegde', 'Hardy Don', 'Trap Town NCS', 'NIRMAL SRINIVAS CHINTALA', 'Weston', 'Gavin Hou', 'Bhanumathi A', 'Faoud Mohd ', 'rocket science', 'LucaVont', 'Kristian Simko', 'LunarAtom', 'Code With Wasif', 'Shreyas shetty', 'Panagiotis V', 'Dan Esquibel', 'TechiesSpammer69', 'John Dripper', 'Barbossa', 'Sahan Eakanayaka', 'Cassio Lacaz', 'Ranga bharath', 'Kisalay Suman', 'Le Dung', 'Mildo', 'Ivan Yosifov', 'Shreyas shetty', 'pranav Bhatki', 'Atharv Nuthi', 'Tibas Tiba', 'Yash Thakkar', 'Dario', 'deranged llama', 'Tom Blackwood', 'Christian Dimayacyac', 'Shaun A', 'Tchosk', 'Ahsan Arain', 'GRANDHI NAGESHWARAO', 'Baby Daily Life', 'PERFECT IGBADUMHE', 'Amad Ahmed', 'benage andy', 'Gerald Minoza', 'Samuel Hafer', 'Augusto Sousa', 'Andreas Mls', 'Somali flame', 'GPSSerbia', 'Hima Subedi', 'Ignatus Nana Amoah', 'Videos Promoter', 'Nabil Fantes', 'collins anele', 'Utku Yucel', 'Robert Coffie', 'M Dandan', 'Hudaibia Syed', 'abdurrahman zakariya', 'sabin katwal', 'Hardy Don', 'v sr', '10 bitangaje', 'Jon Bascos', 'Gaming Hub', 'Ace Hardy', 'Arsh Bains', 'Hima Subedi', 'Samuel Hafer', 'Fake Account', 'Spider', 'Callum Telford', 'Nikola Franicevic', 'David Blankinship', 'Unison', 'ramona Saintandre', 'Mohan hari krishna']

    # Remove duplicates
    entries_converted = set(entries)
    unique_entries = list(entries_converted)

    # number of participants
    count_entries = len(unique_entries) - 1

    # Generate a random number between 0 and the last element of the unique entries 
    random_number = randint(0, count_entries)

    # Win mesage
    winner_label = Label(root, text=unique_entries[random_number], font=("Helvetica", 18))
    winner_label.pack(pady=50)

    


top_label = Label(root, text="Win-O-Rama!", font=("Helvetica", 24), bg="red", fg="blue")
top_label.pack(pady=20)

win_button = Button(root, text="PICK THE WINNER", font=("Helvetica", 14), command=pick)
win_button.pack(pady=20)

root.mainloop()