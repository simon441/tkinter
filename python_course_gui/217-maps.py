from pathlib import Path
from tkinter import Label, Tk
from tkinter.ttk import LabelFrame
import tkintermapview

root = Tk()
root.title('Tkinter MapView')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('900x700')

my_label = LabelFrame(root)
my_label.pack(pady=20)

# Map
map_widget = tkintermapview.TkinterMapView(my_label, width=800, height=600, corner_radius=0)

# Set Coordinates
# map_widget.set_position(44.1015985, 3.0615855)

# Set Address
map_widget.set_address('notre dame, paris, france')

# Set Zoom Level
map_widget.set_zoom(20) # the higher the number the bigger the zoom

map_widget.pack()


root.mainloop()