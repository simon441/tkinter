from tkinter import Button, Entry, Label, Tk, messagebox
from pathlib import Path

from datetime import datetime

root = Tk()
root.title('Age Calculator')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('500x300')


def age():
    """Compute the age"""
    if my_entry.get():
        # Get the current year
        current_year = datetime.now().year
        # Calculate the age
        try:
            the_age = int(my_entry.get().strip())
            if the_age < 0:
                # Show error message
                messagebox.showerror("Error", "Age must be geater than zero")
                return
            your_age = current_year - the_age
            # Show age in message box
            messagebox.showinfo("Your Age", f"Your Age Is {your_age}")
        except ValueError:
            # Show error message
            messagebox.showerror("Error", "Your age must be a whole number!")
    else:
        # Show error message
        messagebox.showerror("Error", "You forgot to enter your age!")


my_label = Label(root, text="Enter Year Born", font=("Hevetica", 24))
my_label.pack(pady=20)

my_entry = Entry(root, font=("Helvetica", 18))
my_entry.pack(pady=20)

my_button = Button(root, text="Calculate Age!",
                   font=("Helvetica", 18), command=age)
my_button.pack(pady=20)

root.mainloop()
