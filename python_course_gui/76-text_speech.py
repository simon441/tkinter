from tkinter import *
import pyttsx3

root = Tk()
root.title("Text to Speech")
root.iconbitmap('codemy.ico')
root.geometry("800x500")

def speak():
    my_text = my_entry.get()
    engine.say(my_text)
    engine.runAndWait()
    my_entry.delete(0, END)

    """ RATE"""
    rate = engine.getProperty('rate')   # getting details of current speaking rate
    print (rate)                        #printing current voice rate
    engine.setProperty('rate', 125)     # setting up new voice rate


    """VOLUME"""
    volume = engine.getProperty('volume')   #getting to know current volume level (min=0 and max=1)
    print (volume)                          #printing current volume level
    engine.setProperty('volume',1.0)    # setting up volume level  between 0 and 1

    """VOICE"""
    voices = engine.getProperty('voices')       #getting details of current voice
    #engine.setProperty('voice', voices[0].id)  #changing index, changes voices. o for male
    engine.setProperty('voice', voices[1].id)   #changing index, changes voices. 1 for female

    engine.say("Hello World!")
    engine.say('My current speaking rate is ' + str(rate))
    engine.runAndWait()
    engine.stop()

# Initialize text to speech module
engine = pyttsx3.init()

# Assing text to render  to the text to speak engine
engine.say("le port du masque sera obligatoire dans les lieux publics clos dès lundi")

# Talk
engine.runAndWait()

my_title = Label(root, text="Enter text to be spoken", font=("Helvetica", 28))
my_title.pack(pady=30)

# Entry to input text to be spoken
my_entry = Entry(root, font=("Helvetica", 28))
my_entry.pack(pady=20)

my_button = Button(root, text="Speak", command=speak)
my_button.pack(pady=10)
root.mainloop()