from tkinter import *
import sqlite3

root = Tk()

root.title("Simon Address Book Manager")
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# Create a Database
conn = sqlite3.connect("address_book.db")

# Create cursor
c = conn.cursor()

# Create table
# c.execute(""" CREATE TABLE addresses (
#         first_name TEXT,
#         last_name TEXT,
#         address TEXT,
#         city TEXT,
#         state TEXT,
#         zipcode INTEGER
#     ) """)

def delete():
    """ Delete a record from the database """

    # Connect to the database
    conn = sqlite3.connect("address_book.db")

    # Create cursor
    c = conn.cursor()
    
    ref_id = id_ref_box.get()
    # Delete a record
    c.execute("DELETE FROM addresses WHERE oid=:oid", {"oid": ref_id})
     
    # Commit Changes
    conn.commit()

    #  Close Connection
    conn.close()    

    id_ref_box.delete(0, END)


def update():
    # Connect to the database
    conn = sqlite3.connect("address_book.db")
    
    # Create cursor
    c = conn.cursor()        

    record_id = id_ref_box.get()

    c.execute("""
        UPDATE addresses SET
         first_name=:f_name,
         last_name=:l_name,
         address=:address,
         city=:city,
         state=:state,
         zipcode=:zipcode
        WHERE oid=:oid
    """, {
        "f_name": f_name_editor.get(),
        "l_name": l_name_editor.get(),
        "address": address_editor.get(),
        "city": city_editor.get(),
        "state": state_editor.get(),
        "zipcode": zipcode_editor.get(),
        "oid": record_id
    })

    
    # Commit Changes
    conn.commit()

    # Close Connection
    conn.close()

    editor.destroy()

def edit():
    """ Edit a record from the database """

    # Connect to the database
    conn = sqlite3.connect("address_book.db")

    # Create cursor
    c = conn.cursor()

    # select record to edit
    c.execute('SELECT *, oid FROM addresses WHERE oid=:iod', {'iod': id_ref_box.get()})
    record = c.fetchone()
    # print(record)

    global editor
    editor = Tk()

    editor.title("Update A Record")
    editor.iconbitmap('codemy.ico')
    editor.geometry("400x300")
    
    # Create Global variables for Text Box names
    global f_name_editor
    global l_name_editor
    global address_editor
    global city_editor
    global state_editor
    global zipcode_editor

    # Create Text Boxes
    f_name_editor = Entry(editor, width=30)
    f_name_editor.grid(row=0, column=1, padx=20, pady=(10, 0))
    l_name_editor = Entry(editor, width=30)
    l_name_editor.grid(row=1, column=1, padx=20)
    address_editor = Entry(editor, width=30)
    address_editor.grid(row=2, column=1, padx=20)
    city_editor = Entry(editor, width=30)
    city_editor.grid(row=3, column=1, padx=20)
    state_editor = Entry(editor, width=30)
    state_editor.grid(row=4, column=1, padx=20)
    zipcode_editor = Entry(editor, width=30)
    zipcode_editor.grid(row=5, column=1, padx=20)

    # Create Text Box Labels
    f_name_label = Label(editor, text="First Name")
    f_name_label.grid(row=0, column=0, pady=(10, 0))
    l_name_label = Label(editor, text="Last Name")
    l_name_label.grid(row=1, column=0)
    address_label = Label(editor, text="Address")
    address_label.grid(row=2, column=0)
    city_label = Label(editor, text="City")
    city_label.grid(row=3, column=0)
    state_label = Label(editor, text="State")
    state_label.grid(row=4, column=0)
    zipcode_label = Label(editor, text="Zip Code")
    zipcode_label.grid(row=5, column=0)

    f_name_editor.insert(0, record[0])
    l_name_editor.insert(0, record[1])
    address_editor.insert(0, record[2])
    city_editor.insert(0, record[3])
    state_editor.insert(0, record[4])
    zipcode_editor.insert(0, record[5])

    # Button to save the edited record
    btn_save = Button(editor, text="Save Record", command=update)
    btn_save.grid(row=6, column=0, columnspan=2, pady=10, padx=10, ipadx=145)


def submit():
    """ Submit Function To insert records in the database """

    # Connect to the database
    conn = sqlite3.connect("address_book.db")

    # Create cursor
    c = conn.cursor()

    # Insert Into Table
    c.execute("INSERT INTO addresses (first_name, last_name, address, city, state, zipcode) \
                VALUES (:f_name, :l_name, :address, :city, :state, :zipcode)",
                {
                    "f_name": f_name.get(),
                    "l_name": l_name.get(),
                    "address": address.get(),
                    "city": city.get(),
                    "state": state.get(),
                    "zipcode": zipcode.get(),
                })
    
    # Commit Changes
    conn.commit()

    # Close Connection
    conn.close()

    # Clear the Text Boxes
    f_name.delete(0, END)
    l_name.delete(0, END)
    address.delete(0, END)
    city.delete(0, END)
    state.delete(0, END)
    zipcode.delete(0, END)


def show_all():
    """ Query All Function to Get all records from Database """

    # Connect to the database
    conn = sqlite3.connect("address_book.db")

    # Create cursor
    c = conn.cursor()

    c.execute('SELECT *, oid FROM addresses')
    records = c.fetchall()
    # print(records)

    print_records = ''

    # print_records += f"Full Name\tId\n"
    for record in records:
        print_records += f"{record[0]} {record[1]}\t{record[6]}\n"

    query_label = Label(root, text=print_records)
    query_label.grid(row=13, column=0, columnspan=2) 
    
    # Commit Changes
    conn.commit()

    #  Close Connection
    conn.close()


# Create Text Boxes
f_name = Entry(root, width=30)
f_name.grid(row=0, column=1, padx=20, pady=(10, 0))

l_name = Entry(root, width=30)
l_name.grid(row=1, column=1, padx=20)

address = Entry(root, width=30)
address.grid(row=2, column=1, padx=20)

city = Entry(root, width=30)
city.grid(row=3, column=1, padx=20)

state = Entry(root, width=30)
state.grid(row=4, column=1, padx=20)

zipcode = Entry(root, width=30)
zipcode.grid(row=5, column=1, padx=20)

id_ref_box = Entry(root, width=30)
id_ref_box.grid(row=9, column=1)

# Create Text Box Labels
f_name_label = Label(root, text="First Name")
f_name_label.grid(row=0, column=0, pady=(10, 0))

l_name_label = Label(root, text="Last Name")
l_name_label.grid(row=1, column=0)

address_label = Label(root, text="Address")
address_label.grid(row=2, column=0)

city_label = Label(root, text="City")
city_label.grid(row=3, column=0)

state_label = Label(root, text="State")
state_label.grid(row=4, column=0)

zipcode_label = Label(root, text="Zip Code")
zipcode_label.grid(row=5, column=0)

id_ref_box_label = Label(root, text="Select ID")
id_ref_box_label.grid(row=9, column=0, pady=5)


# Create Submit Button
submit_btn = Button(root, text="Add Record to Database", command=submit)
submit_btn.grid(row=6, column=0, columnspan=2, pady=10, padx=10, ipadx=100)

# Create a Query All Records Button
show_all_btn = Button(root, text="Show All", command=show_all)
show_all_btn.grid(row=7, column=0, columnspan=2, pady=10, padx=10, ipadx=137)

# Create a Delete Button
delete_btn = Button(root, text="Delete Record", command=delete)
delete_btn.grid(row=10, column=0, columnspan=2, pady=10, padx=10, ipadx=136)

# Create a Edit Button
edit_btn = Button(root, text="Edit Record", command=edit)
edit_btn.grid(row=11, column=0, columnspan=2, pady=10, padx=10, ipadx=145)

# Commit Changes
conn.commit()

# Close Connection
conn.close()


root.mainloop()
