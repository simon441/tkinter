from tkinter import *
from tkinter import messagebox

root = Tk()
root.title('Menu App')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# Panels

panel_1 = PanedWindow(root, bd=4, relief="raised", bg="red")
panel_1.pack(fill=BOTH, expand=1)

left_label = Label(panel_1, text="Left Panel")
left_label.pack()

panel_1.add(left_label)

# Second Panel
panel_2 = PanedWindow(panel_1, orient=VERTICAL, bd=4, relief="raised", bg="blue")
panel_1.add(panel_2)

top = Label(panel_2, text="Top Level")
panel_2.add(top)

bottom = Label(panel_2, text="Bottom Level")
panel_2.add(bottom)

# Launch GUI
root.mainloop()