from tkinter import *
from tkinter import ttk

root = Tk()
root.title("Notebook, tabs")
root.iconbitmap('codemy.ico')
root.geometry("500x500")

def hide():
    #  hide is like a list: start a index 0
    #  hides second tab
    my_notebook.hide(1)

def show():
    #  hide is like a list: start a index 0
    #  shows second tab
    my_notebook.add(my_frame2)

def select():
    #  hide is like a list: start a index 0
    #  shows second tab
    my_notebook.select(1)

my_notebook = ttk.Notebook(root)
my_notebook.pack()

my_frame1 = Frame(my_notebook, width=500, height=500, bg="blue")
my_frame2 = Frame(my_notebook, width=500, height=500, bg="red")
my_frame1.pack(fill="both", expand=1)
my_frame2.pack(fill=BOTH, expand=1)

# Add Frames to Notebook (each frame is a tab)
my_notebook.add(my_frame1, text="My blue tab")
my_notebook.add(my_frame2, text="A red tab")

# Hide a tab
my_button = Button(my_frame1, text="Hide tab 2", command=hide)
my_button.pack(pady=10)

# Show a tab
my_button2 = Button(my_frame1, text="Show tab 2", command=show)
my_button2.pack(pady=10)

# Navigate to a tab
my_button3 = Button(my_frame1, text="Navigate To tab 2", command=select).pack(pady=10)

root.mainloop()