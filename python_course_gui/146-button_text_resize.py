import tkinter as Tk

root = Tk.Tk()
root.title("Resize Button Text")
root.iconbitmap("codemy.ico")
root.geometry('500x500')

Tk.Grid.rowconfigure(root, 0, weight=1)
Tk.Grid.columnconfigure(root, index=0, weight=1)

button1 = Tk.Button(root, text="Button 1")
button1.grid(row=0, column=0, sticky=Tk.NSEW)


def resize(e):
    print(e)
    # Grab the app width and divide by 10
    size = e.width // 10
    # Change the button text size
    button1.config(font=("Helvetica", size))

    # Mess with height
    height_size = e.height // 10
    if e.height <= 300:
        button1.config(font=("Helvetica", height_size))

    '''
    if e.height <= 300 and e.height > 200:
         button1.config(font=("Helvetica", 30))
    elif e.height < 200 and e.height > 100:
        button1.config(font=("Helvetica", 20))
    elif e.height < 100:
        button1.config(font=("Helvetica", 20))
    '''


# Bind the app
root.bind('<Configure>', resize)

root.mainloop()
