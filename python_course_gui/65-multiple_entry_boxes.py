from tkinter import *

root = Tk()
root.title("Multiple entry boxes")
root.iconbitmap('codemy.ico')
root.geometry("700x500")

my_entries = []

def something():
    entry_list = ''
    for entries in my_entries:
        entry_list += str(entries.get()) + "\n"
    my_label.config(text=entry_list)

# Row loop
for i in range(5):
    # Column loop
    for j in range(5):
        my_entry = Entry(root)
        my_entry.grid(row=i, column=j, padx=5, pady=5)
        my_entries.append(my_entry)

# # Only Row loop
# for x in range(5):
#     my_entry = Entry(root)
#     my_entry.grid(row=0, column=x, padx=5, pady=5)
#     my_entries.append(my_entry)

my_button = Button(root, text="Click Me!", command=something)
my_button.grid(row=6, column=0, pady=20)

my_label = Label(root, text="")
my_label.grid(row=7, column=0, pady=20)

root.mainloop()