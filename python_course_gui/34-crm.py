from tkinter import *
from tkinter import ttk, messagebox
from PIL import ImageTk, Image
import mysql.connector
from mysql.connector import Error
import csv
import json
from decimal import Decimal

root = Tk()
root.title("Simon Cateau's Customer Database Manager")
root.iconbitmap('codemy.ico')
root.geometry("400x600")

db_name = "crm_python"
table_name = "customers"
IS_DEBUG = True
file_name_default_for_export = table_name

def print_cursor(cursor, query):
    if not IS_DEBUG:
        return
    cursor.execute(query)
    for x in cursor:
        print(x)

def print_cursor_result(cursor):
    # if not IS_DEBUG:
    #     return
    for x in cursor:
        print(x)


# Connect to MySQL
mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="root",
    database=db_name 
)

# Check to see if connection to MySQL was created
# print(mydb)

# Create a cursor and initialize it
my_cursor = mydb.cursor()

# print(my_cursor)

#################################### 
# Database and table initialization
###################################

# Create Database
my_cursor.execute(f"CREATE DATABASE IF NOT EXISTS {db_name};")

# print_cursor(my_cursor, "SHOW DATABASES")

# Create a table
my_cursor.execute("""CREATE TABLE IF NOT EXISTS customers (
	user_id INT(11) PRIMARY KEY AUTO_INCREMENT,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	zipcode INT(10),
	price_paid DECIMAL(10,2) NOT NULL,
	email VARCHAR(255),
	address_1 VARCHAR(255),
	address_2 VARCHAR(255),
	city VARCHAR(50),
	state VARCHAR(50),
	country VARCHAR(255),
	phone VARCHAR(255),
	payment_method VARCHAR(50),
	discount_code VARCHAR(255)
    );
""")

# show database tables
# print_cursor(my_cursor, "SHOW TABLES")

# show content of table 
# my_cursor.execute("SELECT * FROM " + table_name)
# print_cursor_result(my_cursor.description)

"""
FIELDS
	first_name,
	last_name,
	zipcode,
	price_paid,
	email,
	address_1,
	address_2,
	city,
	state,
	country,
	phone,
	payment_method,
	discount_code
"""

################
# Functions
################

# Tkinter helpers

def on_quit_app():
    if messagebox.askyesno('App Info', "Close App?"):
        print("Closing MySQL connections")
        # messagebox.info('App Info', "Closing Database Connections")
        my_cursor.close()
        mydb.close()
        root.destroy()

# Utility functions

def write_to_file(data, filename):
    pass

def export_to_csv(data, filename=None):
    """ Export a file to csv
        data: data to write to file
        filename: optional filename, defaults to previously defined name
     """

    if filename is None:
        filename = file_name_default_for_export + '.csv'
    
    with open(file=filename, mode="w", encoding="utf-8", newline='') as f:
        w = csv.writer(f, dialect="excel")
        w.writerows(data)
    f.close()


            
def export_to_json(data, field_names, filename=None):
    """ Export a file to JSON
        data: data to parse (values)
        field_names: result field names (key)
        filename: optional filename, defaults to previously defined name
     """

    if filename is None:
        filename = file_name_default_for_export + '.json'

    #  Make a dictionary from the data
    data_dict = []
    for row in data:
        my_row = {}
        for index, column in enumerate(row):
            print(column, index, field_names[index], type(column))
            if type(column) is Decimal:
                column = float(column)
            my_row[field_names[index]] = column
        data_dict.append(my_row)

    # print(data_dict)
    # print(json.dumps(data_dict))

    with open(file=filename, mode="w", encoding="utf-8") as f:
        f.write(json.dumps(data_dict, indent=4))
    f.close()


# GUI and Db Functions

def clear_fields():
    """ Clear the text from all Entry fields """
    first_name_box.delete(0, END)
    last_name_box.delete(0, END)
    address1_box.delete(0, END)
    address2_box.delete(0, END)
    city_box.delete(0, END)
    state_box.delete(0, END)
    zipcode_box.delete(0, END)
    country_box.delete(0, END)
    phone_box.delete(0, END)
    email_box.delete(0, END)
    payment_method_box.delete(0, END)
    discount_code_box.delete(0, END)
    price_paid_box.delete(0, END)


def add_customer():
    """ Submit Customer to Database """
    """ Insert a customer into the table """

    sql_command = """INSERT INTO customers 
    (first_name, last_name, address_1, address_2, city, state, zipcode, country, phone, email, payment_method, discount_code, price_paid)
     VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
     """
    values = (
        first_name_box.get(),
        last_name_box.get(),
        address1_box.get(),
        address2_box.get(),
        city_box.get(),
        state_box.get(),
        zipcode_box.get(),
        country_box.get(),
        phone_box.get(),
        email_box.get(),
        payment_method_box.get(),
        discount_code_box.get(),
        price_paid_box.get()
    )
    # print(values)

    try:
        my_cursor.execute(sql_command, values)

        # Commit the changes to the database
        mydb.commit()
    except Error as e:
        print(f"Error: {e}")
        mydb.rollback()

    # Clear the fields after insert
    clear_fields()

  
def list_customers():
    """ List customers """
    list_customers_query = Tk()
    list_customers_query.title("List All Costumers")
    list_customers_query.iconbitmap('codemy.ico')
    list_customers_query.geometry("1000x600")

    my_cursor.execute("SELECT * FROM customers")
    results = my_cursor.fetchall()
    for index, result in enumerate(results):
        num = 0
        # lookup_label = Label(list_customers_query, text="{0} {1} {2}".format(x[1], x[2], x[0]))
        for field in result:
            lookup_label = Label(list_customers_query, text=field)
            lookup_label.grid(row=index, column=num)
            num += 1
        # print(x)
    
    headers = list(my_cursor.column_names)
    data = []
    data.append(headers)
    data.extend(results)
    print("####", data)
    # Button to Export to CSV format
    csv_button = Button(list_customers_query, text="Save to CSV", command=lambda: export_to_csv(data))
    csv_button.grid(row=index + 1, column=0, padx=10, pady=10)


    # Button to Export to JSON format
    json_button = Button(list_customers_query, text="Save to JSON", command=lambda: export_to_json(results, headers))
    json_button.grid(row=index + 1, column=1, padx=10, pady=10)


def search_customers():
    """ Search All customers """

    # Dropdown search options
    search_options = [
        'Search by...',
        'First Name',
        'Last Name',
        'Email Address',
        'Customer ID',
        'City',
    ]
    search_options_dict = {
        '_NOVALUE_': 'Search by...',
        'first_name': 'First Name',
        'last_name': 'Last Name',
        'email': 'Email Address',
        'id': 'Customer ID',
        'city': 'City',
    }

    # functions
    def search_now():
        """ Search Function """
        searched = search_box.get().strip()

        selected = search_options_box.get()
        # if selected == search_options[1]:
        #     search_query = "first_name LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "last_name LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "email LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "user_id LIKE %s"
        # if selected == search_options[1]:
        #     search_query = "city LIKE %s"
        value_search = "%{}%"
        if selected == search_options_dict['_NOVALUE_']:
            text = Label(search_customers, text="Hey! You forget to select a search option")
            text.grid(row=3, column=0, columnspan=3)
            return
        elif selected == search_options_dict['first_name']:
            search_query = "first_name LIKE %s"
        elif selected == search_options_dict["last_name"]:
            search_query = "last_name LIKE %s"
        elif selected == search_options_dict["email"]:
            search_query = "email LIKE %s"
        elif selected == search_options_dict["id"]:
            search_query = "user_id = %s"
            value_search = "{}"
        elif selected == search_options_dict["city"]:
            search_query = "city LIKE %s"
        else:
            Label(search_customers, text="An error occurd in the selection").grid(row=3, column=0, columnspan=3)
            return

        sql = f"SELECT * FROM customers WHERE {search_query}"
        # print(sql)
        name = (value_search.format(searched), )
        my_cursor.execute(sql, name)

        # print(my_cursor.rowcount)
        # for x in my_cursor:
        #     print(x)
        results = my_cursor.fetchall()
        if my_cursor.rowcount == 0:
            results = "No record found..."
            lookup_label = Label(search_customers, text=results)
            lookup_label.grid(row=3, column=0, columnspan=3)
        else:            
            for index, result in enumerate(results, 3):
                num = 0
                # lookup_label = Label(search_customers, text="{0} {1} {2}".format(x[1], x[2], x[0]))
                for field in result:
                    lookup_label = Label(search_customers, text=field)
                    lookup_label.grid(row=index, column=num)
                    num += 1
                # print(x)
            # tv = ttk.Treeview(search_customers, columns=my_cursor.column_names, show='headings' )
            # for col in my_cursor.column_names:
            #     print(col, type(col))
            #     tv.heading(col, text=col)
            # for index, result in enumerate(results, 3):
            #     # num = 0
            #     # # lookup_label = Label(search_customers, text="{0} {1} {2}".format(x[1], x[2], x[0]))
            #     # for field in result:
            #     #     lookup_label = Label(search_customers, text=field)
            #     #     lookup_label.grid(row=index, column=num)
            #     #     num += 1
            #     tv.insert(search_customers, index, result)
            #     # print(x)
            # #     # tv.heading(str(col), text=str(col))
            # # # tv.column("#0", anchor="w")    
            # # # tv.heading("#0", text='RollNo', anchor='w')
            # # # tv.column("#0", anchor="w")
            # # # tv.heading('Name', text='Name')
            # # # tv.column('Name', anchor='center', width=100)
            # # # tv.heading('Mobile', text='Mobile')
            # # # tv.column('Mobile', anchor='center', width=100)
            # # # tv.heading('course', text='course')
            # # # tv.column('course', anchor='center', width=100)
            # tv.grid(row=4, column=0, columnspan=3, sticky = (N,S,W,E))
            # # search_customers.treeview = tv
            # # search_customers.grid(3, weight = 1)
            # # search_customers.grid_columnconfigure(0, weight = 1)

    search_customers = Tk()
    search_customers.title("Search All Costumers")
    search_customers.iconbitmap('codemy.ico')
    search_customers.geometry("1000x600")

    # Entry box to search for a customer
    search_box = Entry(search_customers)
    search_box.grid(row=0, column=1, padx=10, pady=10)

    # Entry box label
    search_box_label = Label(search_customers, text="Search Customer by Last Name")
    search_box_label.grid(row=0, column=0, padx=10, pady=10)

    # Entry box search button
    search_box_button = Button(search_customers, text="Search Customers", command=search_now)
    search_box_button.grid(row=2, column=0, padx=10)

    # Dropdown box to select fields
    search_options_box = ttk.Combobox(search_customers, values=list(search_options_dict.values()), state="readonly")
    search_options_box.current(0)
    search_options_box.grid(row=0, column=2, padx=10)



   

#################
# BEGIN GUI
################# 

# Create Labels
title_label = Label(root, text="Cateau Customer Database Manager", font=("Helvetica", 16))
title_label.grid(row=0, column=0, columnspan=2, pady="10")

# Main Form to Enter Customer
first_name_label = Label(root, text="First Name").grid(row=1, column=0, sticky=W, padx=10)
last_name_label = Label(root, text="Last Name").grid(row=2, column=0, sticky=W, padx=10)
address1_label = Label(root, text="Address 1").grid(row=3, column=0, sticky=W, padx=10)
address2_label = Label(root, text="Address 2").grid(row=4, column=0, sticky=W, padx=10)
city_label = Label(root, text="City").grid(row=5, column=0, sticky=W, padx=10)
state_label = Label(root, text="State").grid(row=6, column=0, sticky=W, padx=10)
zipcode_label = Label(root, text="Zipcode").grid(row=7, column=0, sticky=W, padx=10)
country_label = Label(root, text="Country").grid(row=8, column=0, sticky=W, padx=10)
phone_label = Label(root, text="Phone Number").grid(row=9, column=0, sticky=W, padx=10)
email_label = Label(root, text="Email Address").grid(row=10, column=0, sticky=W, padx=10)
payment_method_label = Label(root, text="Payment Method").grid(row=11, column=0, sticky=W, padx=10)
discount_code_label = Label(root, text="Discount Code").grid(row=12, column=0, sticky=W, padx=10)
price_paid_label = Label(root, text="Price Paid").grid(row=13, column=0, sticky=W, padx=10)

# Entry Boxes
first_name_box = Entry(root)
first_name_box.grid(row=1, column=1)

last_name_box = Entry(root)
last_name_box.grid(row=2, column=1, pady=5)

address1_box = Entry(root)
address1_box.grid(row=3, column=1, pady=5)

address2_box = Entry(root)
address2_box.grid(row=4, column=1, pady=5)

city_box = Entry(root)
city_box.grid(row=5, column=1, pady=5)

state_box = Entry(root)
state_box.grid(row=6, column=1, pady=5)

zipcode_box = Entry(root)
zipcode_box.grid(row=7, column=1, pady=5)

country_box = Entry(root)
country_box.grid(row=8, column=1, pady=5)

phone_box = Entry(root)
phone_box.grid(row=9, column=1, pady=5)

email_box = Entry(root)
email_box.grid(row=10, column=1, pady=5)

payment_method_box = Entry(root)
payment_method_box.grid(row=11, column=1, pady=5)

discount_code_box = Entry(root)
discount_code_box.grid(row=12, column=1, pady=5)

price_paid_box = Entry(root)
price_paid_box.grid(row=13, column=1, pady=5)

# Buttons
add_customer_button = Button(root, text="Add Customer to Database", command=add_customer)
add_customer_button.grid(row=14, column=0, padx=10, pady=10, sticky=W)
clear_fields_button = Button(root, text="Clear Fields", command=clear_fields)
clear_fields_button.grid(row=14, column=1)

# list customers button
list_customers_button = Button(root, text="List Customers", command=list_customers)
list_customers_button.grid(row=15, column=0, sticky=W, padx=10)

# Search customers button
search_customers_button = Button(root, text="Search Customers", command=search_customers)
search_customers_button.grid(row=15, column=1, sticky=W, padx=10)


#################################
# Start GUI and Launch Event Loop
#################################

root.protocol("WM_DELETE_WINDOW", on_quit_app)

root.mainloop()