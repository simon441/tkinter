from tkinter import messagebox, Button, Toplevel, PhotoImage, Label, Frame
from pathlib import Path
import tkinter as Tk

root = Tk.Tk()
root.title("Custom Message Booxes")
root.iconbitmap("codemy.ico")
root.geometry("300x300")


def choice(option):
    """ Action depending on the user choice """
    pop.destroy()
    if option == 'yes':
        my_label.config(text='You clicked Yes!')
    else:
        my_label.config(text='You clicked No!')


def clicker():
    """ Click of the button action """

    global pop

    pop = Toplevel(root)
    pop.title('My Popup')
    pop.geometry('250x150')
    pop.config(bg='green')

    global me
    me = PhotoImage(
        file=str(Path().cwd().joinpath('images', 'me_head_gr.png')))

    pop_label = Label(pop, text='Would you like to proceed?',
                      bg='green', fg='white', font=('Helvetica', 12))
    pop_label.pack(pady=10)

    my_frame = Frame(pop, bg='green')
    my_frame.pack(pady=5)

    me_pic = Label(my_frame, image=me, borderwidth=0)
    me_pic.grid(row=0, column=0, padx=10)

    yes = Button(my_frame, text='YES',
                 command=lambda: choice('yes'), bg='orange')
    yes.grid(row=0, column=1, padx=10)

    no = Button(my_frame, text='NO',
                command=lambda: choice('no'), bg='yellow')
    no.grid(row=0, column=2, padx=10)


my_button = Button(root, text='Click Me', command=clicker)
my_button.pack(pady=50)

my_label = Label(root, text='')
my_label.pack(pady=20)

# start
root.mainloop()
