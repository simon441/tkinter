import tkinter as Tk
from tkinter import PhotoImage, filedialog, ttk
from pathlib import Path
from random import randint
root = Tk.Tk()
root.title("Stretch Buttons to Fit App")
root.iconbitmap("codemy.ico")
root.geometry('500x500')


# Configure the rows and columns
# Configure row 1
# Tk.Grid.rowconfigure(root, 0, weight=1)
Tk.Grid.columnconfigure(root, index=0, weight=1)

# Configure row 2
# Tk.Grid.rowconfigure(root, index=1, weight=1)


def something():
    pass


# Create 2 buttons
button1 = Tk.Button(root, text="Button 1", command=something)
button2 = Tk.Button(root, text="Button 2", command=something)
button3 = Tk.Button(root, text="Button 3", command=something)
button4 = Tk.Button(root, text="Button 4", command=something)


# Grid them to the screen
index = 0
button1.grid(row=index, column=0, sticky=Tk.NSEW)
index += 1
button2.grid(row=index, column=0, sticky=Tk.NSEW)
index += 1
button3.grid(row=index, column=0, sticky=Tk.NSEW)
index += 1
button4.grid(row=index, column=0, sticky=Tk.NSEW)
index += 1


# Create list of buttons
button_list = [button1, button2, button3, button4]

# Define current row number
row_number = 0
weight_number = 1
# loop through the list and configure each row automatically
for button in button_list:
    Tk.Grid.rowconfigure(root, index=row_number, weight=weight_number)
    # Increment the counter
    row_number += 1
    weight_number += 1


root.mainloop()
