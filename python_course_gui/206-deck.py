import random
from pathlib import Path
from tkinter import Button, Frame, Label, LabelFrame, Tk
from typing import List

from PIL import Image, ImageTk

root = Tk()
root.title('Card Deck')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('900x500')
root.configure(background='green')

# Global variables
# The Deck of cards
Deck: List[str] = []
# Dealer
Dealer: List[str] = []
# Player
Player: List[str] = []


def resize_card(card: str) -> ImageTk.PhotoImage:
    """Resize the Cards with the PIL library
    """
    global card_image_tk
    # Get the full image path
    image_path = Path.joinpath(
        Path(__file__).parent.resolve(), 'images', 'card_deck', card)

    # Open the Image
    card_image = Image.open(image_path)

    # Resize the Image
    card_image_resized = card_image.resize((150, 218))

    # Output the card
    card_image_tk = ImageTk.PhotoImage(card_image_resized)

    # Return the resized card
    return card_image_tk


def shuffle():
    """Shuffle the cards
    """
    # Define the Deck
    # The four suits of cards
    suits = ['diamonds', 'clubs', 'hearts', 'spades']
    # 2 is the first card in the Deck, Ace is the 14th (range does not include the end number e.g. 15)
    # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
    values = range(2, 15)

    # Remove all cards from the deck
    Deck.clear()

    # fill the Deck
    for suit in suits:
        for value in values:
            Deck.append(f'{value}_of_{suit}')

    # Create the players
    Dealer.clear()
    Player.clear()

    # Grab a random Card for the Dealer
    card = random.choice(Deck)
    # Remove card from Deck
    Deck.remove(card)
    # Append card to Dealer's list
    Dealer.append(card)
    # Output card to screen
    global dealer_image
    dealer_image = resize_card(f'{card}.png')

    dealer_label.config(image=dealer_image)

    # Grab a random Card for the Player
    card = random.choice(Deck)
    # Remove card from Deck
    Deck.remove(card)
    # Append card to Player's list
    Player.append(card)
    # Output card to screen
    global player_image
    player_image = resize_card(f'{card}.png')

    player_label.config(image=player_image)

    # Put number of remaining cards in the app's title bar
    root.title(f'{len(Deck)} cards left')


def deal_cards():
    """Deal Out the Cards
    """
    try:
        # Get a Card for the Dealer
        card = random.choice(Deck)
        # Remove card from Deck
        Deck.remove(card)
        # Append card to Dealer's list
        Dealer.append(card)
        # Output card to screen
        global dealer_image
        dealer_image = resize_card(f'{card}.png')

        dealer_label.config(image=dealer_image)
        dealer_label.config(text=card)

        # Get a Card for the Player
        card = random.choice(Deck)
        # Remove card from Deck
        Deck.remove(card)
        # Append card to Player's list
        Player.append(card)

        # Output card to screen
        global player_image
        player_image = resize_card(f'{card}.png')

        player_label.config(image=player_image)

        # Put number of remaining cards in the app's title bar
        root.title(f'{len(Deck)} cards left')
    except IndexError as e:
        root.title(f'No Cards remaining in Deck')


# Main Frame
main_frame = Frame(root, bg='green')
main_frame.pack(pady=20)

# Dealer Frame
dealer_frame = LabelFrame(main_frame, text='Dealer', bd=0)
dealer_frame.grid(row=0, column=0, padx=20, ipadx=20)

# Player Frame
player_frame = LabelFrame(main_frame, text='Player', bd=0)
player_frame.grid(row=0, column=1, ipadx=20)

# Put cards in Frames
dealer_label = Label(dealer_frame, text='')
dealer_label.pack(pady=20)

player_label = Label(player_frame, text='')
player_label.pack(pady=20)


# Button to Shuffle the Deck
shuffle_button = Button(root, text='Shuffle Deck',
                        font=('Helvetica', 14), command=shuffle)
shuffle_button.pack(pady=20)

# Button to Deal the Cards to the Players
deal_button = Button(root, text='Get Cards', font=(
    'Helvetica', 14), command=deal_cards)
deal_button.pack(pady=20)

# Shuffle cards on app load
shuffle()

root.mainloop()
