from tkinter import *
from tkinter import ttk

root = Tk()
root.title("Controls")
root.iconbitmap("codemy.ico")
root.geometry('320x650')

second = None


def launch():
    """
    Create a second window
    """
    global second
    second = Toplevel()
    second.geometry("100x100")
    width_slider.config(state=NORMAL)
    height_slider.config(state=NORMAL)
    both_slider.config(state=NORMAL)


# Create a launch button
launch_button = Button(root, text="Launch Window", command=launch)
launch_button.pack(pady=20)


def width_slide(pos):
    """ 
    Change the width
    """
    second.geometry(f"{int(width_slider.get())}x{int(height_slider.get())}")


def height_slide(pos):
    """ 
    Change the height
    """
    second.geometry(f"{int(width_slider.get())}x{int(height_slider.get())}")


def both_slide(pos):
    """ 
    Change both the width and the height at the same time
    """
    second.geometry(f"{int(both_slider.get())}x{int(both_slider.get())}")


# Create some label frames
width_frame = LabelFrame(root, text="Width")
width_frame.pack(pady=20)

height_frame = LabelFrame(root, text="Height")
height_frame.pack(pady=20)

both_frame = LabelFrame(root, text="Both")
both_frame.pack(pady=20)

# Create Some Sliders
width_slider = ttk.Scale(width_frame, from_=100, to=500,
                         orien=HORIZONTAL, length=200, command=width_slide, value=100, state=DISABLED)
width_slider.pack(pady=20, padx=20)
height_slider = ttk.Scale(width_frame, from_=100,
                          to=500, orien=VERTICAL, length=200, command=height_slide, value=100, state=DISABLED)
height_slider.pack(pady=20, padx=20)
both_slider = ttk.Scale(width_frame, from_=100,
                        to=500, orien=HORIZONTAL, length=200, command=both_slide, value=100, state=DISABLED)
both_slider.pack(pady=20, padx=20)


root.mainloop()
