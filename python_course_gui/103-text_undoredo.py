from tkinter import *
from tkinter import filedialog, font
import os
from pathlib import Path

root = Tk()
root.title('Text box')
root.iconbitmap("codemy.ico")
root.geometry("500x600")

##################
# Functions
##################

def open_file():
#    text_file = open("data/sample.txt", 'r')
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Open Text File", filetypes=(("Text Files", "*.txt"),))
    file_name = os.path.split(file_path)[-1]
    print(Path(file_path).suffix)
    print(file_name[:-len(Path(file_path).suffix)])
    text_file = open(file_path, 'r')
    temp_text = text_file.read()

    my_textbox.insert(END, temp_text)
    text_file.close()

    root.title(f'{file_name} - TextPad')

# Read only r  
# Read and Write r+  (beginning of file)
# Write Only w   (over-written)
# Write and Read w+  (over written)
# Append Only a  (end of file)
# Append and Read a+  (end of file)

def save_file():
    # text_file = open('data/sample.txt', 'w')
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Save Text File", filetypes=(("Text Files", "*.txt"),))
    text_file = open(file_path, 'w')
    text_file.write(my_textbox.get(1.0, END))
    # text_file.close()

def add_image():
    # Add Image
    global my_image
    my_image = PhotoImage(file='images/login.png')
    position = my_textbox.index(INSERT)
    my_textbox.image_create(position, image=my_image)

    my_label.config(text=position)


def select():
    """ Select Text """
    selected = my_textbox.selection_get()
    my_label.config(text=selected)

def bold_text():
    """ Bold selected text with select() """
    bold_font = font.Font(my_textbox, my_textbox.cget("font"))
    bold_font.configure(weight="bold")

    my_textbox.tag_configure("bold", font=bold_font)

    # current_tags = my_textbox.tag_names(SEL_FIRST)
    current_tags = my_textbox.tag_names('sel.first')

    if "bold" in current_tags:
        # remove bold
        # my_textbox.tag_remove("bold", SEL_FIRST, SEL_LAST)
        my_textbox.tag_remove("bold", 'sel.first', 'sel.last')
    else:
        # add bold
        # my_textbox.tag_add("bold", SEL_FIRST, SEL_LAST)
        my_textbox.tag_add("bold", 'sel.first', 'sel.last')

def italic_text():
    """ Make selected text italic from selected text in select() """
    italics_font = font.Font(my_textbox, my_textbox.cget("font"))
    italics_font.configure(slant="italic")

    my_textbox.tag_configure("italic", font=italics_font)

    # current_tags = my_textbox.tag_names(SEL_FIRST)
    current_tags = my_textbox.tag_names('sel.first')

    if "italic" in current_tags:
        # remove italic
        # my_textbox.tag_remove("italic", SEL_FIRST, SEL_LAST)
        my_textbox.tag_remove("italic", 'sel.first', 'sel.last')
    else:
        # add italic
        # my_textbox.tag_add("italic", SEL_FIRST, SEL_LAST)
        my_textbox.tag_add("italic", 'sel.first', 'sel.last')



####################
# GUI
####################

my_frame = Frame(root)
my_frame.pack(pady=10)


# scrollbar
my_scollbar = Scrollbar(my_frame)
my_scollbar.pack(side=RIGHT, fill=Y)


my_textbox = Text(my_frame, width=40, height=10, font=('Helvetica', 16), selectbackground="yellow", selectforeground="black", yscrollcommand=my_scollbar.set, undo=True)
my_textbox.pack(pady=20)

# Configure the scrollbar
my_scollbar.config(command=my_textbox.yview)

##############
#  buttons
##############

##############
#  File manipulation

# Open file
open_button = Button(root, text="Open Text File", command=open_file)
open_button.pack(pady=20)

# Save the file
save_button = Button(root, text="Save Text File", command=save_file)
save_button.pack(pady=20)

##############
# Widget misc buttons

# Add an Image
image_button = Button(root, text="Add Image", command=add_image)
image_button.pack(pady=5)

# get selected text
select_button = Button(root, text="Select Text", command=select)
select_button.pack(pady=5)

############## 
# Font commands

# Bold selected
bold_button = Button(root, text="Bold", command=bold_text)
bold_button.pack(pady=5)

# Italic selected
italic_button = Button(root, text="Italics", command=italic_text)
italic_button.pack(pady=5)

###############
# Undo/Redo 

# Undo button
undo_button = Button(root, text="Undo", command=my_textbox.edit_undo)
undo_button.pack(pady=5)

# Redo button
redo_button = Button(root, text="Redo", command=my_textbox.edit_redo)
redo_button.pack(pady=5)


# Show results of button actions
my_label = Label(root, text="")
my_label.pack()

root.mainloop()