from tkinter import *
from PIL import ImageTk, Image
import mysql.connector

root = Tk()
root.title("Matplots")
root.iconbitmap('codemy.ico')
root.geometry("400x200")

# Connect to MySQL
mydb = mysql.connector.connect(
    host= "localhost",
    user= "root",
    password= "",
)

# Check to see if connection to MySQL was created
print(mydb)

c = mydb.cursor()

print(c)
root.mainloop()