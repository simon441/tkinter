from tkinter import *

root = Tk()
root.title("Canvas")
root.iconbitmap('codemy.ico')
root.geometry("500x500")

# Create a Canvas
my_canvas = Canvas(root, width=300, height=200, bg="#fcfcfc")
my_canvas.pack(pady=20)

#############################
# 
# Create a rectangle
# my_canvas.create_rectangle(x1, y1, x2, y2, fill="pink")
# (x1, y1): Top left coordinates
# (x2, y2): Bottom right coordinates
my_canvas.create_rectangle(50, 150, 250, 50, fill="pink")


#############################
#  
# Oval
# my_canvas.create_oval(50, 150, 250, 250, fill="color")
# (x1, y1): Top left coordinates
# (x2, y2): Bottom right coordinates

# the two below are equivalent
# my_canvas.create_oval(50, 150, 250, 50, fill="cyan")
# or
my_canvas.create_oval(50, 50, 250, 150, fill="cyan")


#############################
# 
# Create a Line inside the canvas
# my_canvas.create_line(x1, y1, x2, y2, fill="color")
# (x1, y1) => coordinates start position
# (x2, y2) => coordinates ending position
# fill => (optional) if no option specified -> the line is a filled line coloed black
my_canvas.create_line(0, 100, 300, 100, fill="red")
my_canvas.create_line(150, 0, 150, 200, fill="red")

root.mainloop()