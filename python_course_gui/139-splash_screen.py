import tkinter as Tk

splash_root = Tk.Tk()
splash_root.title("Splash Screen!!")
splash_root.geometry("300x200+20+60")
# Hide titlebar
splash_root.overrideredirect(True)

splash_label = Tk.Label(
    splash_root, text="Splash Screen!", font=("Helvetica", 18))
splash_label.pack(pady=20)


def main_window():
    """
    create the main window
    """
    # destroy the splash screen
    splash_root.destroy()

    # create the main window
    root = Tk.Tk()
    root.title("Splash screens")
    root.iconbitmap("codemy.ico")
    root.geometry('500x550')

    main_label = Tk.Label(root, text="Main Screen", font=("Helvetica", 18))
    main_label.pack(pady=20)


# splash screeen timer
splash_root.after(3000, main_window)

Tk.mainloop()
