from tkinter import *
from tkinter import messagebox

root = Tk()
root.title('Menu with Frames')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# Initialize menu
my_menu = Menu(root)
root.config(menu=my_menu)


def our_command():
    """ command for menu action """
    my_label = Label(root, text="You click a Dropdown menu!")
    my_label.pack()


def file_new():
    """ File New Function """
    hide_all_frames()
    file_new_frame.pack(fill="both", expand=1)

    my_label = Label(file_new_frame, text="You click the File >> New Menu!")
    my_label.pack()


def edit_cut():
    """ Edit Cut Function """
    hide_all_frames()
    edit_cut_frame.pack(fill="both", expand=1)
    my_label = Label(edit_cut_frame, text="You click the Edit >> Cut Menu!")
    my_label.pack()

    dummy_button = Button(edit_cut_frame, text="fake!").pack(pady=10)

    child_label = Label(edit_cut_frame, text=edit_cut_frame.winfo_children())
    child_label.pack()

    # print(edit_cut_frame.winfo_children())
    # [<tkinter.Label object .!frame2.!label3>, <tkinter.Button object .!frame2.!button2>, <tkinter.Label object .!frame2.!label4>]

def about_command():
    """ About App command """
    messagebox.showinfo('About App', 'App created by Simon Cateau\n\nTemplate to show menu creation in tKinter in python\n@copyright 2020')


def hide_all_frames():
    """ Hide all frames """

    # Loop through all the children in each frame and delete them
    for widget in file_new_frame.winfo_children():
        widget.destroy()

    for widget in edit_cut_frame.winfo_children():
        widget.destroy()

    # hide frames
    file_new_frame.pack_forget()
    edit_cut_frame.pack_forget()

##########
# MENU
##########

# Create a File menu item
file_menu = Menu(my_menu)
my_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New...", command=file_new)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)


# Create an Edit menu item
edit_menu = Menu(my_menu)
my_menu.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Undo", command=our_command)
edit_menu.add_command(label="Redo", command=our_command)
edit_menu.add_separator()
edit_menu.add_command(label="Cut", command=edit_cut)
edit_menu.add_command(label="Copy", command=our_command)
edit_menu.add_command(label="Paste", command=our_command)


# Create an Options menu item
options_menu = Menu(my_menu)
my_menu.add_cascade(label="Options", menu=options_menu)
options_menu.add_command(label="Find", command=our_command)
options_menu.add_command(label="Find Next", command=our_command)


# Create a Help menu item
help_menu = Menu(my_menu)
my_menu.add_cascade(label="?", menu=help_menu)
help_menu.add_command(label="Help", command=our_command)
help_menu.add_command(label="About", command=about_command)

#########
# FRAMES
#########

# Create some Frames
file_new_frame = Frame(root, width=400, height=400, bg="red")
edit_cut_frame = Frame(root, width=400, height=400, bg="blue")


# Launch GUI
root.mainloop()