from tkinter import *

root = Tk()
root.title("Simple Button Amination")
root.iconbitmap("codemy.ico")
root.geometry('700x700')

# define some variables
count = 0
size = 26
pos = 100


def contract():
    global count, size, pos
    if count <= 10 and count > 0:
        size -= 2
        # Configure the button font size
        my_button.config(font=('Helvetica', size))
        # Change button position
        my_button.pack_configure(pady=pos)
        # Increase the count by 1
        count -= 1
        pos -= 20
        # Set the timer
        root.after(100, contract)


def expand():
    global count, size, pos
    if count < 10:
        size += 2
        # Configure the button font size
        my_button.config(font=('Helvetica', size))
        # Change button position
        my_button.pack_configure(pady=pos)
        # Increase the count by 1
        count += 1
        pos += 20
        # Set the timer
        root.after(100, expand)
    elif count == 10:
        contract()


# Create a Button
my_button = Button(root, text='Click Me!', command=expand,
                   font=('Helvetica', 24), fg='red')
my_button.pack(pady=pos)

root.mainloop()
