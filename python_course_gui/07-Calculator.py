from tkinter import *

root = Tk()
root.title("Simple Calculator")

f_num = s_num = 0

e = Entry(root, width=35, borderwidth=5)
e.grid(row=0, column=0, columnspan=3, padx=10, pady=10)

# variable to convert into int or float 
# depending on the type of the value returned 
# from the computed operation
is_int = True

def get_formatted_number(number):
    """ format a number """
    
    global is_int
    if type(number) is str:
        if is_int:
            return int(number)
        return float(number)
    return number

def button_click(number):
    """ get the button clicked by its number (0 to 9) """
    # e.delete(0, END)
    current = e.get()
    e.delete(0, END)
    e.insert(0, str(current) + str(number))

def button_clear():
    """ clear number field """
    e.delete(0, END)

def button_add():
    """ addition """
    first_number = e.get()

    global f_num
    global math
    math = 'addition'

    f_num = get_formatted_number(first_number)
    e.delete(0, END)

def button_substract():
    """ substraction """
    first_number = e.get()

    global f_num
    global math

    math = 'substraction'

    f_num = get_formatted_number(first_number)
    e.delete(0, END)

def button_multiply():
    """ multiplication """
    first_number = e.get()

    global f_num
    global math

    math = 'multiplication'

    f_num = get_formatted_number(first_number)
    e.delete(0, END)

def button_divide():
    """ division """
    first_number = e.get()

    global f_num
    global math
    
    math = 'division'

    f_num = get_formatted_number(first_number)
    e.delete(0, END)

def button_equal():
    """ compute operations """

    second_number = e.get()

    e.delete(0, END)
    
    if math == "addition":
        first_number = f_num + get_formatted_number(second_number)
    elif math == "substraction":
        first_number = f_num - get_formatted_number(second_number)
    elif math == "multiplication":
        first_number = f_num * get_formatted_number(second_number)
    elif math == "division":
        first_number = f_num / get_formatted_number(second_number)

    e.insert(0,first_number)

    first_number = e.get()

    global is_int

    if type(first_number) is int:
        is_int = True
    else:
        is_int = False

    # print(type(first_number), is_int)

# Define buttons

button_1 = Button(root, text="1", padx=40, pady=20, command=lambda: button_click(1))
button_2 = Button(root, text="2", padx=40, pady=20, command=lambda: button_click(2))
button_3 = Button(root, text="3", padx=40, pady=20, command=lambda: button_click(3))
button_4 = Button(root, text="4", padx=40, pady=20, command=lambda: button_click(4))
button_5 = Button(root, text="5", padx=40, pady=20, command=lambda: button_click(5))
button_6 = Button(root, text="6", padx=40, pady=20, command=lambda: button_click(6))
button_7 = Button(root, text="7", padx=40, pady=20, command=lambda: button_click(7))
button_8 = Button(root, text="8", padx=40, pady=20, command=lambda: button_click(8))
button_9 = Button(root, text="9", padx=40, pady=20, command=lambda: button_click(9))
button_0 = Button(root, text="0", padx=40, pady=20, command=lambda: button_click(0))

button_add = Button(root, text="+", padx=39, pady=20, command=button_add)
button_equal = Button(root, text="=", padx=91, pady=20, command=button_equal)
button_clear = Button(root, text="Clear", padx=79, pady=20, command=button_clear)

button_substract = Button(root, text="-", padx=41, pady=20, command=button_substract)
button_multiply = Button(root, text="x", padx=40, pady=20, command=button_multiply)
button_divide = Button(root, text="/", padx=41, pady=20, command=button_divide)

# Put buttons  on the screen

button_1.grid(row=3, column=0)
button_2.grid(row=3, column=1)
button_3.grid(row=3, column=2)

button_4.grid(row=2, column=0)
button_5.grid(row=2, column=1)
button_6.grid(row=2, column=2)

button_7.grid(row=1, column=0)
button_8.grid(row=1, column=1)
button_9.grid(row=1, column=2)

button_0.grid(row=4, column=0)

button_add.grid(row=5, column=0)
button_equal.grid(row=5, column=1, columnspan=2)
button_clear.grid(row=4, column=1, columnspan=2)

button_substract.grid(row=6, column=0)
button_multiply.grid(row=6, column=1)
button_divide.grid(row=6, column=2)


# def myClick():
#     hello = f"Hello {e.get()}"
#     myLabel = Label(root, text=hello)
#     myLabel.pack()

# myButton = Button(root, text="Enter your name", command=myClick)
# myButton.pack()

root.mainloop()