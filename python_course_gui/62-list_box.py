from tkinter import *
from random import randint

root = Tk()
root.title("List Boxes")
root.iconbitmap('codemy.ico')
root.geometry("400x600")


# To add scrollbars to a list box => put the listbox into a frame, the scrollbars into a frame
# Create Frame and scrollbar
my_frame = Frame(root)
my_scrollbar = Scrollbar(my_frame, orient=VERTICAL)


# Create Listbox
# listbox
# SINGLE, BROWSE, MULTIPLE, EXTENDED
my_listbox = Listbox(my_frame, width=50, yscrollcommand=my_scrollbar.set, selectmode=MULTIPLE)

# Configured scrollbar
my_scrollbar.config(command=my_listbox.yview)
my_scrollbar.pack(side=RIGHT, fill=Y)
my_frame.pack()

my_listbox.pack(pady=15)


# Add Item to Listbox
# At the end of the box
# END inserts at the end of the box
my_listbox.insert(END, 'This is an item')
my_listbox.insert(END, 'Second item!')


# Add a list of items
my_list = ["One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three","One", "Two", "Three"]

for item in my_list:
    my_listbox.insert(END, item)

def delete():
    my_listbox.delete(ANCHOR)
    my_label.config(text="")

def delete_all():
    # delet eeverything from the listbox
    my_listbox.delete(0, END)
    my_label.config(text="")

def select():
    my_label.config(text=my_listbox.get(ANCHOR))

def select_all():
    # on selectmode=MULTIPLE
    result = ''
    for item in my_listbox.curselection():
        result += str(my_listbox.get(item)) + "\n"
    my_label.config(text=result)

def delete_multiple_selected():
    # on selectmode=MULTIPLE
    for item in reversed(my_listbox.curselection()):
        my_listbox.delete(item)
    my_label.config(text='')

my_button = Button(root, text="Delete", command=delete)
my_button.pack(pady=10)

my_button2 = Button(root, text="Select", command=select)
my_button2.pack(pady=10)

my_label = Label(root, text="")
my_label.pack(pady=5)

my_button3 = Button(root, text="Delete All", command=delete_all)
my_button3.pack(pady=10)

my_button4 = Button(root, text="Show Selected", command=select_all)
my_button4.pack(pady=10)

my_button5 = Button(root, text="Delete Selected", command=delete_multiple_selected)
my_button5.pack(pady=10)

root.mainloop()