from tkinter import *
from PIL import ImageTk, Image
import numpy as np
import matplotlib.pyplot as plt

root = Tk()
root.title("Matplots")
root.iconbitmap('codemy.ico')
root.geometry("400x200")

def graph():
    house_prices = np.random.normal(200000, 25000, 5000)
    plt.hist(house_prices)
    plt.hist(house_prices, 50)
    plt.hist(house_prices, 200)
    plt.show()


def pie():
    house_prices = np.random.normal(200000, 25000, 5000)
    plt.pie(house_prices)
    plt.show()


def polar():
    house_prices = np.random.normal(200000, 25000, 5000)
    plt.polar(house_prices)
    plt.show()


my_button = Button(root, text="Graph It!", command=graph)
my_button2 = Button(root, text="Pie It!", command=pie)
my_button3 = Button(root, text="Polar It!", command=polar)
my_button.pack()
my_button2.pack()
my_button3.pack()

root.mainloop()