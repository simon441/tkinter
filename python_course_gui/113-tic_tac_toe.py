from tkinter import *
from tkinter import messagebox, Button, Tk

root = Tk()
root.title("Tic-Tac-Toe")
root.iconbitmap("codemy.ico")
# root.geometry("400x600")

############
# Variables

# button font
SYSTEM_FACE = "SystemButtonFace"

# background of winning tiles
BG_WON = "red"

# number of button
BUTTONS_MAX_RANGE = 9


# list of all buttons
button_list = []

# whose turn (X or O)
winner = False

# Xs starts as True as game starts with X
clicked = True

# count how many clicked occured
count = 0

###########
# Functions


def disable_all_buttons():
    """ Disable buttons """

    for _ in range(BUTTONS_MAX_RANGE):
        button_list[_].config(state=DISABLED)
    # b1.config(state=DISABLED)
    # b2.config(state=DISABLED)
    # b3.config(state=DISABLED)
    # b4.config(state=DISABLED)
    # b5.config(state=DISABLED)
    # b6.config(state=DISABLED)
    # b7.config(state=DISABLED)
    # b8.config(state=DISABLED)
    # b9.config(state=DISABLED)


def show_win_message():
    global winner
    winner = True
    messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    disable_all_buttons()


def check_win_conditions(player, winner):
    win_message = "Congratulations!\n" + player + " Won!"

    patterns = [(0, 1, 2), (3, 4, 5), (6, 7, 8), (0, 3, 6),
                (1, 4, 7), (2, 5, 8), (0, 4, 8), (2, 5, 6)]
    # print(button_list)
    for p in patterns:
        # print(p)
        # print(player, button_list[p[0]]["text"], button_list[p[1]]["text"], button_list[p[2]]["text"])
        if button_list[p[0]]["text"] == player and button_list[p[1]]["text"] == player and button_list[p[2]]["text"] == player:
            button_list[p[0]].config(bg=BG_WON)
            button_list[p[1]].config(bg=BG_WON)
            button_list[p[2]].config(bg=BG_WON)
            winner = True
            messagebox.showinfo("Tic Tac Toe", win_message)
            disable_all_buttons()
            return winner
    # print("iswinner", winner)
    return winner
    # #  player across row 1
    # if b1["text"] == player and b2["text"] == player and b3["text"] == player:
    #     b1.config(bg=BG_WON)
    #     b2.config(bg=BG_WON)
    #     b3.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player across row 2
    # elif b4["text"] == player and b5["text"] == player and b6["text"] == player:
    #     b4.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b6.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player across row 3
    # elif b7["text"] == player and b8["text"] == player and b9["text"] == player:
    #     b7.config(bg=BG_WON)
    #     b8.config(bg=BG_WON)
    #     b9.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player column col 1
    # elif b1["text"] == player and b4["text"] == player and b7["text"] == player:
    #     b1.config(bg=BG_WON)
    #     b4.config(bg=BG_WON)
    #     b7.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player column col 2
    # elif b2["text"] == player and b5["text"] == player and b8["text"] == player:
    #     b2.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b8.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player column col 3
    # elif b3["text"] == player and b6["text"] == player and b9["text"] == player:
    #     b3.config(bg=BG_WON)
    #     b6.config(bg=BG_WON)
    #     b9.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player diagonal \
    # elif b1["text"] == player and b5["text"] == player and b9["text"] == player:
    #     b1.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b9.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # #  player diagonal /
    # elif b3["text"] == player and b5["text"] == player and b7["text"] == player:
    #     b3.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b7.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", win_message)
    #     disable_all_buttons()
    # return winner


def check_if_won(whose_turn):
    """ Check to see if someone won """
    global winner

    # X turn?
    if whose_turn == True:
        winner = check_win_conditions('X', winner)
    # O turn?
    else:
        winner = check_win_conditions('O', winner)

    # Check if tie
    if count == 9 and winner == False:
        messagebox.showinfo("Tic Tac Toe", "It's a Tie!\nNo one wins!")
        disable_all_buttons()
    # #  X across row 1
    # if b1["text"] == "X" and b2["text"] == "X" and  b3["text"] == "X" :
    #     b1.config(bg=BG_WON)
    #     b2.config(bg=BG_WON)
    #     b3.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X across row 2
    # elif b4["text"] == "X" and b5["text"] == "X" and  b6["text"] == "X" :
    #     b4.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b6.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X across row 3
    # elif b7["text"] == "X" and b8["text"] == "X" and  b9["text"] == "X" :
    #     b7.config(bg=BG_WON)
    #     b8.config(bg=BG_WON)
    #     b9.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X column col 1
    # elif b1["text"] == "X" and b4["text"] == "X" and  b7["text"] == "X" :
    #     b1.config(bg=BG_WON)
    #     b4.config(bg=BG_WON)
    #     b7.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X column col 2
    # elif b2["text"] == "X" and b5["text"] == "X" and  b8["text"] == "X" :
    #     b2.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b8.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X column col 3
    # elif b3["text"] == "X" and b6["text"] == "X" and  b9["text"] == "X" :
    #     b3.config(bg=BG_WON)
    #     b6.config(bg=BG_WON)
    #     b9.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X diagonal \
    # elif b1["text"] == "X" and b5["text"] == "X" and  b9["text"] == "X" :
    #     b1.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b9.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()
    # #  X diagonal /
    # elif b3["text"] == "X" and b5["text"] == "X" and  b7["text"] == "X" :
    #     b3.config(bg=BG_WON)
    #     b5.config(bg=BG_WON)
    #     b7.config(bg=BG_WON)
    #     winner = True
    #     messagebox.showinfo("Tic Tac Toe", "Congratulations!\nX Won!")
    #     disable_all_buttons()


def button_click(btn):
    """ Actions on button click """

    global clicked, count

    # for the X player
    # if btn['text'] = " " and clicked == True:
    #     btn["text"] = "X"
    #     clicked = False
    #     count += 1
    # # for the O player
    # elif  btn['text'] = " " and clicked == False:
    #     btn["text"] = "O"
    #     clicked = True
    #     count += 1

    # button has not been already selected
    if btn['text'] == " ":
        # for the X player
        if clicked == True:
            btn["text"] = "X"
        # for the O player
        else:
            btn["text"] = "O"
        check_if_won(clicked)

        #  change player's turn
        clicked = not clicked
        # another round
        count += 1

        # print(clicked, count)
        # check_if_won(clicked)
    else:
        messagebox.showerror(
            "Tic Tac Toe", "Hey! That box has already been selected\nPick another box...")


def button_click_list(btn_id):
    """ Actions on button click """

    global clicked, count, button_list

    # for the X player
    # if btn['text'] = " " and clicked == True:
    #     btn["text"] = "X"
    #     clicked = False
    #     count += 1
    # # for the O player
    # elif  btn['text'] = " " and clicked == False:
    #     btn["text"] = "O"
    #     clicked = True
    #     count += 1
    btn = button_list[btn_id]
    print('id', btn_id, btn, btn['text'])
      # button has not been already selected
    if btn['text'] == " ":
        # for the X player
        if clicked == True:
            btn["text"] = "X"
        # for the O player
        else:
            btn["text"] = "O"
        check_if_won(clicked)

        #  change player's turn
        clicked = not clicked
        # another round
        count += 1
    else:
        messagebox.showerror(
            "Tic Tac Toe", "Hey! That box has already been selected\nPick another box...")


def generate_buttons(n):
    for _ in range(n):  # xrange in python 2
        newbutton = tk.Button(
            text=" ", command=lambda name=_: button_click(newbutton))
        newbutton.grid(column=_)
        button_list.append(newbutton)


def reset():
    """ Start the game over """
    global b1, b2, b3, b4, b5, b6, b7, b8, b9
    global clicked
    global count
    clicked = True
    count = 0

    # Build the Buttons
    x = 0
    y = 0
    button_list.clear()
    for _ in range(0, 9):
        b = Button(root, text=" ", font=("Helvetica", 20), height=3,
                   width=6, bg=SYSTEM_FACE, command=lambda id = _: button_click_list(id))
        b.grid(row=x, column=y)
        if y == 2:
            y = 0
            x += 1
        else:
            y += 1
        button_list.append(b)

    # b1 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b1))
    # b2 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b2))
    # b3 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b3))
    # b4 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b4))
    # b5 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b5))
    # b6 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b6))
    # b7 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b7))
    # b8 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b8))
    # b9 = Button(root, text=" ", font=("Helvetica", 20), height=3,
    #             width=6, bg=SYSTEM_FACE, command=lambda: button_click(b9))

    # # Grid buttons to the screen
    # b1.grid(row=0, column=0)
    # b2.grid(row=0, column=1)
    # b3.grid(row=0, column=2)

    # b4.grid(row=1, column=0)
    # b5.grid(row=1, column=1)
    # b6.grid(row=1, column=2)

    # b7.grid(row=2, column=0)
    # b8.grid(row=2, column=1)
    # b9.grid(row=2, column=2)


# Menu
my_menu = Menu(root)
root.config(menu=my_menu)

# Options Menu
options_menu = Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="Options", menu=options_menu)
options_menu.add_command(label="Reset Game", command=reset)


# Start game
reset()

#################################
# Start GUI and Launch Event Loop
#################################

root.mainloop()
