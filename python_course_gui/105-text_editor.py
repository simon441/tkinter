from tkinter import *
from tkinter import filedialog, font
import os
from pathlib import Path

DEFAULT_TITLE = "Simon Cateau's TextPad!"

root = Tk()
root.title(DEFAULT_TITLE)
root.iconbitmap("codemy.ico")
root.geometry("1200x660")

STATUS_BAR_SPACING = "         "
FILE_TYPES = [("Text Files", "*.txt"), ("HTML Files", "*.html *.htm"), ("Python Files", "*.py *.pyc")]
FILE_TYPES_SELECT = list(FILE_TYPES)
# FILE_TYPES_SELECT_SAVE = list(FILE_TYPES)
FILE_TYPES_SELECT.append(("All Files", "*.*"))

############
# Functions 
############
def new_file():
    """ Create a new file """

    # Delete previous text
    my_textbox.delete(1.0, END)
    root.title('New File - TextPad!')
    status_bar.config(text="New File" + STATUS_BAR_SPACING)

def open_file():
    """ Open a file """

    # Delete previous text
    my_textbox.delete(1.0, END)

    #  get file
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Open File", filetypes=FILE_TYPES_SELECT)  # print(file_path)
    file_name = os.path.basename(file_path)
    
    # Update status bars
    root.title(f'{file_name} - TextPad!')
    status_bar.config(text=file_path + STATUS_BAR_SPACING)

    # Open the file
    text_file = open(file_path, 'r')
    data = text_file.read()
    # Add contents to textbox
    my_textbox.insert(END, data)
    # Close the opened file
    text_file.close()


def save_as_file():
    """ Save the file with a chosen filename """

    ext = ""
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else: 
        file_name = os.path.basename(status_bar['text'].strip())
        ext = Path(status_bar['text'].strip()).suffix
        if not ext:
            ext = ".*"

    file_path = filedialog.asksaveasfilename(initialfile=file_name, initialdir=os.getcwd(), filetypes=FILE_TYPES_SELECT, defaultextension=ext )
    if file_path:
        file_name = os.path.basename(file_path)

        # Update status bars
        root.title(f'{file_name} - TextPad!')
        status_bar.config(text=f"Saved: {file_path} {STATUS_BAR_SPACING}")

        # Save the file
        text_file = open(file_path, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()

############
# GUI
############

# Main Frame
main_frame = Frame(root)
main_frame.pack(pady=5)

# Scrollbar for the Text box
text_scroll = Scrollbar(main_frame)
text_scroll.pack(side=RIGHT, fill=Y)

# Text box
my_textbox = Text(main_frame, width=97, height=25, font=("Helvetica", 16), selectbackground="yellow", selectforeground="black", undo=True, yscrollcommand=text_scroll.set)
my_textbox.pack()

# Configure the scrollbar
text_scroll.config(command=my_textbox.yview)

########### 
# Menu bar
###########
main_menu = Menu(root)
root.config(menu=main_menu)

# Add File Menu
file_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New", command=new_file)
file_menu.add_command(label="Open", command=open_file)
file_menu.add_command(label="Save")
file_menu.add_command(label="Save As...", command=save_as_file)
# file_menu.add_command(label="Save A Copy...")
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)

# Add Edit Menu
edit_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Undo")
edit_menu.add_command(label="Redo")
edit_menu.add_separator()
edit_menu.add_command(label="Cut")
edit_menu.add_command(label="Copy")
edit_menu.add_command(label="Paste")


##########
# Status bar to bottom o fapp
status_bar = Label(root, text='Ready' + STATUS_BAR_SPACING, anchor=E)
status_bar.pack(fill=X, side=BOTTOM, ipady=5)

root.mainloop()