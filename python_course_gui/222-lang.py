from tkinter import Button, Label, Text, Tk, END

from langcodes import *
from langdetect import detect

root = Tk()
root.title('Language Detection!')

root.geometry('500x350')


def check_lang():
    """check language"""
    if my_text.compare('end-1c', '==', '1.0'):
        my_label.config(text='Hey! You forgot to enter something...')
    else:
        # Detect language code (eg: en, fr, es, ...)
        code = detect(my_text.get(1.0, END))
        # Get the language name from the language code found
        my_result = Language.make(language=code).display_name()
        my_label.config(text=f'Your Language is {my_result}')

my_text = Text(master=root, height=10, width=50)
my_text.pack(pady=20)

my_button = Button(master=root, text='Check Language', command=check_lang)
my_button.pack(pady=20)

my_label = Label(master=root, text='')
my_label.pack(pady=10)


root.mainloop()
