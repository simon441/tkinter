BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "addresses" (
	"first_name"	TEXT,
	"last_name"	TEXT,
	"address"	TEXT,
	"city"	TEXT,
	"state"	TEXT,
	"zipcode"	INTEGER
);
INSERT INTO "addresses" ("first_name","last_name","address","city","state","zipcode") VALUES ('John','Smith','123, Elm St','London','Great Britain',40000);
INSERT INTO "addresses" ("first_name","last_name","address","city","state","zipcode") VALUES ('Jane','Miller','89 Apple St','New York','NY',40000);
COMMIT;
