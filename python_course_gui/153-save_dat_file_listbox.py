import tkinter as Tk
import pickle

root = Tk.Tk()
root.title("Save Listbox to Dat File")
root.iconbitmap("codemy.ico")
root.geometry('500x400')


# Creating a list of sizes
sizes = [
    "Small",
    "Medium",
    "Large"
]

# Define a filename
filename = "data/dat_stuff_list"


def save_file():
    """
    Save text box data to file
    """
    # Grab the text from the list box
    stuff = my_list.get(0, Tk.END)

    # Open the file
    with open(file=filename, mode="wb") as output_file:
        # Actually add the data to the file
        pickle.dump(stuff, output_file)


def open_file():
    # Open the file
    with open(file=filename, mode="rb") as input_file:
        # Load the data from the file into a variable
        stuff = pickle.load(input_file)

        # Output to listbox
        for item in stuff:
            my_list.insert(Tk.END, item)

        # print(stuff)


def delete_items():
    my_list.delete(0, Tk.END)


my_list = Tk.Listbox(root)
my_list.pack(pady=20)
for item in sizes:
    my_list.insert(Tk.END, item)

my_button1 = Tk.Button(root, text="Save File", command=save_file)
my_button2 = Tk.Button(root, text="Open File", command=open_file)
my_button3 = Tk.Button(root, text="Delete Items", command=delete_items)
my_button1.pack(pady=20)
my_button2.pack(pady=20)
my_button3.pack(pady=20)

root.mainloop()
