from tkinter import *
from tkinter import filedialog, font, messagebox, colorchooser
import os, sys, platform
import win32print
import win32api
import tempfile
import subprocess
from pathlib import Path

DEFAULT_TITLE = "Simon Cateau's TextPad!"
opened_file_name = False
global selected_text
selected_text = False

root = Tk()
root.title(DEFAULT_TITLE)
root.iconbitmap("codemy.ico")
root.geometry("1200x680")

STATUS_BAR_SPACING = "         "
FILE_TYPES = [("Text Files", "*.txt"), ("HTML Files",
                                        "*.html *.htm"), ("Python Files", "*.py *.pyc")]
FILE_TYPES_SELECT = list(FILE_TYPES)
# FILE_TYPES_SELECT_SAVE = list(FILE_TYPES)
FILE_TYPES_SELECT.append(("All Files", "*.*"))

############
# Functions
############


def get_file_name():
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else:
        file_name = os.path.basename(status_bar['text'].strip())
    return file_name


def get_selected_text():
    return my_textbox.selection_get()


def new_file():
    """ Create a new file """

    # Delete previous text
    my_textbox.delete(1.0, END)
    root.title('New File - TextPad!')
    status_bar.config(text="New File" + STATUS_BAR_SPACING)


def open_file():
    """ Open a file """
    global opened_file_name

    # Delete previous text
    my_textbox.delete(1.0, END)

    #  get file
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(
    ), title="Open File", filetypes=FILE_TYPES_SELECT)  # print(file_path)
    file_name = os.path.basename(file_path)

    if file_name:
        opened_file_name = file_name

    # Update status bars
    root.title(f'{file_name} - TextPad!')
    status_bar.config(text=file_path + STATUS_BAR_SPACING)

    # Open the file
    text_file = open(file_path, 'r')
    data = text_file.read()
    # Add contents to textbox
    my_textbox.insert(END, data)
    # Close the opened file
    text_file.close()


def save_file():
    """ Save the file """

    filename = get_file_name()
    global opened_file_name
    if filename:
        # Save the file
        text_file = open(filename, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()
        # put status update or popup code
        messagebox.showinfo("File", f'File {filename} saved')
        status_bar.config(
            text=f"Saved: {opened_file_name} {STATUS_BAR_SPACING}")
    else:
        save_as_file()


def save_as_file():
    """ Save the file with a chosen filename """

    ext = ""
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else:
        file_name = os.path.basename(status_bar['text'].strip())
        ext = Path(status_bar['text'].strip()).suffix
        if not ext:
            ext = ".*"

    file_path = filedialog.asksaveasfilename(initialfile=file_name, initialdir=os.getcwd(
    ), filetypes=FILE_TYPES_SELECT, defaultextension=ext)
    if file_path:
        file_name = os.path.basename(file_path)

        # Update status bars
        root.title(f'{file_name} - TextPad!')
        status_bar.config(text=f"Saved: {file_path} {STATUS_BAR_SPACING}")

        # Save the file
        text_file = open(file_path, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()


def cut_text(e):
    """" Cut text """
    # get selected text from textbox
    global selected_text

    # Check to see if keyboard shortcut was used
    if e:
        selected_text = root.clipboard_get()
    else:
        if my_textbox.selection_get():
            selected_text = my_textbox.selection_get()
            # delete selected text from textbox
            my_textbox.delete("sel.first", "sel.last")

            # Clear the clipboard then append
            root.clipboard_clear()
            root.clipboard_append(selected_text)


def copy_text(e):
    """ Copy text """
    global selected_text

    if e:  # use keybord shortcut
        selected_text = root.clipboard_get()
    # get selected text from textbox
    if my_textbox.selection_get():
        selected_text = my_textbox.selection_get()

        # Clear the clipboard then append
        root.clipboard_clear()
        root.clipboard_append(selected_text)


def paste_text(e):
    """ Paste text """
    global selected_text

    # Check to see if the keyboard shortcut is used
    if e:
        selected_text = root.clipboard_get()
    else:
        if selected_text:
            position = my_textbox.index(INSERT)
            my_textbox.insert(position, selected_text)


def bold_it():
    """ Bold text """
    bold_font = font.Font(my_textbox, my_textbox.cget("font"))
    bold_font.configure(weight="bold")

    # Configure a tag
    my_textbox.tag_configure("bold", font=bold_font)

    # Define current tags
    try:
        current_tags = my_textbox.tag_names(SEL_FIRST)
    except TclError as e:
        return
    # Has the tag been set?
    if "bold" in current_tags:
        my_textbox.tag_remove('bold', 'sel.first', 'sel.last')
    else:
        my_textbox.tag_add("bold", 'sel.first', 'sel.last')


def italics_it():
    """ Make text italic """
    italics_font = font.Font(my_textbox, my_textbox.cget("font"))
    italics_font.configure(slant='italic')

    # Configure a tag
    my_textbox.tag_configure("italic", font=italics_font)

    # Define current tags
    try:
        current_tags = my_textbox.tag_names(SEL_FIRST)
    except TclError as e:
        return

    # Has the tag been set?
    if "italic" in current_tags:
        my_textbox.tag_remove('italic', 'sel.first', 'sel.last')
    else:
        my_textbox.tag_add("italic", 'sel.first', 'sel.last')

def text_color():
    """ Change selected text color """

    # Pick a color
    my_color = colorchooser.askcolor()[1]
    # print(my_color)
    if my_color:

        color_font = font.Font(my_textbox, my_textbox.cget("font"))

        # Configure a tag
        my_textbox.tag_configure("colored", font=color_font, foreground=my_color)

        # Define current tags
        try:
            current_tags = my_textbox.tag_names(SEL_FIRST)

            if "colored" in current_tags:
                my_textbox.tag_remove('colored', 'sel.first', 'sel.last')
            else:
                my_textbox.tag_add("colored", 'sel.first', 'sel.last')
        except TclError as e:
            pass


def all_text_color():
    """ Change whole document text color  """

    # Pick a color
    my_color = colorchooser.askcolor()[1]
    # print(my_color)
    if my_color:
        my_textbox.config(fg=my_color)

def bg_color():
    """ Change whole document background color  """

    # Pick a color
    my_color = colorchooser.askcolor()[1]
    # print(my_color)
    if my_color:
        my_textbox.config(bg=my_color)

def undo_action(e = None):
    try:
        my_textbox.edit_undo()
    except TclError as e:
        pass


def redo_action(e = None):
    try:
        my_textbox.edit_redo()
    except TclError as e:
        pass

def print_file():
    file_to_print = os.path.basename(filedialog.askopenfilename(initialdir=os.getcwd(
    ), title="Open File", filetypes=FILE_TYPES_SELECT))
    print(file_to_print)

    if file_to_print:
        print_data(file_to_print)

def print_current_document():
    print_data()

def print_linux(data):
    lpr =  subprocess.Popen("/usr/bin/lpr", stdin=subprocess.PIPE)
    lpr.stdin.write(data)
    lpr.stdin.close()


def print_data(filename = None):
    # printer_name =win32print.GetDefaultPrinter()
    # print(printer_name)
    # File name
    if filename == None:

        filename = tempfile.mktemp(".txt")
        fname = open(filename, "w")
        if platform.system() == "Windows":
            fname.write(my_textbox.get(1.0, END))
    else:
        fname = open(filename, "r")

    if platform.system() == "Windows":
        win32api.ShellExecute(0,
        "print",
        filename,
        None,
        ".",
        0
        )

    if platform.system() == "Linux" or platform.system() == 'Darwin':
        print_linux(fname)
    fname.close()


def quit_app(e=None):
    msg = messagebox.askokcancel('TextPad', 'Do your want to quit TextPad?')
    if msg:
        root.destroy()

############
# GUI
############


# Create a toobar frame
toolbar_frame = Frame(root)
toolbar_frame.pack(fill=X)

# Main Frame
main_frame = Frame(root)
main_frame.pack(pady=5)

# Scrollbar for the Text box
text_scroll = Scrollbar(main_frame)
text_scroll.pack(side=RIGHT, fill=Y)

# Horizontal Scrollbar
horiz_scroll = Scrollbar(main_frame, orient=HORIZONTAL)
horiz_scroll.pack(side=BOTTOM, fill=X)


# Text box
# wrap = none => no wrap at textbox end line
my_textbox = Text(main_frame, width=97, height=25, font=("Helvetica", 16), selectbackground="yellow", selectforeground="black", undo=True, yscrollcommand=text_scroll.set,
                  wrap="word",
                  xscrollcommand=horiz_scroll.set)
my_textbox.pack()

# Configure the scrollbar
text_scroll.config(command=my_textbox.yview)
horiz_scroll.config(command=my_textbox.xview)

###########
# Menu bar
###########
main_menu = Menu(root)
root.config(menu=main_menu)

# Add File Menu
file_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New", command=new_file)
file_menu.add_command(label="Open", command=open_file)
file_menu.add_command(label="Save", command=save_file)
file_menu.add_command(label="Save As...", command=save_as_file)
# file_menu.add_command(label="Save A Copy...")
file_menu.add_separator()
file_menu.add_command(label="Print A File", command=print_file)
file_menu.add_command(label="Print", accelerator="Ctrl+P", command=print_current_document)
file_menu.add_separator()
file_menu.add_command(label="Exit", accelerator="Ctrl+Q", command=quit_app)

# Add Edit Menu
edit_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Undo", accelerator="Ctrl+Z",
                      underline=1, command=undo_action)
edit_menu.add_command(label="Redo", accelerator="Ctrl+Y",
                      underline=1, command=redo_action)
edit_menu.add_separator()
edit_menu.add_command(label="Cut", accelerator="Ctrl+X",
                      underline=1, command=lambda: cut_text(False))
edit_menu.add_command(label="Copy", accelerator="Ctrl+C",
                      underline=1, command=lambda: copy_text(False))
edit_menu.add_command(label="Paste", accelerator="Ctrl+V",
                      underline=1, command=lambda: paste_text(False))



# Add Color Menu
color_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Colors", menu=color_menu)
color_menu.add_command(label="Selected Text",
                      underline=1, command=text_color)
color_menu.add_command(label="All Text",
                      underline=1, command=all_text_color)
color_menu.add_command(label="Background",
                      underline=1, command=bg_color)
edit_menu.add_separator()

##########
# Status bar to bottom of app
status_bar = Label(root, text='Ready' + STATUS_BAR_SPACING, anchor=E)
status_bar.pack(fill=X, side=BOTTOM, ipady=15)

#########
# Buttons

# Bold Button
bold_button = Button(toolbar_frame, text="Bold", command=bold_it)
bold_button.grid(row=0, column=0, sticky=W, padx=5)

# Italics Button
italics_button = Button(toolbar_frame, text="Italics", command=italics_it)
italics_button.grid(row=0, column=1, padx=5)

# Undo Button
undo_button = Button(toolbar_frame, text="Undo", command=undo_action)
undo_button.grid(row=0, column=2, padx=5)

# Redo Button
redo_button = Button(toolbar_frame, text="Redo", command=redo_action)
redo_button.grid(row=0, column=3, padx=5)

# Text Color Button
text_color_button = Button(toolbar_frame, text="Text Color", command=text_color)
text_color_button.grid(row=0, column=4, padx=5)

#########
# Bindings

# File bindings
root.bind('<Control-KeyPress-q>', quit_app)

# Edit bindings
root.bind('<Control-Key-Z>', undo_action)
root.bind('<Control-Key-Y>', redo_action)
root.bind('<Control-Key-X>', cut_text)
root.bind('<Control-Key-C>', copy_text)
root.bind('<Control-Key-V>', paste_text)

#################################
# Start GUI and Launch Event Loop
#################################

root.protocol("WM_DELETE_WINDOW", quit_app)

root.mainloop()
# time 5:01
