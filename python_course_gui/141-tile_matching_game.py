import tkinter as Tk
import random
from tkinter import messagebox

root = Tk.Tk()
root.title("Match Game!")
root.iconbitmap("codemy.ico")
root.geometry('500x550')
# root.config(bg="#c3c3c3")

# Create the Matches
matches = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6]

# Shuffle the matches
random.shuffle(matches)

# print(matches)

# Variables
count = 0
answer_list = []
answer_dict = {}


def button_click(btn, number):
    """
    Function for clicking buttons 
    btn : Tkinter Button clicked
    number: position of the clicked button in the list
    """
    global count, answer_dict, answer_list

    # Empty cell and number of cells shown are one or two
    if btn["text"] == ' ' and count < 2:
        btn["text"] = matches[number]
        # Add number to answer list
        answer_list.append(number)
        # Add button and number to answer dictionary
        answer_dict[btn] = matches[number]
        # Increment the counter
        count += 1

    # Determine if correct or not
    if len(answer_list) == 2:
        if matches[answer_list[0]] == matches[answer_list[1]]:
            my_label.config(text="match")
            for key in answer_dict:
                key["state"] = Tk.DISABLED
            count = 0
            answer_list = []
            answer_dict = {}
        else:
            my_label.config(text="uh-oh!")
            count = 0
            answer_list = []
            # popup box
            messagebox.showinfo("Incorrect", "Incorrect")

            # Reset the buttons
            for key in answer_dict:
                key["text"] = ' '

            answer_dict = {}


# Frame
my_frame = Tk.Frame(root)
my_frame.pack(pady=10)

# Buttons
b0 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b0, 0))
b1 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b1, 1))
b2 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b2, 2))
b3 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b3, 3))
b4 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b4, 4))
b5 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b5, 5))
b6 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b6, 6))
b7 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b7, 7))
b8 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b8, 8))
b9 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b9, 9))
b10 = Tk.Button(my_frame, text=' ', font=(
    'Helvetica', 20), height=3, width=6, command=lambda: button_click(b10, 10))
b11 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
                height=3, width=6, command=lambda: button_click(b11, 11))

# Grid the buttons
b0.grid(row=0, column=0)
b1.grid(row=0, column=1)
b2.grid(row=0, column=2)
b3.grid(row=0, column=3)

b4.grid(row=1, column=0)
b5.grid(row=1, column=1)
b6.grid(row=1, column=2)
b7.grid(row=1, column=3)

b8.grid(row=2, column=0)
b9.grid(row=2, column=1)
b10.grid(row=2, column=2)
b11.grid(row=2, column=3)

my_label = Tk.Label(root, text='')
my_label.pack(pady=20)

root.mainloop()
