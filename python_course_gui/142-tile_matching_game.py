import tkinter as Tk
import random
from tkinter import messagebox

root = Tk.Tk()
root.title("Match Game!")
root.iconbitmap("codemy.ico")
root.geometry('460x450')
# root.config(bg="#c3c3c3")

# Variables
# Winner counter
global winner
winner = 0

# Count the number of clicked cells
count = 0
# Save the clicked cells number
answer_list = []
# Save the button and the clicked button number (btn: number)
answer_dict = {}

# matches
matches = []

b = Tk.Button(root)

DEFAULT_BG_BUTTON = b["bg"]


def reset_game():
    """ Reset the game """
    global matches, winner

    # Create the Matches
    matches = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6]

    # Shuffle the matches
    random.shuffle(matches)

    # print(matches)

    # Reset the winner
    winner = 0

    # reset label
    my_label.config(text="")

    # Reset the titles
    button_list = [b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11]
    # Loop through buttons and change colors
    for button in button_list:
        button.config(text=" ", bg=DEFAULT_BG_BUTTON, state=Tk.NORMAL)


def win():
    """ When the game is won """
    my_label.config(text="Congratulations! You Win!!!")
    button_list = [b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11]
    # Loop through buttons and change colors
    for button in button_list:
        button.config(bg="yellow")


def button_click(btn, number):
    """
    Function for clicking buttons 
    btn : Tkinter Button clicked
    number: position of the clicked button in the list
    """
    global count, answer_dict, answer_list, winner

    # Empty cell and number of cells shown are one or two
    if btn["text"] == ' ' and count < 2:
        btn["text"] = matches[number]
        # Add number to answer list
        answer_list.append(number)
        # Add button and number to answer dictionary
        answer_dict[btn] = matches[number]
        # Increment the counter
        count += 1

    # Determine if correct or not
    if len(answer_list) == 2:
        if matches[answer_list[0]] == matches[answer_list[1]]:
            my_label.config(text="MATCH!")
            for key in answer_dict:
                key["state"] = Tk.DISABLED
            count = 0
            answer_list = []
            answer_dict = {}

            # Increment the winner counter
            winner += 1
            if winner == 6:
                win()
        else:
            # my_label.config(text="uh-oh!")
            count = 0
            answer_list = []
            # popup box
            messagebox.showinfo("Incorrect", "Incorrect")

            # Reset the buttons
            for key in answer_dict:
                key["text"] = ' '

            answer_dict = {}


# Frame
my_frame = Tk.Frame(root)
my_frame.pack(pady=10)

# Buttons
b0 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b0, 0), relief="groove")
b1 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b1, 1), relief="groove")
b2 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b2, 2), relief="groove")
b3 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b3, 3), relief="groove")
b4 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b4, 4), relief="groove")
b5 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b5, 5), relief="groove")
b6 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b6, 6), relief="groove")
b7 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b7, 7), relief="groove")
b8 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b8, 8), relief="groove")
b9 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
               height=3, width=6, command=lambda: button_click(b9, 9), relief="groove")
b10 = Tk.Button(my_frame, text=' ', font=(
    'Helvetica', 20), height=3, width=6, command=lambda: button_click(b10, 10), relief="groove")
b11 = Tk.Button(my_frame, text=' ', font=('Helvetica', 20),
                height=3, width=6, command=lambda: button_click(b11, 11), relief="groove")

# Grid the buttons
b0.grid(row=0, column=0)
b1.grid(row=0, column=1)
b2.grid(row=0, column=2)
b3.grid(row=0, column=3)

b4.grid(row=1, column=0)
b5.grid(row=1, column=1)
b6.grid(row=1, column=2)
b7.grid(row=1, column=3)

b8.grid(row=2, column=0)
b9.grid(row=2, column=1)
b10.grid(row=2, column=2)
b11.grid(row=2, column=3)

my_label = Tk.Label(root, text='', font=("Helvetica", 10))
my_label.pack(pady=20)

# Create a Menu
my_menu = Tk.Menu(root)
root.config(menu=my_menu)

# Options dropdown Menu
options_menu = Tk.Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="Options", menu=options_menu)

options_menu.add_command(label="Reset Game", command=reset_game)
options_menu.add_separator()
options_menu.add_command(label="Exit Game", command=root.quit)

reset_game()

root.mainloop()
