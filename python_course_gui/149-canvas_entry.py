import tkinter as Tk
from PIL import Image, ImageTk

APP_WIDTH = 323
APP_HEIGHT = 576

BG_IMAGE_ = "images/app/bg.png"
FG_COLOR = "#336d92"

root = Tk.Tk()
root.title("Canvas Entry Boxes")
root.iconbitmap("codemy.ico")
root.geometry(f'{APP_WIDTH}x{APP_HEIGHT}')
# Make sure app can't be resized
root.resizable(width=False, height=False)

# Define background image
bg = ImageTk.PhotoImage(file=BG_IMAGE_)

# Define a canvas
my_canvas = Tk.Canvas(root, width=APP_WIDTH, height=APP_HEIGHT)
my_canvas.pack(fill=Tk.BOTH, expand=True)

# Put the image on the canvas
my_canvas.create_image(0, 0, image=bg, anchor=Tk.NW)


# Define Entry Boxes
username_entry = Tk.Entry(root, font=("Helvetica", 24),
                          width=14, fg=FG_COLOR, bd=0)
password_entry = Tk.Entry(root, font=("Helvetica", 24),
                          width=14, fg=FG_COLOR, bd=0)

username_entry.insert(0, "username")
password_entry.insert(0, "password")

# Add the entry boxes to the canvas
username_window = my_canvas.create_window(
    34, 290, anchor=Tk.NW, window=username_entry)
password_window = my_canvas.create_window(
    34, 370, anchor=Tk.NW, window=password_entry)


def welcome(e=None):
    """
    Create welcome screen
    """
    username_entry.destroy()
    password_entry.destroy()
    login_button.destroy()

    # Add a welcome message
    my_canvas.create_text(160, 450, text="Welcome!", font=(
        "Helvetica", 40), fill="white")


# Define button
login_button = Tk.Button(root, text="Login", font=(
    "Helvetica", 20), width=15, fg=FG_COLOR, command=welcome)

login_window = my_canvas.create_window(
    36, 470, anchor=Tk.NW, window=login_button)


def entry_clear(e):
    """
    Clear the entry box on click
    """
    if username_entry.get() == 'username':
        username_entry.delete(0, Tk.END)
    if password_entry.get() == 'password':
        password_entry.delete(0, Tk.END)
        # Change text to stars
        password_entry.config(show="*")


# Bind the entry boxes
username_entry.bind('<Button-1>', entry_clear)
password_entry.bind('<Button-1>', entry_clear)
username_entry.bind('<FocusIn>', entry_clear)
password_entry.bind('<FocusIn>', entry_clear)

login_button.bind('<Return>', welcome)

root.mainloop()
