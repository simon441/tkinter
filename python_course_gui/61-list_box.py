from tkinter import *
from random import randint

root = Tk()
root.title("List Boxes")
root.iconbitmap('codemy.ico')
root.geometry("400x400")

# Create Listbox
my_listbox = Listbox(root)
my_listbox.pack(pady=15)

# Add Item to Listbox
# At the end of the box
# END inserts at the end of the box
my_listbox.insert(END, 'This is an item')
my_listbox.insert(END, 'Second item!')


# Add a list of items
my_list = ["One", "Two", "Three"]

for item in my_list:
    my_listbox.insert(END, item)

def delete():
    my_listbox.delete(ANCHOR)
    my_label.config(text="")

def select():
    my_label.config(text=my_listbox.get(ANCHOR))

my_button = Button(root, text="Delete", command=delete)
my_button.pack(pady=10)

my_button2 = Button(root, text="Select", command=select)
my_button2.pack(pady=10)

my_label = Label(root, text="")
my_label.pack(pady=5)

root.mainloop()