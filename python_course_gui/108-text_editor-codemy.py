from tkinter import *
from tkinter import filedialog, font, messagebox
import os
from pathlib import Path

DEFAULT_TITLE = "Simon Cateau's TextPad!"
opened_file_name = False
global selected_text
selected_text = False

root = Tk()
root.title(DEFAULT_TITLE)
root.iconbitmap("codemy.ico")
root.geometry("1200x680")

STATUS_BAR_SPACING = "         "
FILE_TYPES = [("Text Files", "*.txt"), ("HTML Files", "*.html *.htm"), ("Python Files", "*.py *.pyc")]
FILE_TYPES_SELECT = list(FILE_TYPES)
# FILE_TYPES_SELECT_SAVE = list(FILE_TYPES)
FILE_TYPES_SELECT.append(("All Files", "*.*"))

############
# Functions
############
def get_file_name():
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else:
        file_name = os.path.basename(status_bar['text'].strip())
    return file_name


def get_selected_text():
    return my_textbox.selection_get()

def new_file():
    """ Create a new file """

    # Delete previous text
    my_textbox.delete(1.0, END)
    root.title('New File - TextPad!')
    status_bar.config(text="New File" + STATUS_BAR_SPACING)

def open_file():
    """ Open a file """
    global opened_file_name

    # Delete previous text
    my_textbox.delete(1.0, END)

    #  get file
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Open File", filetypes=FILE_TYPES_SELECT)  # print(file_path)
    file_name = os.path.basename(file_path)

    if file_name:
        opened_file_name = file_name

    # Update status bars
    root.title(f'{file_name} - TextPad!')
    status_bar.config(text=file_path + STATUS_BAR_SPACING)

    # Open the file
    text_file = open(file_path, 'r')
    data = text_file.read()
    # Add contents to textbox
    my_textbox.insert(END, data)
    # Close the opened file
    text_file.close()

def save_file():
    """ Save the file """

    filename = get_file_name()
    global opened_file_name
    if filename:
        # Save the file
        text_file = open(filename, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()
        # put status update or popup code
        messagebox.showinfo("File", f'File {filename} saved')
        status_bar.config(text=f"Saved: {opened_file_name} {STATUS_BAR_SPACING}")
    else:
        save_as_file()


def save_as_file():
    """ Save the file with a chosen filename """

    ext = ""
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else:
        file_name = os.path.basename(status_bar['text'].strip())
        ext = Path(status_bar['text'].strip()).suffix
        if not ext:
            ext = ".*"

    file_path = filedialog.asksaveasfilename(initialfile=file_name, initialdir=os.getcwd(), filetypes=FILE_TYPES_SELECT, defaultextension=ext )
    if file_path:
        file_name = os.path.basename(file_path)

        # Update status bars
        root.title(f'{file_name} - TextPad!')
        status_bar.config(text=f"Saved: {file_path} {STATUS_BAR_SPACING}")

        # Save the file
        text_file = open(file_path, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()



def cut_text(e):
    """" Cut text """
    # get selected text from textbox
    global selected_text

    # Check to see if keyboard shortcut was used
    if e:
        selected_text = root.clipboard_get()
    else:
        if my_textbox.selection_get():
            selected_text = my_textbox.selection_get()
            # delete selected text from textbox
            my_textbox.delete("sel.first", "sel.last")

            # Clear the clipboard then append
            root.clipboard_clear()
            root.clipboard_append(selected_text)


def copy_text(e):
    """ Copy text """
    global selected_text

    if e: # use keybord shortcut
        selected_text = root.clipboard_get()
    # get selected text from textbox
    if my_textbox.selection_get():
        selected_text = my_textbox.selection_get()

        # Clear the clipboard then append
        root.clipboard_clear()
        root.clipboard_append(selected_text)

def paste_text(e):
    """ Paste text """
    global selected_text

    # Check to see if the keyboard shortcut is used
    if e:
        selected_text = root.clipboard_get()
    else:
        if selected_text:
            position = my_textbox.index(INSERT)
            my_textbox.insert(position, selected_text)


def undo_action(e):
    pass

def redo_action(e):
    pass

def quit_app(e = None):
    msg = messagebox.askokcancel('TextPad', 'Do your want to quit TextPad?')
    if msg:
        root.destroy()

############
# GUI
############

# Main Frame
main_frame = Frame(root)
main_frame.pack(pady=5)

# Scrollbar for the Text box
text_scroll = Scrollbar(main_frame)
text_scroll.pack(side=RIGHT, fill=Y)

# Horizontal Scrollbar
horiz_scroll = Scrollbar(main_frame, orient=HORIZONTAL)
horiz_scroll.pack(side=BOTTOM, fill=X)


# Text box
# wrap = none => no wrap at textbox end line
my_textbox = Text(main_frame, width=97, height=25, font=("Helvetica", 16), selectbackground="yellow", selectforeground="black", undo=True, yscrollcommand=text_scroll.set,
wrap="none",
 xscrollcommand=horiz_scroll.set)
my_textbox.pack()

# Configure the scrollbar
text_scroll.config(command=my_textbox.yview)
horiz_scroll.config(command=my_textbox.xview)

###########
# Menu bar
###########
main_menu = Menu(root)
root.config(menu=main_menu)

# Add File Menu
file_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New", command=new_file)
file_menu.add_command(label="Open", command=open_file)
file_menu.add_command(label="Save", command=save_file)
file_menu.add_command(label="Save As...", command=save_as_file)
# file_menu.add_command(label="Save A Copy...")
file_menu.add_separator()
file_menu.add_command(label="Exit", accelerator="Ctrl+Q", command=quit_app)

# Add Edit Menu
edit_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Edit", menu=edit_menu)
edit_menu.add_command(label="Undo", accelerator="Ctrl+Z", underline=1, command= my_textbox.edit_undo)
edit_menu.add_command(label="Redo", accelerator="Ctrl+Y", underline=1, command=my_textbox.edit_redo)
edit_menu.add_separator()
edit_menu.add_command(label="Cut", accelerator="Ctrl+X", underline=1, command=lambda: cut_text(False))
edit_menu.add_command(label="Copy", accelerator="Ctrl+C", underline=1, command=lambda: copy_text(False))
edit_menu.add_command(label="Paste", accelerator="Ctrl+V", underline=1, command=lambda: paste_text(False))


##########
# Status bar to bottom o fapp
status_bar = Label(root, text='Ready' + STATUS_BAR_SPACING, anchor=E)
status_bar.pack(fill=X, side=BOTTOM, ipady=15)


#########
# Bindings

# File bindings
root.bind('<Control-KeyPress-q>', quit_app)

# Edit bindings
root.bind('<Control-Key-Z>', undo_action)
root.bind('<Control-Key-Y>', redo_action)
root.bind('<Control-Key-X>', cut_text)
root.bind('<Control-Key-C>', copy_text)
root.bind('<Control-Key-V>', paste_text)

#################################
# Start GUI and Launch Event Loop
#################################

root.protocol("WM_DELETE_WINDOW", quit_app)

root.mainloop()