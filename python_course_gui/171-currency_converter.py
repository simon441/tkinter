from tkinter import *
from tkinter import messagebox, ttk

root = Tk()
root.title("Currency Converter")
root.iconbitmap("codemy.ico")
root.geometry('500x500')


# Create Tabs
my_notebook = ttk.Notebook(root)
my_notebook.pack(pady=5)


# Create Two Frames
currency_frame = Frame(my_notebook, width=480, height=480)
conversion_frame = Frame(my_notebook, width=480, height=480)

currency_frame.pack(fill=BOTH, expand=1)
conversion_frame.pack(fill=BOTH, expand=1)

my_notebook.add(currency_frame, text="Currencies")
my_notebook.add(conversion_frame, text="Convert")

# Disable 2nd tab
my_notebook.tab(1, state=DISABLED)

############################
# CURRENCY STUFF
############################

def lock():
    if not home_entry.get() or not conversion_entry.get() or not rate_entry.get():
        messagebox.showwarning("Warning", "You didn't fill all the fields")
    else:
        try:
            float(rate_entry.get())
        except ValueError:
            messagebox.showwarning("Warning", "Rate must be a number")
            return
        # Diable Entry Boxes
        home_entry.config(state="disabled")
        conversion_entry.config(state="disabled")
        rate_entry.config(state="disabled")
        
        # Enable Tab
        my_notebook.tab(1, state="normal")

        # Change Tab Fields
        amount_label.config(text=f"Amount of {home_entry.get()} To Convert To {conversion_entry.get()}")
        converted_label.config(text=f"Equals This Many {conversion_entry.get()}")
        convert_button.config(text=f"Convert From {home_entry.get()}")

def unlock():
    # Enable Entry Boxes
    home_entry.config(state="normal")
    conversion_entry.config(state="normal")
    rate_entry.config(state="normal")
    # Diable Tab
    my_notebook.tab(1, state=DISABLED)

home = LabelFrame(currency_frame, text="Your Home Currency")
home.pack(pady=20)

# Home currency entry box
home_entry = Entry(home, font=('Helvetica', 24))
home_entry.pack(pady=10, padx=10)

# Conversion Currency Entry
conversion = LabelFrame(currency_frame, text='Conversion Currency')
conversion.pack(pady=20)

# Label convert to
conversion_label = Label(conversion, text="Currency To Convert To...")
conversion_label.pack(pady=10)

# Entry convert to
conversion_entry = Entry(conversion, font=('Helvetica', 24))
conversion_entry.pack(padx=10, pady=10)

# Entry Label
rate_label = Label(conversion, text="Current Conversion Rate...")
rate_label.pack(pady=10)

# Rate Entry
rate_entry = Entry(conversion, font=('Helvetica', 24))
rate_entry.pack(padx=10, pady=10)

# Button Frame
button_frame = Frame(currency_frame)
button_frame.pack(pady=20)

# Buttons for Button Frame
lock_button = Button(button_frame, text="Lock", command=lock)
lock_button.grid(row=0, column=0, padx=10)

unlock_button = Button(button_frame, text="Unlock", command=unlock)
unlock_button.grid(row=0, column=1, padx=10)


############################
# CONVERSION STUFF
############################

def convert():
    # Clear Convered Entry Box
    converted_entry.delete(0, END)

    # Convert
    try:
        conversion = float(rate_entry.get()) * float(amount_entry.get())    
    except ValueError:
        messagebox.showwarning("Warning", "Amount must be a number")
        return
    
    # Convert To 2 decimals
    conversion = round(conversion, 2)

    # Add commas if USD
    if home_entry.get().lower() == 'usd':
        conversion = '{:,}'.format(conversion)
    else:
        conversion = '{:,}'.format(conversion) .replace(',', ' ')

    # Update Entry Box
    converted_entry.insert(0, conversion)



def clear():
    amount_entry.delete(0, END)
    converted_entry.delete(0, END)

amount_label = LabelFrame(conversion_frame, text="Amount To Convert")
amount_label.pack(pady=20)

# Entry Box for Amount
amount_entry = Entry(amount_label, font=('Helvetica', 24))
amount_entry.pack(padx=10, pady=20)

# Convert Button
convert_button = Button(amount_label, text="Convert", command=convert)
convert_button.pack(pady=20)

# Equals Frame
converted_label = LabelFrame(conversion_frame, text="Converted Currency")
converted_label.pack(pady=20)

# Converted Entry
converted_entry = Entry(converted_label, font=('Helvetica', 24), bd=0, bg=root["bg"])
converted_entry.pack(padx=10, pady=10)

# Clear Button
clear_button = Button(conversion_frame, text="Clear", command=clear)
clear_button.pack(pady=20)

# Fake Label for spacing
spacer = Label(conversion_frame, text="", width=68)
spacer.pack()

root.mainloop()
