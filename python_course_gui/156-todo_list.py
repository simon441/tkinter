import tkinter as Tk
from tkinter.font import Font, families
from tkinter import filedialog
from pathlib import Path
import pickle

root = Tk.Tk()
root.title("ToDo List!")
root.iconbitmap("codemy.ico")
root.geometry('600x500')

# Define the font
my_font = Font(
    family="Comic Sans MS",
    # family="Brush Script MT",
    size=28,
    weight="bold"
)

# constants
CROSSED_COLOR = "#dedede"
UNCROSSED_COLOR = "#464646"


# Create frame
my_frame = Tk.Frame(root)
my_frame.pack(pady=10)

btn = Tk.Button(root)
systemface = btn['bg']
print(systemface)
# Create listbox
# my_list = Tk.Listbox(my_frame,
#  font=my_font,
#  width=25,
#  height=5,
#  bg="SystemButtonFace",
#  bd=0,
#  fg="#464646",

#  )
my_list = Tk.Listbox(my_frame,
                     font=my_font,
                     width=25,
                     height=5,
                     bg="SystemButtonFace",
                     bd=0,
                     fg=UNCROSSED_COLOR,
                     highlightthickness=0,
                     selectbackground="#a6a6a6",
                     activestyle="none")
my_list.pack(side=Tk.LEFT, fill=Tk.BOTH)

"""
# Create dummy list
stuff = ["Walk The Dog", "Buy Groceries", "Learn Tkinter", "Rule The World"]
# Add dummy list to list box
for item in stuff:
    my_list.insert(Tk.END, item)
"""
# for name in sorted(families()):
#     print(name)

# Create scrollbar
scrollbar = Tk.Scrollbar(my_frame)
scrollbar.pack(side=Tk.RIGHT, fill=Tk.BOTH)

# Add scrollbar
my_list.config(yscrollcommand=scrollbar.set)
scrollbar.config(command=my_list.yview)

# Create entry box to add items to the list
my_entry = Tk.Entry(root, font=("Helvetica", 24))
my_entry.pack(pady=20)

# Create a button frame
button_frame = Tk.Frame(root)
button_frame.pack(pady=20)

# Functions


def delete_item():
    """
    Delete an item from the list
    """
    my_list.delete(Tk.ANCHOR)


def add_item():
    """
    Add an item to the list
    """
    my_list.insert(Tk.END, my_entry.get())
    my_entry.delete(0, Tk.END)


def done_item():
    """
    Cross an item from the list when done
    """
    print(my_list.curselection(), len(my_list.curselection()) == 0)
    try:
        # Cross off item
        my_list.itemconfig(
            my_list.curselection(),
            fg=CROSSED_COLOR
        )
        # Get rid of selection bar
        my_list.selection_clear(0, Tk.END)
    except Tk.TclError as e:
        print(e)


def waiting_item():
    """
    Remove the cross from an item from the list
    """
    try:
        # Uncross item
        my_list.itemconfig(
            my_list.curselection(),
            fg=UNCROSSED_COLOR
        )
        # Get rid of selection bar
        my_list.selection_clear(0, Tk.END)
    except Tk.TclError as e:
        print(e)


def delete_crossed():
    """
    Delete all crossed items
    """
    count = 0
    while count < my_list.size():
        if my_list.itemcget(count, "fg") == CROSSED_COLOR:
            my_list.delete(my_list.index(count))
        else:
            count += 1


def open_list():
    """
    Populate the list with contents from file
    """
    file_name = Tk.filedialog.askopenfilename(
        initialdir=Path.joinpath(Path.cwd(), Path('data')),
        title="Open File",
        filetypes=(("Dat Files", "*.dat"), ("All Files", "*.*")),
    )
    if file_name:
        # Delete currently open list
        my_list.delete(0, Tk.END)

        # Open the file
        with open(file_name, "rb") as input_file:
            # Load the data from the file
            stuff = pickle.load(input_file)

            # Output data to the screen
            for item in stuff:
                my_list.insert(Tk.END, item)


def save_list():
    """
    Save the list to file
    """
    file_name = Tk.filedialog.asksaveasfilename(
        initialdir=Path.joinpath(Path.cwd(), Path('data')),
        title="Save File",
        filetypes=(("Dat Files", "*.dat"), ("All Files", "*.*")),
    )

    if file_name:
        if file_name.endswith(".dat"):
            pass
        else:
            file_name = f'{file_name}.dat'
        # Delete crossed off items before saving
        # delete_crossed()
        the_list = []
        count = 0
        while count < my_list.size():
            if my_list.itemcget(count, "fg") == CROSSED_COLOR:
                # my_list.delete(my_list.index(count))
                pass
            else:
                the_list.append(my_list.get(my_list.index(count)))
            count += 1

        print(the_list)
        # Grab all the list items
        stuff = my_list.get(0, Tk.END)
        stuff = the_list

        # Open the file
        with open(file=file_name, mode="wb") as f:
            # Add the data to the file
            pickle.dump(stuff, f)


def clear_list():
    """
    Clear the list: remove all items
    """
    my_list.delete(0, Tk.END)


# Create menu
my_menu = Tk.Menu(root)
root.config(menu=my_menu)

# Add items to the menu
file_menu = Tk.Menu(my_menu, tearoff=False)
my_menu.add_cascade(label="File", menu=file_menu)
# Add dropdown items
file_menu.add_command(label="Open List", command=open_list)
file_menu.add_command(label="Save List", command=save_list)
file_menu.add_separator()
file_menu.add_command(label="Clear List", command=clear_list)
file_menu.add_separator()
file_menu.add_command(label="Exit", command=root.quit)

# Add some buttons
delete_button = Tk.Button(
    button_frame, text="Delete Item", command=delete_item)
add_button = Tk.Button(
    button_frame, text="Add Item", command=add_item)
cross_off_button = Tk.Button(
    button_frame, text="Cross off Item", command=done_item)
uncross_button = Tk.Button(
    button_frame, text="Uncross All", command=waiting_item)
delete_crossed_button = Tk.Button(
    button_frame, text="Delete Crossed Items", command=delete_crossed)

delete_button.grid(row=0, column=0)
add_button.grid(row=0, column=1, padx=20)
cross_off_button.grid(row=0, column=2)
uncross_button.grid(row=0, column=3, padx=20)
delete_crossed_button.grid(row=0, column=4)

root.mainloop()
