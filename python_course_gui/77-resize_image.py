from tkinter import *
from PIL import ImageTk, Image

root = Tk()
root.title("Resize image")
root.iconbitmap('codemy.ico')
root.geometry("800x500")

# Image size: 500x375
# Open Image
my_pic = Image.open('images/aspen.png')

# Resize Image
resized = my_pic.resize((300, 225), Image.ANTIALIAS)

# Add image
new_pic = ImageTk.PhotoImage(resized)

my_label = Label(root, image=new_pic)
my_label.pack(pady=20)

root.mainloop()