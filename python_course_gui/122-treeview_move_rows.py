from tkinter import *
from tkinter import ttk, messagebox

root = Tk()
root.title("Treeview")
root.iconbitmap("codemy.ico")
root.geometry("500x800")

# Add some style
style = ttk.Style()

# Pick a Theme
style.theme_use('default')
# style.theme_use('alt')
# style.theme_use('clam')
# style.theme_use('vista')
# style.theme_use('classic')
# style.theme_use('xpnative')
# style.theme_use('winnative')
# ('winnative', 'clam', 'alt', 'default', 'classic', 'vista', 'xpnative')

# Configure the treeview colors explicitly

style.configure("Treeview",
                background="#D3D3D3",
                foreground="black",
                rowheight=25,
                fieldbackground="#D3D3D3"
                )

# Change selected color
style.map('Treeview',
          background=[('selected', 'blue')])


# Treeview Frame (for the scrollbar)
tree_frame = Frame(root)
tree_frame.pack(pady=20)

# Treeview Scrollbar
tree_scrollbar = Scrollbar(tree_frame)
tree_scrollbar.pack(side=RIGHT, fill=Y)

# Create Treeview
my_tree = ttk.Treeview(
    tree_frame, yscrollcommand=tree_scrollbar.set, selectmode=EXTENDED)
# selectmode:  If set to extended (the default), multiple items may be selected. If browse, only a single item will be selected at a time. If none, the selection will not be changed.

# Pack to the screen
my_tree.pack()

# Coinfigure the scrollbar
tree_scrollbar.config(command=my_tree.yview)


# define the columns
my_tree['columns'] = ("Name", "ID", "Favorite Pizza")

# Format the columns
# width > 00 => show phantom column
my_tree.column("#0", width=120, minwidth=25)
my_tree.column("Name", anchor=W, width=120)
my_tree.column("ID", anchor=CENTER, width=80)
my_tree.column("Favorite Pizza", anchor=W, width=150)

# no phantom column
my_tree.column("#0", width=0, stretch=NO)

# Create Headings
my_tree.heading("#0", text="Label", anchor=W)

my_tree.heading("Name", text="Name", anchor=W)
my_tree.heading("ID", text="Id", anchor=CENTER)
my_tree.heading("Favorite Pizza", text="Favorite Pizza", anchor=W)

# remove phantom
my_tree.heading("#0", text="", anchor=W)

# Create striped row tags
my_tree.tag_configure('oddrow', background="white")
my_tree.tag_configure('evenrow', background="lightblue")

# Add Data
data = [
    ["John", 1, "Pepperoni"],
    ["Mary", 2, "Cheese"],
    ["Tim", 3, "Mushroom"],
    ["Erin", 4, "Ham"],
    ["Bob", 5, "Onion"],
    ["Steve", 6, "Peppers"],
    ["Tina", 7, "Cheese"],
    ["Mark", 8, "Supreme"],
    ["Ruth", 9, "Vegan"],
    ["John", 1, "Pepperoni"],
    ["Mary", 2, "Cheese"],
    ["Tim", 3, "Mushroom"],
    ["Erin", 4, "Ham"],
    ["Bob", 5, "Onion"],
    ["Steve", 6, "Peppers"],
    ["Tina", 7, "Cheese"],
    ["Mark", 8, "Supreme"],
    ["Ruth", 9, "Vegan"],
    ["John", 1, "Pepperoni"],
    ["Mary", 2, "Cheese"],
    ["Tim", 3, "Mushroom"],
    ["Erin", 4, "Ham"],
    ["Bob", 5, "Onion"],
    ["Steve", 6, "Peppers"],
    ["Tina", 7, "Cheese"],
    ["Mark", 8, "Supreme"],
    ["Ruth", 9, "Vegan"],
    ["John", 1, "Pepperoni"],
    ["Mary", 2, "Cheese"],
    ["Tim", 3, "Mushroom"],
    ["Erin", 4, "Ham"],
    ["Bob", 5, "Onion"],
    ["Steve", 6, "Peppers"],
    ["Tina", 7, "Cheese"],
    ["Mark", 8, "Supreme"],
    ["Ruth", 9, "Vegan"],
]

count = 0
for record in data:
    if count % 2 == 0:
        tags = ('evenrow',)
    else:
        tags = ('oddrow',)

    my_tree.insert(parent="", index=END, iid=count, text="", values=(
        record[0], record[1], record[2]), tags=tags)
    count += 1


# Add Data explicitely row by row
""" my_tree.insert(parent="", index='end', iid=0, text="",
               values=("John", 1, "Pepperoni",))
my_tree.insert(parent="", index='end', iid=1,
               text="", values=("Mary", 2, "Cheese"))
my_tree.insert(parent="", index='end', iid=2,
               3text="", values=("Tina", 3, "Ham"))
my_tree.insert(parent="", index='end', iid=3,
               text="", values=("Bob", 4, "Supreme"))
my_tree.insert(parent="", index='end', iid=4,
               text="", values=("Erin", 5, "Cheese",))
my_tree.insert(parent="", index='end', iid=5,
               text="", values=("Wes", 6, "Onion",)) """

# Add Data with phantom column
""" my_tree.insert(parent="", index='end', iid=0, text="Parent",
               values=("John", 1, "Pepperoni",))
my_tree.insert(parent="", index='end', iid=1,
               text="Parent", values=("Mary", 2, "Cheese"))
my_tree.insert(parent="", index='end', iid=2,
               text="Parent", values=("Tina", 3, "Ham"))
my_tree.insert(parent="", index='end', iid=3,
               text="Parent", values=("Bob", 4, "Supreme"))
my_tree.insert(parent="", index='end', iid=4,
               text="Parent", values=("Erin", 5, "Cheese",))
my_tree.insert(parent="", index='end', iid=5,
               text="Parent", values=("Wes", 6, "Onion",))
# Child
my_tree.insert(parent="", index='end', iid=6, text="Child",
               values=("Steve", "1.2", "Peppers"))
my_tree.move('6', '0', '0') """


# Frame for the boxes
add_frame = Frame(root)
add_frame.pack(pady=20)

# Label for the bowes
name_label = Label(add_frame, text="Name")
name_label.grid(row=0, column=0)
id_label = Label(add_frame, text="ID")
id_label.grid(row=0, column=1)
topping_label = Label(add_frame, text="Topping")
topping_label.grid(row=0, column=2)

# Entry bowes
name_box = Entry(add_frame)
name_box.grid(row=1, column=0)
id_box = Entry(add_frame)
id_box.grid(row=1, column=1)
topping_box = Entry(add_frame)
topping_box.grid(row=1, column=2)

# Actions


def add_record():
    """ Add a record in the current treeview """

    if id_box.get() == "" or name_box.get() == "" or topping_box.get() == "":
        messagebox.showerror('Treeview', 'All Fields are required')
        return

    global count

    # insert into treeview
    try:
        id = int(id_box.get())
    except ValueError as e:
        messagebox.showerror('Treeview', str(e))

    if count % 2 == 0:
        tags = ('evenrow',)
    else:
        tags = ('oddrow',)

    # my_tree.insert(parent="", index=END, iid=count, text="", values=record)
    my_tree.insert(parent="", index=END, iid=count, text="", values=(
        name_box.get(), id, topping_box.get()), tags=tags)

    count += 1

    # empty the box contents
    name_box.delete(0, END)
    id_box.delete(0, END)
    topping_box.delete(0, END)


def remove_all_records():
    """ remove all records from the treeview (empty treeview) """

    for record in my_tree.get_children():
        my_tree.delete(record)


def remove_record():
    """ remove a selected record from the treeview """

    selected_record = my_tree.selection()[0]
    my_tree.delete(selected_record)


def remove_many_records():
    """ remove selected records from the treeview """

    for selected_record in my_tree.selection():
        my_tree.delete(selected_record)


def clear_entry_boxes():
    """ Clear entry boxes (remove all content) """
    name_box.delete(0, END)
    id_box.delete(0, END)
    topping_box.delete(0, END)


def select_record():
    """ Select a record """

    # Clear entry boxes
    clear_entry_boxes()

    # Select a record
    # Grab record number
    selected = my_tree.focus()

    # Grab record values
    values = my_tree.item(selected, 'values')

    # print(values)
    name_box.insert(0, values[0])
    id_box.insert(0, values[1])
    topping_box.insert(0, values[2])

    # Prevent changing while sediting
    my_tree.config(selectmode=NONE)

    pass


def update_record():
    """ Save updated record """

    # Grab record number
    selected = my_tree.focus()

    # Save new data
    my_tree.item(selected, text="", values=(
        name_box.get(), id_box.get(), topping_box.get()))

    # Retore selection mode after save
    my_tree.config(selectmode=EXTENDED)

    clear_entry_boxes()


def clicker(e):
    """ action on event binding """
    select_record()


def up():
    """ Move Row(s) Up """

    rows = my_tree.selection()
    for row in rows:
        my_tree.move(row, my_tree.parent(row), my_tree.index(row)-1)


def down():
    """ Move Row(s) Down """

    rows = my_tree.selection()
    for row in reversed(rows):
        my_tree.move(row, my_tree.parent(row), my_tree.index(row)+1)


# Buttons

# Move selection up
move_up_button = Button(root, text="Move Up", command=up)
move_up_button.pack(pady=10)

# Move selection down
move_down_button = Button(root, text="Move Down", command=down)
move_down_button.pack(pady=10)

# Select a Record
select_button = Button(root, text="Select Record", command=select_record)
select_button.pack(pady=15)

# Update a record
update_button = Button(root, text="Seve Record", command=update_record)
update_button.pack(pady=10)

# Add a record
add_record = Button(root, text="Add Record", command=add_record)
add_record.pack(pady=20)

# Remove All records
remove_all_record = Button(
    root, text="Remove All Records", command=remove_all_records)
remove_all_record.pack(pady=10)

# Remove Selected record
remove_record = Button(
    root, text="Remove One Selected", command=remove_record)
remove_record.pack(pady=10)

# Remove many Selected records
remove_many_records = Button(
    root, text="Remove Many Selected", command=remove_many_records)
remove_many_records.pack(pady=10)

##########
# Bindings
##########
# double-click
my_tree.bind("<Double-1>", clicker)
# single  click: on button release
# my_tree.bind("<ButtonRelease-1>", clicker)

# start event loop
root.mainloop()
