from tkinter import Frame, Button, Tk, messagebox

root = Tk()
root.title('Classes')
root.iconbitmap('codemy.ico')
root.geometry("400x400")

class SimonApp():
    
    def __init__(self, master):
       my_frame = Frame(master)
       my_frame.pack()

       self.my_button = Button(master, text="Click Me!", command=self.clicker)
       self.my_button.pack(pady=20)

    def clicker(self):
        messagebox.showinfo('Hi', 'Look at you...\nYou clicked a button!')

SimonApp(root)

root.mainloop()