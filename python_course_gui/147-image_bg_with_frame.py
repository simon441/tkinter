import tkinter as Tk

root = Tk.Tk()
root.title("Set Image as Background")
root.iconbitmap("codemy.ico")
root.geometry('800x500')

# Define image
bg = Tk.PhotoImage(file="images/mario.png")


BG_COLOR = '#6b88fe'

# Create a label
my_label = Tk.Label(root, image=bg)
my_label.place(x=0, y=0, relwidth=1, relheight=1)

# Add something on top of the image
my_text = Tk.Label(root, text="Welcome!", font=(
    "Helvetica", 50), fg="white", bg=BG_COLOR)
my_text.pack(pady=50)


# Add a frame
my_frame = Tk.Frame(root, bg=BG_COLOR)
my_frame.pack(pady=20)

# Add some buttons
my_button1 = Tk.Button(my_frame, text="Exit")
my_button1.grid(row=0, column=0, padx=10)

my_button2 = Tk.Button(my_frame, text="Start")
my_button2.grid(row=0, column=1, padx=10)

my_button3 = Tk.Button(my_frame, text="Reset")
my_button3.grid(row=0, column=2, padx=10)


root.mainloop()
