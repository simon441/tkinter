import tkinter as Tk
from tkinter import PhotoImage, filedialog, ttk
from pathlib import Path
from random import randint


root = Tk.Tk()
root.title("Rock, Paper, Scissors")
root.iconbitmap("codemy.ico")
root.geometry('500x600')

# Change background color to white
root.config(bg="white")

WON_TEXT = ""
TIE_TEXT = "It's A Tie! Spin Again..."
LOST_TEXT = ""

rock = PhotoImage(file=Path.joinpath(
    Path.cwd(), Path('images/rps/rock.png')))
paper = PhotoImage(file=Path.joinpath(
    Path.cwd(), Path('images/rps/paper.png')))
scissors = PhotoImage(file=Path.joinpath(
    Path.cwd(), Path('images/rps/scissors.png')))

# Add Images to a list
image_list = [rock, paper, scissors]

# Pick random number between 0 and 2
picked_number = randint(0, 2)

# Throw up an image when the program starts
my_image = Tk.Label(root, image=image_list[picked_number], bd=0)
my_image.pack(pady=20)


def spin():
    """ Get random image between the three in the list """
    # Pick random number
    picked_number = randint(0, 2)
    # Show image
    my_image.config(image=image_list[picked_number], bd=0)

    # 0 = Rock
    # 1 = Paper
    # 2 = Scissors

    # Convert Dropdown choice to a number
    if user_choice.get() == 'Rock':
        user_choice_value = 0
    elif user_choice.get() == 'Paper':
        user_choice_value = 1
    elif user_choice.get() == 'Scissors':
        user_choice_value = 2

    # Determine if the user won or not
    if user_choice_value == 0:  # Rock
        if picked_number == 0:  # Rock
            win_lose_label.config(text=TIE_TEXT)
        elif picked_number == 1:  # Paper
            win_lose_label.config(text="Paper Covers Rock! You Lose...")
        elif picked_number == 2:  # Scissors
            win_lose_label.config(text="Rock Smashes Scissors! You Win!!!")

    if user_choice_value == 1:  # Paper
        if picked_number == 0:  # Rock
            win_lose_label.config(text="Paper Covers Rock! You Win!!!")
        elif picked_number == 1:  # Paper
            win_lose_label.config(text=TIE_TEXT)
        elif picked_number == 2:  # Scissors
            win_lose_label.config(text="Rock Smashes Scissors! You Lose...")

    if user_choice_value == 2:  # Scissors
        if picked_number == 0:  # Rock
            win_lose_label.config(text="Rock Smashes Scissors! You Lose...")
        elif picked_number == 1:  # Paper
            win_lose_label.config(text="Scissors Cuts Paper! You Win!!!")
        elif picked_number == 2:  # Scissors
            win_lose_label.config(text=TIE_TEXT)


# Make our choice
user_choice = ttk.Combobox(root, value=("Rock", "Paper", "Scissors"))
user_choice.current(0)
user_choice.pack(pady=20)

# Spin button to display random image every time
spin_button = Tk.Button(root, text="spin", command=spin)
spin_button.pack(pady=10)


# Label for showing if you won or not
win_lose_label = Tk.Label(root, text="", font=("Helvetica", 18), bg="white")
win_lose_label.pack(pady=50)

root.mainloop()
