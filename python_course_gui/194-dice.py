import random
from os import path
from tkinter import Button, Frame, Label, Tk

root = Tk()
root.title('Rool the Dice')
root.iconbitmap(path.join(path.abspath(__file__), '..', 'codemy.ico'))
root.geometry('500x500')

def get_number(dice_number: str) -> int:
    """ Get the dice number """
    if dice_number == '\u2680':
        return 1
    elif dice_number == '\u2681':
        return 2
    elif dice_number == '\u2682':
        return 3
    elif dice_number == '\u2683':
        return 4
    elif dice_number ==  '\u2684':
        return 5
    elif dice_number == '\u2685':
        return 6
    return 0


def roll_dice():
    """ Roll the Dice """
    # Roll dice at random
    dice1 = random.choice(my_dice)
    dice2 = random.choice(my_dice)

    # Determine dice number
    subdice1 = get_number(dice1)
    subdice2 = get_number(dice2)

    # Update the labels
    dice_label1.config(text=dice1)
    dice_label2.config(text=dice2)
    
    # Update sub labels
    sub_dice_label1.config(text=subdice1)
    sub_dice_label2.config(text=subdice2)

    # Update Total label
    total = subdice1 + subdice2
    total_label.config(text=f'You Rolled: {total}')



# Create a Dice List
my_dice=['\u2680', '\u2681', '\u2682', '\u2683', '\u2684', '\u2685', ]

# Create a Frame
my_frame = Frame(root)
my_frame.pack(pady=20)

# Create Dice Labels
dice_label1 = Label(my_frame, text='', font=('Helvetica', 100), fg='#333')
dice_label1.grid(row=0, column=0, padx=5)
sub_dice_label1 = Label(my_frame, text='', font=('Helvetica', 16))
sub_dice_label1.grid(row=1, column=0, padx=5)

dice_label2 = Label(my_frame, text='', font=('Helvetica', 100), fg='#333')
dice_label2.grid(row=0, column=1, padx=5)
sub_dice_label2 = Label(my_frame, text='', font=('Helvetica', 16))
sub_dice_label2.grid(row=1, column=1, padx=5)

# Create Roll Button
roll_button = Button(root, text='Roll Dice', command=roll_dice, font=('Helvetica', 24))
roll_button.pack(pady=20)

# Totals label to output the score of the two dices
total_label = Label(root, text='', font=('Helvetica', 24), fg='grey')
total_label.pack(pady=40)

# Roll the dice at program start
roll_dice()

root.mainloop()
