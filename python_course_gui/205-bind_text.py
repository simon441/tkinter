import tkinter
from tkinter import Label, Text, Tk, ttk
import pathlib
from tkinter.constants import END

root = Tk()
root.title('Bind Text')
root.iconbitmap(pathlib.Path.joinpath(
    pathlib.Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('500x500')


def labeler(e):
    """Add text from textbox to label"""
    my_label.config(text=my_textbox.get(1.0, END + "-1c") + e.char)


my_textbox = Text(root, width=50, height=20,
                  font=('Helvatica', 12), wrap='word')
my_textbox.pack(pady=20)

my_label = Label(root, text='Type stuff above', font=('Helvetica', 14))
my_label.pack(pady=20)

# Bind textbox
my_textbox.bind('<Key>', labeler)
root.mainloop()
