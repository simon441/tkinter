from tkinter import *
from tkinter import filedialog, font, messagebox, TclError
import os
from pathlib import Path
import re

DEFAULT_TITLE = "Simon Cateau's TextPad!"
opened_file_name = False
global selected_text
selected_text = False

root = Tk()
root.title(DEFAULT_TITLE)
root.iconbitmap("codemy.ico")
root.geometry("1200x660")

STATUS_BAR_SPACING = "         "
FILE_TYPES = [("Text Files", "*.txt"), ("HTML Files", "*.html *.htm"), ("Python Files", "*.py *.pyc")]
FILE_TYPES_SELECT = list(FILE_TYPES)
# FILE_TYPES_SELECT_SAVE = list(FILE_TYPES)
FILE_TYPES_SELECT.append(("All Files", "*.*"))

############
# Functions 
############
def get_file_name():
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else: 
        file_name = os.path.basename(status_bar['text'].strip())
    return file_name


def get_selected_text():
    return my_textbox.selection_get()

def new_file(e = None):
    """ Create a new file """

    # Delete previous text
    my_textbox.delete(1.0, END)
    root.title('New File - TextPad!')
    status_bar.config(text="New File" + STATUS_BAR_SPACING)

def open_file(e = None):
    """ Open a file """
    global opened_file_name

    # Delete previous text
    my_textbox.delete(1.0, END)

    #  get file
    file_path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Open File", filetypes=FILE_TYPES_SELECT)  # print(file_path)
    file_name = os.path.basename(file_path)

    if file_name:
        opened_file_name = file_name
    
    # Update status bars
    root.title(f'{file_name} - TextPad!')
    status_bar.config(text=file_path + STATUS_BAR_SPACING)

    if not file_path or not os.path.exists(file_path):
        return
    try:
        # Open the file
        text_file = open(file_path, 'r')
        data = text_file.read()
        # Add contents to textbox
        my_textbox.insert(END, data)
        # Close the opened file
        text_file.close()
    except FileNotFoundError as e:
        print(e)


def save_file(e = None):
    """ Save the file """
    
    filename = get_file_name()
    global opened_file_name
    if filename:
        # Save the file
        text_file = open(filename, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()
        # put status update or popup code
        messagebox.showinfo("File", f'File {filename} saved')
        status_bar.config(text=f"Saved: {opened_file_name} {STATUS_BAR_SPACING}")
    else:
        save_as_file()


def save_as_file(e = None):
    """ Save the file with a chosen filename """

    ext = ""
    if root.title() == DEFAULT_TITLE or root.title().startswith('New File - '):
        file_name = ''
    else: 
        file_name = os.path.basename(status_bar['text'].strip())
        ext = Path(status_bar['text'].strip()).suffix
        if not ext:
            ext = ".*"

    file_path = filedialog.asksaveasfilename(initialfile=file_name, initialdir=os.getcwd(), filetypes=FILE_TYPES_SELECT, defaultextension=ext )
    if file_path:
        file_name = os.path.basename(file_path)

        # Update status bars
        root.title(f'{file_name} - TextPad!')
        status_bar.config(text=f"Saved: {file_path} {STATUS_BAR_SPACING}")

        # Save the file
        text_file = open(file_path, 'w')
        text_file.write(my_textbox.get(1.0, END))
        # Close the file
        text_file.close()



def cut_text(e):
    """" Cut text """
    # get selected text from textbox
    global selected_text

    # Check to see if keyboard shortcut was used
    if e:
        selected_text = root.clipboard_get()
    else:
        try:
            if my_textbox.selection_get():
                selected_text = my_textbox.selection_get()
                # delete selected text from textbox
                my_textbox.delete("sel.first", "sel.last")

                # Clear the clipboard then append
                root.clipboard_clear()
                root.clipboard_append(selected_text)
        except TclError as e:
            print(e)

def copy_text(e):
    """ Copy text """
    global selected_text

    if e: # use keybord shortcut
        selected_text = root.clipboard_get()
    # get selected text from textbox
    else:
        try:
            if my_textbox.selection_get():
                selected_text = my_textbox.selection_get()

                # Clear the clipboard then append
                root.clipboard_clear()
                root.clipboard_append(selected_text)
        except TclError as e:
            print(e)


def paste_text(e):
    """ Paste text """
    global selected_text

    # Check to see if the keyboard shortcut is used
    if e:
        selected_text = root.clipboard_get()
    else:
        if selected_text:
            position = my_textbox.index(INSERT)
            my_textbox.insert(position, selected_text)


def undo_action(e):
    try:
        my_textbox.edit_undo()
        edit_menu.entryconfig(1, state=NORMAL)
    except TclError as e:
        print(str(e))
        edit_menu.entryconfig(0, state=DISABLED)
        edit_menu.entryconfig(1, state=NORMAL)
    
def redo_action(e):
    try:
        my_textbox.edit_redo()
        edit_menu.entryconfig(0, state=NORMAL)
    except TclError as e:
        print(str(e))
        edit_menu.entryconfig(0, state=NORMAL)
        edit_menu.entryconfig(1, state=DISABLED)


def select_all(e = None):
    my_textbox.tag_add(SEL, 1.0, END)

def deselect_all(e = None):
    my_textbox.tag_remove(SEL, 1.0, END)



def get_search(search):
    pass

def search_me(search, ignorecase = False):
    # if not search:
    #     return
    print(search)
    print(my_textbox.get(1.0, END))
    data_to_search = my_textbox.get(1.0, END)
    count_var = StringVar()
    position = my_textbox.search(search, "1.0", count=count_var, nocase=ignorecase)
    # position = my_textbox.search(search, 1.0, stopindex="end", count=count_var)
    print('index:', position, type(position))
    my_textbox.tag_add("search", position, "{}+{}c".format(position, count_var.get()))
    
    my_textbox.tag_configure("search",background="green", foreground="white")
    # self.text.tag_configure("search", background="skyblue", foreground="red")
    return True


def search_me_all(search, ignorecase = False):
    
    position = 1.0
    print("ignorecase", ignorecase)
    while True:
        count_var = StringVar()
        position = my_textbox.search(search, position, stopindex=END, count=count_var, nocase=ignorecase)
        # position = my_textbox.search(search, 1.0, stopindex="end", count=count_var)
        print('index:', position, type(position))
        if not position:
            break
        # if (position == ""):
        #     break

        print("#", count_var.get(), '~#')
        lastindex =  "{}+{}c".format(position, len(search))

        my_textbox.tag_add("search", position, lastindex)
        
        my_textbox.tag_configure("search",background="green", foreground="white")

        position = lastindex

def find_text(e = None):
    print("find_text")
    find_window = Tk()
    find_window.title("Find")
    find_window.geometry('500x100')

    top_frame = Frame(find_window)
    top_frame.pack()
    find_search_box = Entry(top_frame, width=50)
    find_search_box.grid(row=0, column=1)
    ignorecase = BooleanVar()
    find_ignorecase = Checkbutton(top_frame, text="Ignore case", variable=ignorecase)
    find_ignorecase.grid(row=1, column=1)
    find_button = Button(top_frame, text="Find", command=lambda: search_me(find_search_box.get(), ignorecase.get()))
    find_button.grid(row=0, column=2)

    results_frame = Frame(find_window)
    results_frame.pack(pady=20)

def find_all_text(e = None):
    print("find_all_text")
    print("find_text")
    find_window = Tk()
    find_window.title("Find")
    find_window.geometry('500x100')

    top_frame = Frame(find_window)
    top_frame.pack()
    find_search_box = Entry(top_frame, width=50)
    find_search_box.grid(row=0, column=1)
    find_button = Button(top_frame, text="Find", command=lambda: search_me_all(find_search_box.get(), ignorecase=ignorecase.get()))
    find_button.grid(row=0, column=2)
    ignorecase = BooleanVar()
    ignorecase.set(True)
    find_ignorecase = Checkbutton(top_frame, text="Ignore case", variable=ignorecase, onvalue=True, offvalue=False)
    find_ignorecase.deselect()
    find_ignorecase.grid(row=1, column=1)

    results_frame = Frame(find_window)
    results_frame.pack(pady=20)

def find_and_replace_text(e = None):
    print("find_and_replace_text")
    pass


def quit_app(e = None):
    msg = messagebox.askokcancel('TextPad', 'Do your want to quit TextPad?')
    if msg:
        root.destroy()
        
        root.quit()

############
# GUI
############

# Main Frame
main_frame = Frame(root)
main_frame.pack(pady=5)

# Scrollbar for the Text box
text_scroll = Scrollbar(main_frame)
text_scroll.pack(side=RIGHT, fill=Y)

# Text box
my_textbox = Text(main_frame, width=97, height=25, font=("Helvetica", 16), selectbackground="yellow", selectforeground="black", undo=True, yscrollcommand=text_scroll.set)
my_textbox.pack()

# Configure the scrollbar
text_scroll.config(command=my_textbox.yview)

########### 
# Menu bar
###########
main_menu = Menu(root)
root.config(menu=main_menu)

# Add File Menu
file_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="File", menu=file_menu)
file_menu.add_command(label="New", accelerator="Ctrl+N", command=new_file)
file_menu.add_command(label="Open", accelerator="Ctrl+O", command=open_file)
file_menu.add_command(label="Save", accelerator="Ctrl+S", command=save_file)
file_menu.add_command(label="Save As...", command=save_as_file)
# file_menu.add_command(label="Save A Copy...")
file_menu.add_separator()
file_menu.add_command(label="Exit", accelerator="Ctrl+Q", command=quit_app)

# Add Edit Menu
edit_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Edit", menu=edit_menu)
# edit_menu.add_command(label="Undo    Ctrl+Z", accelerator="", command=lambda: undo_action(False))
edit_menu.add_command(label="Undo", accelerator="Ctrl+Z", underline=1, command=lambda: undo_action(False))
edit_menu.add_command(label="Redo", accelerator="Ctrl+Y", underline=1, command=lambda: redo_action(False))
# edit_menu.add_command(label="Redo    Ctrl+Y", command=lambda: redo_action(False))
edit_menu.add_separator()
edit_menu.add_command(label="Cut", accelerator="Ctrl+X", underline=1, command=lambda: cut_text(False))
edit_menu.add_command(label="Copy", accelerator="Ctrl+C", underline=1, command=lambda: copy_text(False))
edit_menu.add_command(label="Paste", accelerator="Ctrl+V", underline=1, command=lambda: paste_text(False))
edit_menu.add_separator()
edit_menu.add_command(label="Select All", accelerator="Ctrl+A", underline=1, command=lambda: select_all(False))
edit_menu.add_command(label="Deselect All", accelerator="", underline=1, command=lambda: deselect_all(False))


# Add Find Menu
find_menu = Menu(main_menu, tearoff=False)
main_menu.add_cascade(label="Find", menu=find_menu)
find_menu.add_command(label="Find", accelerator="Ctrl+F", underline=0, command=lambda: find_text())
find_menu.add_command(label="Find All", accelerator="Ctrl+Shift+F", underline=0, command=lambda: find_all_text())
find_menu.add_separator()
find_menu.add_command(label="Find And Replace", accelerator="Ctrl+H", underline=0, command=lambda: find_and_replace_text())



##########
# Status bar to bottom o fapp
status_bar = Label(root, text='Ready' + STATUS_BAR_SPACING, anchor=E)
status_bar.pack(fill=X, side=BOTTOM, ipady=5)


#########
# Bindings

# File bindings
root.bind('<Control-KeyPress-n>', new_file)
root.bind('<Control-KeyPress-o>', open_file)
root.bind('<Control-KeyPress-s>', save_file)
root.bind('<Control-KeyPress-q>', quit_app)

# Edit bindings
root.bind('<Control-Key-z>', undo_action)
root.bind('<Control-Key-y>', redo_action)
root.bind('<Control-Key-x>', cut_text)
root.bind('<Control-Key-c>', copy_text)
root.bind('<Control-Key-v>', paste_text)
root.bind('<Control-Key-a>', select_all)


# Find bindings
root.bind('<Control-Key-f>', find_menu)
root.bind('<Control-Key-F>', find_all_text)
root.bind('<Control-Key-h>', find_and_replace_text)

#################################
# Start GUI and Launch Event Loop
#################################

root.protocol("WM_DELETE_WINDOW", quit_app)

root.mainloop()