from tkinter import *

root = Tk()
root.title("Alpha method")
root.iconbitmap("codemy.ico")
root.geometry('500x550')

COLOR = '#60b26c'
# ROOT COLOR
ROOT_COLOR = root['bg']

# transparency levels go from 0.1 to 1.0
root.wm_attributes('-transparentcolor', COLOR)
root.wm_attributes('-transparentcolor', ROOT_COLOR)

my_frame = Frame(root, width=200, height=200, bg=COLOR)
my_frame.pack(pady=20, ipady=20, ipadx=20)

my_label = Label(my_frame, text="Hello World!",
                 font=('Helvetica', 20), bg=COLOR, fg="white")
my_label.pack(pady=20)


root.mainloop()
