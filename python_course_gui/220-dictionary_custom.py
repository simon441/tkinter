from pathlib import Path
from tkinter import END, WORD, Text
from typing import Dict, List

import customtkinter
from PyDictionary import PyDictionary

customtkinter.set_appearance_mode("dark")
customtkinter.set_default_color_theme('dark-blue')

root = customtkinter.CTk()
root.title('English Dictionary')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('620x470')


def lookup():
    """lookup Search for entered text
    """
    # Clear the text box
    my_textbox.delete(1.0, END)

    # Lookup a word
    dictionary = PyDictionary()
    definition: Dict[str, List[str]] = dictionary.meaning(
        my_entry.get().strip())

    # Add definition to text box
    # my_textbox.insert(END, str(definition))

    # Find keys and values in definition
    for key, values in definition.items():
        # Put the key header in the text box
        my_textbox.insert(END, key + "\n\n")

        for value in values:
            my_textbox.insert(END, f"- {value}\n\n")


my_labelframe = customtkinter.CTkFrame(root, corner_radius=10)
my_labelframe.pack(pady=20)

my_entry = customtkinter.CTkEntry(
    my_labelframe, width=400, height=40, border_width=1, text_font=("Helvetica", 12), placeholder_text="Enter a Word", text_color='silver')
my_entry.grid(row=0, column=0, padx=10, pady=10)

my_button = customtkinter.CTkButton(
    my_labelframe, text="Lookup", command=lookup, text_font=("Helvetica", 12))
my_button.grid(row=0, column=1, padx=10)

text_frame = customtkinter.CTkFrame(root, corner_radius=10)
text_frame.pack(pady=10, padx=10)

my_textbox = Text(text_frame, height=20, width=67,
                  wrap=WORD, bd=0, bg="#292929", fg="silver")
my_textbox.pack(pady=10)


if __name__ == '__main__':
    root.mainloop()
