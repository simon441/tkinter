from os import path
from tkinter import Button, Entry, Label, messagebox, TclError, Tk
from tkinter.constants import END

from pyshorteners import Shortener
from pyshorteners.exceptions import ExpandingErrorException, ShorteningErrorException

root = Tk()
root.title('Link Shortener')
root.iconbitmap(path.join(path.abspath(__file__), '..', 'codemy.ico'))
root.geometry('500x500')

# Get the background color and set it if error getting it
try:
    BG_COLOR = root.cget('bg')
except TclError:
    BG_COLOR = '#f0f0f0'
    root.config(bg=BG_COLOR)


def shorten():
    """Shorten a link"""
    # Empty shorty if not empty
    if shorty.get():
        shorty.delete(0, END)

    if my_entry.get():
        # Get the shortener
        s = Shortener()
        # Convert to tinyurl
        try:
            url = s.tinyurl.short(my_entry.get().strip())
        except ShorteningErrorException as e:
            messagebox.showerror(f'Address {my_entry.get()} not found\n {e}')
            print('short', e)
            return
        # Output to screen
        shorty.insert(END, url)

        # Rever the URL
        try:
            reversed_url = s.tinyurl.expand(url)
            print(f'Reversed URL: {reversed_url}')
        except Exception as e:
            print(e)


my_label = Label(root, text='Enter Link to Shorten', font=('Helvetica', 34))
my_label.pack(pady=20)

my_entry = Entry(root, font=('Helvetica', 24))
my_entry.pack(pady=20)

my_button = Button(root, text='Shorten Link',
                   command=shorten, font=('Helvetica', 24))
my_button.pack(pady=20)

shorty_label = Label(root, text='Shortened Link', font=('Helvetica', 14))
shorty_label.pack(pady=50)

shorty = Entry(root, font=('Helvatica', 22), justify='center',
               width=30, bd=0, bg=BG_COLOR)
shorty.pack(pady=10)

root.mainloop()
