from tkinter import *

root = Tk()
root.title("Canvas, omve shapers with keyboard keys")
root.iconbitmap('codemy.ico')
root.geometry("800x600")

w = 600
h = 400
x = w//2
y = h//2
object_diameter = 20

def left(event):
    x = -10
    y = 0
    my_canvas.move(my_object, x, y)

def right(event):
    x = +10
    y = 0
    my_canvas.move(my_object, x, y)

def up(event):
    x = 0
    y = -10
    my_canvas.move(my_object, x, y)

def down(event):
    x = 0
    y = +10
    my_canvas.move(my_object, x, y)

def pressing(event):
    x = 0
    y = 0
    if event.char == 'w': x = -10
    if event.char == 'e': x = 10
    if event.char == 'n': y = -10
    if event.char == 's': y = 10
    my_canvas.move(my_object, x, y)

# Creat ethe Canvas
my_canvas = Canvas(root, width=w, height=h, bg="#fcfcfc")
my_canvas.pack(pady=20)

# Create the circle
# my_object = my_canvas.create_oval(x-object_diameter/2, y-object_diameter/2, x+object_diameter/2, y+object_diameter/2)
# Create a rectangle
my_object = my_canvas.create_rectangle(x-object_diameter/2, y-object_diameter/2, x+object_diameter/2, y+object_diameter/2)

# Bind the keys to the events
# define specific keys (the arrow keys)
root.bind("<Left>", left)
root.bind("<Right>", right)
root.bind("<Up>", up)
root.bind("<Down>", down)

# Define only any keyboard key event
root.bind('<Key>', pressing)

root.mainloop()