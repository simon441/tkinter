from tkinter import *
import time
from random import randint
import threading

root = Tk()
root.title('Spinboxes')
root.iconbitmap("codemy.ico")
root.geometry("500x400")

def grab():
    my_label.config(text=my_spin.get())
    
def grab2():
    my_label2.config(text=my_spin2.get())

# with numbers
my_spin = Spinbox(root, from_=0, to_=10, increment=2, font=('Helvetica', 20))
my_spin.pack(pady=20)


my_button = Button(root, text="Submit", command=grab)
my_button.pack(pady=20)


my_label = Label(root, text="")
my_label.pack(pady=20)

# with strings
names = ('John', 'Sally', 'Martha', 'Marty')
my_spin2 = Spinbox(root, values=names, increment=2, font=('Helvetica', 20))
my_spin2.pack(pady=20)

my_button2 = Button(root, text="Submit", command=grab2)
my_button2.pack(pady=20)



my_label2 = Label(root, text="")
my_label2.pack(pady=20)


root.mainloop()