from os import path
import tkinter as tk
from tkinter import Button, Label, Tk
import webbrowser

root = Tk()
root.title('Open Web Browser')
root.iconbitmap(path.join(path.abspath(__file__), '..', 'codemy.ico'))
root.geometry('500x500')

DEFAULT_WEBPAGE = 'http://localhost'


def open_browser(e=None):
    # Open web page in default browser
    # webbrowser.open_new(DEFAULT_WEBPAGE)

    # Open specific browser
    webbrowser.get(
        'C:/Opera GX/opera.exe %s').open_new(DEFAULT_WEBPAGE)
    # webbrowser.get('windows-default').open_new(DEFAULT_WEBPAGE)


# Create button
my_button = Button(root, text='Open Web Browser!', font=(
    'Helvetica', 24), command=open_browser)
my_button.pack(pady=50)

# Create label
my_label = Label(text='Open Browser', font=('Helvetica', 24), fg='blue')
my_label.pack(pady=20)

# Bind the label
my_label.bind('<Button-1>', open_browser)


root.mainloop()
