from tkinter import Tk, Label, Frame, Menu, BOTH, messagebox, Button, Entry
from tkinter import * 
from PIL import ImageTk, Image
from random import randint
from os import listdir, path


root = Tk()
root.title('Flashcards!')
root.iconbitmap('codemy.ico')
root.geometry("500x600")

state_image = None
STATES_DIR = 'states'

def _state_listing():
    # Create a list of state names
    files_list = listdir(STATES_DIR)
    # print(files_list)
    our_states = [x.replace('.png', '') for x in files_list]
    # print(states)
    return our_states

STATES_LIST_GEOGRAPH = _state_listing()

#############
# FUNCTIONS
#############

def random_state(show_state):
    """ Generate a random state from the state list (from the state image dir) 
        show_state [tKinter.Label]: label to show if the answer is correct or not
    """

    # Generate a random number
    random_state = randint(0, len(STATES_LIST_GEOGRAPH) -1)

    selected_state = STATES_LIST_GEOGRAPH[random_state]
    print('random_state', selected_state)


    state_path = STATES_DIR + '/' + selected_state + '.png'
    # print(state_path)
    if not path.isfile(state_path):
        messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
        return

    # Create ou state images
    global state_image
    state_image = ImageTk.PhotoImage(Image.open(state_path))
    show_state.config(image=state_image, bg="white")
    return selected_state


def state_answer(answer_label, answer_input, valid_answer, show_state):
    """ Verify if answer is correct function 
        answer_label [String]: Label to display text if correct or not
        answer_input [String]: input by the user to be verified
        valid_answer [String]: Valid answer to compare to user input
    """
    # print(answer)
    answer = answer_input.get().replace(' ', '').replace('-', '').lower()
    # print(answer)

    # Determine if the answer is right or wrong
    if answer == valid_answer:
        response = "Correct! " + valid_answer.title()
    else:
        response = "Incorrect! " + valid_answer.title()
    
    answer_label.config(text=response)

    # Refreash to next card
    random_state(show_state)

    answer_input.delete(0, END)


def states():
    """ State Flashcard Function """
    # hide all frames
    hide_all_frames()
    state_frame.pack(fill=BOTH, expand=1)
    # my_label = Label(state_frame, text="States").pack()


    # # Generate a random number
    # random_state = randint(0, len(STATES_LIST_GEOGRAPH) -1)

    # selected_state = STATES_LIST_GEOGRAPH[random_state]



    # state_path = STATES_DIR + '/' + selected_state + '.png'
    # # print(state_path)
    # if not path.isfile(state_path):
    #     messagebox.showerror('State Image', "An error has occurred.\nThe file for the current state could not be retrieved.")
    #     return

    # # Create ou state images
    # global state_image
    # state_image = ImageTk.PhotoImage(Image.open(state_path))
    # global show_state
    show_state = Label(state_frame)
    show_state.pack(pady=15)

    selected_state = random_state(show_state)

    # Answer imput box
    # global answer_input
    answer_input = Entry(state_frame, font=('Helvetice', 18), bd=2)
    answer_input.pack(pady=15)
     
    # Button to Randomize State Images
    random_state_button = Button(state_frame, text="Pass", command=states)
    random_state_button.pack(pady=10)

    # Label to Tell us if we got the answer right or not
    answer_label = Label(state_frame, text="", font=('Helvetice', 18), bg="white")

    # Button to answer the Question
    answer_button = Button(state_frame, text="Answer", command=lambda: state_answer(answer_label, answer_input, selected_state, show_state))
    answer_button.pack(pady=5)

    answer_label.pack(pady=15)


def state_capitals():
    """ State capitals Flashcard Function """
    # hide all frames
    hide_all_frames()
    state_capitals_frame.pack(fill=BOTH, expand=1)
    my_label = Label(state_capitals_frame, text="Capitals")
    my_label.pack()


def hide_all_frames():
    """ Hide all frames """

    # Loop through all the children in each frame and delete them
    for widget in state_frame.winfo_children():
        widget.destroy()

    for widget in state_capitals_frame.winfo_children():
        widget.destroy()

    # hide frames
    state_frame.pack_forget()
    state_capitals_frame.pack_forget()


############
# MENU
############

# Create our menu
my_menu = Menu(root)
root.config(menu=my_menu)

# Create Geography menu items
states_menu = Menu(my_menu)
my_menu.add_cascade(label="Geography", menu=states_menu)

states_menu.add_command(label="States", command=states)
states_menu.add_command(label="State Capitals", command=state_capitals)
states_menu.add_separator()
states_menu.add_command(label="Exit", command=root.quit)

###########
# Frames
###########

state_frame = Frame(root, width=500, height=500, bg="white")
state_capitals_frame = Frame(root, width=500, height=500)

# Launch GUI
root.mainloop()