from tkinter import *
import time

root = Tk()
root.title("Timers and clocks")
root.iconbitmap('codemy.ico')
root.geometry("600x400")


def clock():
    hour = time.strftime('%H')
    minute = time.strftime('%M')
    second = time.strftime('%S')
    day =  time.strftime('%d')
    day_full_word =  time.strftime('%A')
    month_full_word =  time.strftime('%B')
    month =  time.strftime('%m')
    year =  time.strftime('%Y')
    timeaone =  time.strftime('%Z')
    my_label.config(text=f"{hour}:{minute}:{second}\n{year}/{month}/{day}")
    # my_label.config(text=f"{hour}:{minute}:{second}")
    my_label.after(1000, clock)

    my_label2.config(text=f"{day_full_word} {day} {month_full_word} {year}\n\nTimezone: {timeaone}")
    my_label2.after(1000, clock)

def update():
    my_label.config(text="new text")

#  Hour minute second
my_label = Label(root, text="", font=("Helvetice", 28), fg="green", bg="black")
my_label.pack(pady=20)

# Year month day
my_label2 = Label(root, text="", font=("Helvetice", 14))
my_label2.pack(pady=20)

# Start the clock
clock()


root.mainloop()