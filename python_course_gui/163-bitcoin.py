from tkinter import *
from bs4 import BeautifulSoup
from urllib import request
from datetime import datetime

root = Tk()
root.title("Bitcoin Price Grabber")
root.iconbitmap("codemy.ico")
root.geometry('700x210')
root.config(bg="black")

# Create a Frame
my_frame = Frame(root, bg="black")
my_frame.pack(pady=20)

logo = PhotoImage(file="images/bitcoin.png")

logo_label = Label(my_frame, image=logo, bd=0)
logo_label.grid(row=0, column=0, rowspan=2)

# Add bitcoin prices labe
bit_label = Label(my_frame, text="test", font=(
    "Hehlvetica", 45), bg="black", fg="green")
bit_label.grid(row=0, column=1, padx=20, sticky=S)

# Latest price Up/Down
latest_price = Label(my_frame, text="price", font=(
    "Helvetica", 8), bg="black", fg="grey")
latest_price.grid(row=1, column=1, sticky=N)

UPDATE_FREQUENCY = 30 * 1000

global previous
previous = False


def get_current_time():
    """ Get the formatted current time"""
    # Get current time
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    return current_time


def Update():
    """ Grab the bitcoin price """
    global previous

    # Grab bitcoin price
    page = request.urlopen("https://www.coindesk.com/price/bitcoin").read()
    html = BeautifulSoup(page, "html.parser")
    price_large = html.find(class_="price-large")

    # convert to string so it can be sliced
    price_large_str = str(price_large)
    # Grab a slic ethat contains the price
    price_large_value = price_large_str[54:63]
    print(price_large_value)

    # Update the bitcoin label
    bit_label.config(text=f"${price_large_value}")

    # Update the status bar
    status_bar.config(text=f"Last Updated {get_current_time()}   ")

    # Set timer to 1 minute
    # 1 second = 1000ms
    root.after(UPDATE_FREQUENCY, Update)

    # Determine price change
    # Grab current price
    current_price = price_large_value

    # Remove the comma
    current_price = current_price.replace(",", "")

    if previous:
        if float(previous) > float(current_price):
            latest_price.config(
                text=f"Price Down ${round(float(previous)-float(current_price), 2)}", fg="red")

        elif float(previous) == float(current_price):
            latest_price.config(text="Price Unchanged", fg="grey")
        else:
            latest_price.config(
                text=f"Price Up ${round(float(current_price)-float(previous), 2)}", fg="green")

    else:
        previous = current_price
        latest_price.config(text="Price Unchanged", fg="grey")


# Status bar
status_bar = Label(
    root, text=f"Last Updated {get_current_time()}   ", bd=0, anchor=E, bg="black", fg="grey")
status_bar.pack(fill=X, side=BOTTOM, ipady=2)
# On program start, run Update function
Update()

root.mainloop()
