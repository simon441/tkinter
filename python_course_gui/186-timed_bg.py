from tkinter import *


WIDTH = 1000
HEIGHT = 667

root = Tk()
root.title('Timed Background Images')
root.iconbitmap('codemy.ico')
root.geometry(f"{WIDTH}x{HEIGHT}")


class TimedBgApp():

    def __init__(self, root, *args, **kwargs):
        self.root = root

        self.our_images = [
            PhotoImage(file="images/bg/1.png"),
            PhotoImage(file="images/bg/2.png"),
            PhotoImage(file="images/bg/3.png"),
        ]
        self.count = -1

        self.wait_for_change_duration = 5000

        self.create_widgets()

    def create_widgets(self):
        """ Create the widget to display """
        # Create a Canvas
        self.my_canvas = Canvas(self.root, width=WIDTH,
                                height=HEIGHT, highlightthickness=0)
        self.my_canvas.pack(fill=BOTH, expand=True)

        # Set the canvas image
        self.create_image(0)

    def start(self):
        """ Start the program """
        self.next()
        self.root.mainloop()

    def next(self):
        """ Go to Next image """
        # if self.count == len(self.our_images) - 1:
        #     self.count = 0
        # else:
        #     self.count += 1
        self.count = 0 if self.count == len(
            self.our_images) - 1 else self.count + 1

        self.create_image(self.count)
        self.root.after(self.wait_for_change_duration, self.next)

    def create_image(self, index: int = 0):
        """ Create an image on the canvas 
        index: int - image index in the image list
        """
        self.my_canvas.create_image(
            0, 0, image=self.our_images[index], anchor=NW)


app = TimedBgApp(root)
app.start()
