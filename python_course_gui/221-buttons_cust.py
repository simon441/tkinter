from pathlib import Path
from tkinter import LEFT, RIGHT, TOP

import customtkinter
from PIL import Image, ImageTk

customtkinter.set_appearance_mode("dark")
customtkinter.set_default_color_theme('dark-blue')

root = customtkinter.CTk()
root.title('Custom Buttons With Images')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('620x470')

# Define Images
FOLDER_PATH = 'test_images'


def getPath(p: str) -> Path:
    return Path.joinpath(Path(__file__).parent.resolve(), FOLDER_PATH, p)


def getImage(image_name: str) -> ImageTk.PhotoImage:
    return ImageTk.PhotoImage(Image.open(getPath(image_name)).resize((20, 20), resample=Image.Resampling.LANCZOS))


# add_folder_image = ImageTk.PhotoImage(Image.open(getPath('add-folder.png')).resize((20,20), Image.ANTIALIAS))
add_folder_image = getImage('add-folder.png')
add_list_image = getImage('add-list.png')
add_user_image = getImage('add-user.png')

# Create Buttons
button_1 = customtkinter.CTkButton(
    master=root, image=add_folder_image, text='Add Folder', width=190, height=40, compound=LEFT)
button_1.pack(pady=20, padx=20)

button_2 = customtkinter.CTkButton(
    master=root, image=add_folder_image, text='Add List', width=190, height=40, compound=RIGHT,
    fg_color='#D35B58', hover_color='#C77C78')
button_2.pack(pady=10, padx=20)

button_3 = customtkinter.CTkButton(
    master=root, image=add_folder_image, text='Add User', width=190, height=40, compound=TOP,
    fg_color='#C3C3C3', hover_color='#A6A6A0', text_color='#444')
button_3.pack(pady=10, padx=20)

if __name__ == '__main__':
    root.mainloop()
