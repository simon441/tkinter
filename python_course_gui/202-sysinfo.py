from tkinter import Label, Tk
from pathlib import Path
import platform

root = Tk()
root.title('System Info')
root.iconbitmap(Path.joinpath(Path(__file__).parent.resolve(), 'codemy.ico'))
root.geometry('600x300')

info = f"""System: {platform.system()}
User Name: {platform.node()}
Release: {platform.release()}
Version: {platform.version()}
Machine Type: {platform.machine()}
Processor: {platform.processor()}
Python Version: {platform.python_version()}
"""

my_label = Label(root, text=info, font=('Helvetica', 14))
my_label.pack(pady=20)

root.mainloop()
