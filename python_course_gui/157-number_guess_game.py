import tkinter as Tk
from random import randint

root = Tk.Tk()
root.title("Guess The Number!")
root.iconbitmap("codemy.ico")
root.geometry('520x500')


# variables and constants
# numbezr to guess
num = 0
# store default background color
DEFAULT_BG = root['bg']
# max number to generate
MAX_NUMBER = 10
# default text fror the label
PICK_NUMBER_TEXT = f"Pick A Number\nBetween 1 and {MAX_NUMBER}!"

num_label = Tk.Label(
    root, text=PICK_NUMBER_TEXT, font=("Comic Sans MS", 32))
num_label.pack(pady=20)

guess_box = Tk.Entry(root, font=("Helvetica", 100), width=2)
guess_box.pack(pady=20)


def guesser():
    """
    Check if guees is right or not
    """
    if guess_box.get().isdigit():
        # Reset the label
        num_label.config(text=PICK_NUMBER_TEXT)
        guessed_int = int(guess_box.get())

        if guessed_int < 0 or guessed_int > MAX_NUMBER:
            num_label.config(
                text=f"Number must be between\n1 and {MAX_NUMBER}")
            return

        # Find out how far away the picked number is from the generated number
        diff = abs(num - guessed_int)

        # Check to see if correct
        if guessed_int == num:
            bc = DEFAULT_BG
            num_label.config(text="Correct!!")
        elif diff == 5:
            # Set background color to white
            bc = "white"
        elif diff < 5:
            bc = f"#ff{diff}{diff}{diff}{diff}"
        else:
            bc = f"#{diff}{diff}{diff}{diff}ff"

        # Change the background color of the app
        root.config(bg=bc)
        # Change background of label
        num_label.config(bg=bc)
    else:
        # Delet entry and throw error message
        guess_box.delete(0, Tk.END)
        num_label.config(text="Hey!\nThat's Not A Number!")


def rando():
    """
    Generate a random number
    """
    global num
    # Generate a random number
    num = randint(1, MAX_NUMBER)
    # Clear the guess box
    guess_box.delete(0, Tk.END)

    # Change the colors back to normal and reset label text
    num_label.config(bg=DEFAULT_BG, text=PICK_NUMBER_TEXT)
    root.config(bg=DEFAULT_BG)


guess_button = Tk.Button(root, text="Submit", command=guesser)
guess_button.pack(pady=20)

rand_button = Tk.Button(root, text="New Number", command=rando)
rand_button.pack(pady=20)

# Generate random number on start
rando()

root.mainloop()
