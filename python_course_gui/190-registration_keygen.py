import random
from tkinter import *

WIDTH = 500
HEIGHT = 500

root = Tk()
root.title('Software Registration Key Generator')
root.iconbitmap('codemy.ico')
root.geometry(f"{WIDTH}x{HEIGHT}")

class AppRegistration():

    def __init__(self, root: Tk, *args, **kwargs):
        self.master = root

        self.score = 0 # verifying scrore
        self.create_widgets()

    def create_widgets(self):
        """ Create the widgets """
        # Button to generate the Key
        self.generate_button = Button(self.master, text='Generate Key!', font=("Helvetica", 32), command=self.generate)
        self.generate_button.pack(pady=50)

        # Entry box disguised as a label for copying the key
        self.key_label = Entry(self.master, font=("Helvetica", 24), bd=0, bg="SystemButtonFace", width=25)
        self.key_label.pack(pady=10, padx=50)

        # Verify label
        self.verify_label = Label(self.master, text="Waiting...", font=("Helvetica", 32))
        self.verify_label.pack(pady=10)

        # Score label
        self.score_label = Label(self.master, text="Score: ", font=("Helvetica", 32))
        self.score_label.pack(pady=10)

    def generate(self):
        """ Generate a new key """
        # Clear key label
        self.key_label.delete(0, END)
        self.verify_label.config(text="")

        # Set defaults
        key = ""
        section_blob = "" # blob of 4 characters
        check_digit_count = 0
        alphabet = "abcdefghijklmnoprqrstuvxyz0123456789"
        # alphabet = "azertyuiopqsdfghjklmwxcvbn0123456789"

        # key = aaaa-bbbb-cccc-dddd-1111 or 24 characters
        while len(key) < 25:
            # Get random char from alphabet
            char = random.choice(alphabet)
            # Add random choice  to key
            key += char
            # Also add the random choice to the section blob
            section_blob += char

            # Add in the dashes
            if len(section_blob) == 4:
                # add in a hyphen
                key += "-"
                # Reset the section blob to nothing
                section_blob = ""

        # Set the key to all but the last digit
        key = key[:-1]

        # Output the key
        # self.key_label.insert(0, key)

        # Verify the key
        if self.verify(key):
            # key is verified
            self.key_label.insert(0, key)
            self.verify_label.config(text="Valid!")
            self.score_label.config(text=f'Score: {self.score}')
        else:
            # Key is not verified

            # Run the generate method again
            self.generate()

    def verify(self, key: str)-> bool:
        """Check the key has the correct format"""
        # Reset score
        self.score = 0

        # Define the check digit (special digit to make a rule for)
        check_digit = key[2] # third digit
        check_digit_count = 0 # how many times the digit is present in the key

        # Separate by Dash
        chunks = key.split('-')

        # Loop through and check digit presence
        for chunk in chunks:
            if len(chunk) != 4:
                return False

            for char in chunk:
                if char == check_digit:
                    check_digit_count += 1
                # Grab the score of the ASCII character
                self.score += ord(char)

        # Check for rules
        if self.score > 1700 and self.score < 1800 and check_digit_count == 3:
            return True
        return False


    def run(self):
        self.master.mainloop()

app = AppRegistration(root)
app.run()
